import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Silabus = {
  /**
   * Create Silabus
   * @param {object} req 
   * @param {object} res
   * @returns {object} silabus object 
   */
  async createSilabus(req, res){
    const text = `INSERT INTO
      silabus(
        kode_matkul,
        nama_matkul,
        silabus_ringkas,
        silabus_lengkap,
        luaran,
        mata_kuliah_terkait,
        kegiatan_penunjang,
        pustaka,
        panduan_penilaian,
        catatan_tambahan)
      VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
      returning *`;
    const values = [
        req.body.kode_matkul,
        req.body.nama_matkul,
        req.body.silabus_ringkas,
        req.body.silabus_lengkap,
        req.body.luaran,
        req.body.mata_kuliah_terkait,
        req.body.kegiatan_penunjang,
        req.body.pustaka,
        req.body.panduan_penilaian,
        req.body.catatan_tambahan
    ];

    try{
        const { rows } = await db.query(text, values);
        return res.status(201).send(rows[0]);
    } catch(error){
        return res.status(400).send(error);
    }
},
    /**
   * Get All Silabus 
   * @param {object} req 
   * @param {object} res 
   * @returns {object} silabus array
   */
    async getAllSilabus(req, res){
        const findAllQuery = 'SELECT * FROM silabus;';
        try {
            const { rows } = await db.query(findAllQuery);
            return res.status(200).send(rows);
        } catch(error) {
            return res.status(400).send(error);
        }
    },
         /**
   * Get One Silabus 
   * @param {object} req 
   * @param {object} res
   * @returns {object} silabus object
   */
  async getOneSilabus(req, res) {
    const text = `SELECT * FROM silabus
    WHERE kode_matkul = $1`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        return res.status(200).send(rows);
      } catch(error){
          return res.status(400).send(error)
      } 
},
/**
 * Update Silabus
 * @param {object} req 
 * @param {object} res 
 * @returns {object} silabus kurikulum
 */
async updateSilabus(req, res){
  const findOneQuery = `SELECT * FROM silabus
  WHERE kode_matkul = $1`;
  const updateOneQuery =`UPDATE silabus
    SET
    nama_matkul=$1,
    silabus_ringkas=$2,
    silabus_lengkap=$3,
    luaran=$4,
    mata_kuliah_terkait=$5,
    kegiatan_penunjang=$6,
    pustaka=$7,
    panduan_penilaian=$8,
    catatan_tambahan=$9
    WHERE kode_matkul=$10 returning *`;
  try{
      const { rows } = await db.query(findOneQuery, [req.params.kode_matkul]);
      if(!rows[0]) {
          return res.status(404).send({'message': 'kode matkul not found'});
      }
      const values = [
        req.body.nama_matkul || rows[0].nama_matkul,
        req.body.silabus_ringkas || rows[0].silabus_ringkas,
        req.body.silabus_lengkap || rows[0].silabus_lengkap,
        req.body.luaran || rows[0].luaran,
        req.body.mata_kuliah_terkait || rows[0].mata_kuliah_terkait,
        req.body.kegiatan_penunjang || rows[0].kegiatan_penunjang,
        req.body.pustaka || rows[0].pustaka,
        req.body.panduan_penilaian || rows[0].panduan_penilaian,
        req.body.catatan_tambahan || rows[0].catatan_tambahan,
        req.params.kode_matkul
      ];
      const response = await db.query(updateOneQuery, values);
      return res.status(200).send(response.rows[0]);
    
  } catch(err){
      return res.status(400).send(err);
  }
},
/**
* Delete Silabus
* @param {object} req 
* @param {object} res 
* @returns {void} return status code 204 
*/
async deleteSilabus(req, res){
  const deleteQuery = 'DElETE FROM silabus WHERE kode_matkul=$1 returning *';
  try{
      const { rows } = await db.query(deleteQuery, [req.params.kode_matkul]);
      if (!rows[0]){
          return res.status(404).send({'message': 'kode matkul not found'});    
      }
      else{
      return res.status(204).send({'message': 'deleted'});
      }
  } catch(error){
    return res.status(400).send(error);
  }
  
}

    

}

export default Silabus;