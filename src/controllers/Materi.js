import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';
import path from 'path';


const Materi = {
       /**
   * Get All Materi
   * @param {object} req 
   * @param {object} res
   * @returns {object} materi object
   */
  async getAllMateri(req, res) {
    const text = `SELECT kode_matkul,minggu,materi FROM sap
    WHERE kode_matkul = $1`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        return res.status(200).send(rows);
      } catch(error){
          return res.status(400).send(error)
      } 
},
   /**
   * Get One Materi
   * @param {object} req 
   * @param {object} res
   * @returns {object} silabus object
   */
  async getOneMateri(req, res) {
    const text = `SELECT materi FROM sap
    WHERE kode_matkul = $1 AND minggu = $2`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul,req.params.minggu]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        
        var file = __dirname + '/materi/' + req.params.kode_matkul + '/' + rows[0].materi;
        console.log(file);
        return res.download(file);
      } catch(error){
          return res.status(400).send(error)
      } 
}
}
export default Materi;
