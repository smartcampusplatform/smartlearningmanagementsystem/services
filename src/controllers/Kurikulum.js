import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Kurikulum = {
    /**
   * Create Kurikulum
   * @param {object} req
   * @param {object} res
   * @returns {object} kurikulum object
   */
  async createKurikulum(req, res){
    const text = `INSERT INTO
      kurikulum(id_kurikulum,kode_prodi,nama_fakultas,nama_prodi,tahun_kurikulum,semester,kode_matkul,nama_matkul,sks,wajib,pilihan)
      VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
      returning *`;
    const values = [
        req.body.id_kurikulum,
        req.body.kode_prodi,
        req.body.nama_fakultas,
        req.body.nama_prodi,
        req.body.tahun_kurikulum,
        req.body.semester,
        req.body.kode_matkul,
        req.body.nama_matkul,
        req.body.sks,
        req.body.wajib,
        req.body.pilihan
    ];

    try{
        const { rows } = await db.query(text, values);
        return res.status(201).send(rows[0]);
    } catch(error){
        return res.status(400).send(error);
    }
},

    /**
   * Get All Matkul
   * @param {object} req
   * @param {object} res
   * @returns {object} matkul array
   */
    async getAllKurikulum(req, res){
        const findAllQuery = 'SELECT * FROM kurikulum;';
        res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
        try {
            const { rows } = await db.query(findAllQuery);
            return res.status(200).send(rows);
        } catch(error) {
            return res.status(400).send(error);
        }
    },

   /**
   * Get All Matkul wajib
   * @param {object} req
   * @param {object} res
   * @returns {object} matkul wajib array
   */
  async getAllWajib(req, res){
      const findAllQuery = `SELECT *
     FROM
      kurikulum
     WHERE wajib = TRUE AND kode_prodi = $1 AND tahun_kurikulum = $2;`
     ;
     res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
      try {
          const { rows } = await db.query(findAllQuery, [req.params.kode_prodi,req.params.tahun_kurikulum]);
          if (!rows[0]) {
            console.log(req.params);
            return res.status(404).send({'message': 'prodi not found'});
        }
          return res.status(200).send(rows);
      } catch(error) {
          return res.status(400).send(error);
      }
  },
     /**
   * Get All Matkul pilihan
   * @param {object} req
   * @param {object} res
   * @returns {object} matkul wajib array
   */
  async getAllPilihan(req, res){
    const findAllQuery = `SELECT *
   FROM
    kurikulum
   WHERE pilihan = TRUE AND kode_prodi = $1 AND tahun_kurikulum = $2;`
   ;
   res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
    try {
        const { rows } = await db.query(findAllQuery, [req.params.kode_prodi,req.params.tahun_kurikulum]);
        if (!rows[0]) {
          console.log(req.params);
          return res.status(404).send({'message': 'prodi not found'});
      }
        return res.status(200).send(rows);
    } catch(error) {
        return res.status(400).send(error);
    }
},
    /**
   * Get One Semester Kurikulum
   * @param {object} req
   * @param {object} res
   * @returns {object} semester object
   */
  async getOneSemester(req, res) {
      const text = `SELECT * FROM kurikulum
      WHERE wajib=TRUE
      AND kurikulum.kode_prodi = $1
      AND kurikulum.tahun_kurikulum = $2
      AND kurikulum.semester = $3`;
      try {
          const { rows } = await db.query(text, [req.params.kode_prodi,req.params.tahun_kurikulum,req.params.semester]);
          if (!rows[0]) {
              return res.status(404).send({'message': 'semester not found'});
          }
          return res.status(200).send(rows);
        } catch(error){
            return res.status(400).send(error)
        }
  },
  /**
   * Update Kurikulum
   * @param {object} req
   * @param {object} res
   * @returns {object} updated kurikulum
   */
  async updateKurikulum(req, res){
    const findOneQuery = 'SELECT * FROM kurikulum WHERE id_kurikulum=$1';
    const updateOneQuery =`UPDATE kurikulum
      SET kode_prodi=$1,nama_fakultas=$2,nama_prodi=$3,tahun_kurikulum=$4,semester=$5,kode_matkul=$6,nama_matkul=$7,sks=$8,wajib=$9,pilihan=$10
      WHERE id_kurikulum=$11 returning *`;
    try{
        const { rows } = await db.query(findOneQuery, [req.params.id_kurikulum]);
        if(!rows[0]) {
            return res.status(404).send({'message': 'kurikulum id not found'});
        }
        const values = [
            req.body.kode_prodi || rows[0].kode_prodi,
            req.body.nama_fakultas || rows[0].nama_fakultas,
            req.body.nama_prodi || rows[0].nama_prodi,
            req.body.tahun_kurikulum || rows[0].tahun_kurikulum,
            req.body.semester || rows[0].semester,
            req.body.kode_matkul || rows[0].kode_matkul,
            req.body.nama_matkul || rows[0].nama_matkul,
            req.body.sks || rows[0].sks,
            req.body.wajib || rows[0].wajib,
            req.body.pilihan || rows[0].pilihan,
            req.params.id_kurikulum
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);

    } catch(err){
        return res.status(400).send(err);
    }
},
  /**
 * Delete Kurikulum
 * @param {object} req
 * @param {object} res
 * @returns {void} return status code 204
 */
async deleteKurikulum(req, res){
    const deleteQuery = 'DElETE FROM kurikulum WHERE id_kurikulum=$1 returning *';
    try{
        const { rows } = await db.query(deleteQuery, [req.params.id_kurikulum]);
        if (!rows[0]){
            return res.status(404).send({'message': 'kurikulum id not found'});
        }
        else{
        return res.status(204).send({'message': 'deleted'});
        }
    } catch(error){
      return res.status(400).send(error);
    }

}

}

export default Kurikulum;
