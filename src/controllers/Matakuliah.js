import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Matkul = {
/**
     * Create Matkul
     * @param {object} req
     * @param {object} res
     * @returns {object} Matkul object
     */
    async createMatkul(req, res){
        const text = `INSERT INTO
        matkul(
        id_matkul,
        kode_matkul,
        nama_matkul
        )
        VALUES($1, $2, $3)
        returning *`;
        const values = [
            req.body.id_matkul,
            req.body.kode_matkul,
            req.body.nama_matkul,
        ];
        try{
            const { rows } = await db.query(text, values);
            return res.status(201).send(rows[0]);
        } catch(error){
            return res.status(400).send(error);
        }
    },

/**
     * Update Matakuliah
    * @param {object} req
    * @param {object} res
    * @returns {object} Matkul Object
    */
    async updateMatkul(req, res){
        const findOneQuery = `SELECT * FROM matkul
        WHERE id_matkul = $1`;
        const updateOneQuery =`UPDATE matkul
          SET
          kode_matkul=$1,
          nama_matkul=$2
          WHERE id_matkul=$3 returning *`;
        try{
            const { rows } = await db.query(findOneQuery, [req.params.id_matkul]);
            if(!rows[0]) {
                return res.status(404).send({'message': 'id mata kuliah not found'});
            }
            const values = [
              req.body.kode_matkul || rows[0].kode_matkul,
              req.body.nama_matkul || rows[0].nama_matkul,
              req.params.id_matkul
            ];
            const response = await db.query(updateOneQuery, values);
            return res.status(200).send(response.rows[0]);

        } catch(err){
            return res.status(400).send(err);
        }
    },

     /**
   * Get Semua matkul
   * @param {object} req
   * @param {object} res
   * @returns {object} Matkul array
   */
  async getAll(req, res){
    const findAllQuery = 'SELECT * FROM matkul;';
    res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
    try {
        const { rows } = await db.query(findAllQuery);
        return res.status(200).send(rows);
    } catch(error) {
        return res.status(400).send(error);
    }
  },

  /**
   * Get A Matkul
   * @param {object} req
   * @param {object} res
   * @returns {object} Matkul object
   */
  async getOneMatkul(req, res) {
    const text = 'SELECT * FROM matkul WHERE id_matkul=$1 ';
    try {
        const { rows } = await db.query(text, [req.params.id_matkul]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'reflection not found'});
        }
        return res.status(200).send(rows[0]);
      } catch(error){
          return res.status(400).send(error)
      }
  },

  /**
  * Delete matakuliah
  * @param {object} req
  * @param {object} res
  * @returns {void} return status code 204
  */
  async deleteMatkul(req, res){
    const deleteQuery = 'DElETE FROM matkul WHERE id_matkul=$1 returning *';
    try{
        const { rows } = await db.query(deleteQuery, [req.params.id_matkul]);
        if (!rows[0]){
            return res.status(404).send({'message': 'id matakuliah not found'});
        }
        else{
        return res.status(204).send({'message': 'deleted'});
        }
    } catch(error){
      return res.status(400).send(error);
    }

  }

}

export default Matkul;
