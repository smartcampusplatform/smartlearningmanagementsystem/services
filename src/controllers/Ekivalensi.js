import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Ekivalensi = {
/**
   * Create Ekivalensi
   * @param {object} req 
   * @param {object} res
   * @returns {object} ekivalensi object 
   */
  async createEkivalensi(req, res){
    const text = `INSERT INTO
      ekivalensi(
    id_ekivalensi,
    semester_lama,
    kode_matkul_lama,
    nama_matkul_lama,
    sks_lama,
    jenis_lama,
    semester_baru,
    kode_matkul_baru,
    nama_matkul_baru,
    sks_baru,
    jenis_baru)
      VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
      returning *`;
    const values = [
        req.body.id_ekivalensi,
        req.body.semester_lama,
        req.body.kode_matkul_lama,
        req.body.nama_matkul_lama,
        req.body.sks_lama,
        req.body.jenis_lama,
        req.body.semester_baru,
        req.body.kode_matkul_baru,
        req.body.nama_matkul_baru,
        req.body.sks_baru,
        req.body.jenis_baru
    ];

    try{
        const { rows } = await db.query(text, values);
        return res.status(201).send(rows[0]);
    } catch(error){
        return res.status(400).send(error);
    }
},



    /**
   * Get All Ekivalensi
   * @param {object} req 
   * @param {object} res 
   * @returns {object} ekivalensi array
   */
    async getAllEkivalensi(req, res){
        const findAllQuery = 'SELECT * FROM ekivalensi;';
        try {
            const { rows } = await db.query(findAllQuery);
            return res.status(200).send(rows);
        } catch(error) {
            return res.status(400).send(error);
        }
    },
         /**
   * Get Ekivalensi Lama
   * @param {object} req 
   * @param {object} res
   * @returns {object} ekivalensi object
   */
  async getEkivalensiLama(req, res) {
    const text = `SELECT * FROM ekivalensi
    WHERE kode_matkul_lama = $1`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul_lama]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        return res.status(200).send(rows);
      } catch(error){
          return res.status(400).send(error)
      } 
},

         /**
   * Get Ekivalensi Baru
   * @param {object} req 
   * @param {object} res
   * @returns {object} ekivalensi object
   */
  async getEkivalensiBaru(req, res) {
    const text = `SELECT * FROM ekivalensi
    WHERE kode_matkul_baru = $1`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul_baru]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        return res.status(200).send(rows);
      } catch(error){
          return res.status(400).send(error)
      } 
},
/**
* Delete Ekivalensi
* @param {object} req 
* @param {object} res 
* @returns {void} return status code 204 
*/
async deleteEkivalensi(req, res){
    const deleteQuery = 'DElETE FROM ekivalensi WHERE id_ekivalensi=$1 returning *';
    try{
        const { rows } = await db.query(deleteQuery, [req.params.id_ekivalensi]);
        if (!rows[0]){
            return res.status(404).send({'message': 'id ekivalensi not found'});    
        }
        else{
        return res.status(204).send({'message': 'deleted'});
        }
    } catch(error){
      return res.status(400).send(error);
    }
    
  }

    

}

export default Ekivalensi;