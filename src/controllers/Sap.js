import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Sap = {
  /**
   * Create Silabus
   * @param {object} req 
   * @param {object} res
   * @returns {object} silabus object 
   */
  async createSap(req, res){
    const text = `INSERT INTO
      sap(
        id_sap,
        kode_matkul,
    minggu,
    topik,
    subtopik,
    capaian_belajar,
    sumber_materi)
      VALUES(default,$1, $2, $3, $4, $5, $6)
      returning *`;
    const values = [
        req.body.kode_matkul,
        req.body.minggu,
        req.body.topik,
        req.body.subtopik,
        req.body.capaian_belajar,
        req.body.sumber_materi
    ];

    try{
        const { rows } = await db.query(text, values);
        return res.status(201).send(rows[0]);
    } catch(error){
        return res.status(400).send(error);
    }
},
    /**
   * Get All Sap
   * @param {object} req 
   * @param {object} res 
   * @returns {object} sap array
   */
    async getAllSap(req, res){
        const findAllQuery = 'SELECT * FROM sap;';
        try {
            const { rows } = await db.query(findAllQuery);
            return res.status(200).send(rows);
        } catch(error) {
            return res.status(400).send(error);
        }
    },
         /**
   * Get One Sap
   * @param {object} req 
   * @param {object} res
   * @returns {object} silabus object
   */
  async getOneSap(req, res) {
    const text = `SELECT * FROM sap
    WHERE kode_matkul = $1`;
    try {
        const { rows } = await db.query(text, [req.params.kode_matkul]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'matkul not found'});
        }
        return res.status(200).send(rows);
      } catch(error){
          return res.status(400).send(error)
      } 
},
/**
 * Update Sap
 * @param {object} req 
 * @param {object} res 
 * @returns {object} sap
 */
async updateSap(req, res){
  const findOneQuery = `SELECT * FROM sap
  WHERE id_sap = $1`;
  const updateOneQuery =`UPDATE sap
    SET
    kode_matkul=$1,
    minggu=$2,
    topik=$3,
    subtopik=$4,
    capaian_belajar=$5,
    sumber_materi=$6
    WHERE id_sap=$7 returning *`;
  try{
      const { rows } = await db.query(findOneQuery, [req.params.id_sap]);
      if(!rows[0]) {
          return res.status(404).send({'message': 'id sap not found'});
      }
      const values = [
        req.body.kode_matkul || rows[0].kode_matkul,
        req.body.minggu || rows[0].minggu,
        req.body.topik || rows[0].topik,
        req.body.subtopik || rows[0].subtopik,
        req.body.capaian_belajar || rows[0].capaian_belajar,
        req.body.sumber_materi || rows[0].sumber_materi,
        req.params.id_sap
      ];
      const response = await db.query(updateOneQuery, values);
      return res.status(200).send(response.rows[0]);
    
  } catch(err){
      return res.status(400).send(err);
  }
},
/**
* Delete Sap
* @param {object} req 
* @param {object} res 
* @returns {void} return status code 204 
*/
async deleteSap(req, res){
  const deleteQuery = 'DElETE FROM sap WHERE id_sap=$1 returning *';
  try{
      const { rows } = await db.query(deleteQuery, [req.params.id_sap]);
      if (!rows[0]){
          return res.status(404).send({'message': 'id sap not found'});    
      }
      else{
      return res.status(204).send({'message': 'deleted'});
      }
  } catch(error){
    return res.status(400).send(error);
  }
  
}

    

}

export default Sap;