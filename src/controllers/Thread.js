import moment, { updateLocale } from 'moment';
import db from '../db';

const Thread = {

/**
   * Create Thread
   * @param {object} req
   * @param {object} res
   * @returns {object} Thread object
   */
  async createThread(req, res){
    const text = `INSERT INTO
      thread(
    id_thread,
    kode_matkul,
    judul_thread,
    pertanyaan,
    isdone,
    favorit,
    userid,
    datecreated
    )
      VALUES($1, $2, $3, $4, $5, $6, $7, $8)
      returning *`;
    const values = [
        req.body.id_thread,
        req.body.kode_matkul,
        req.body.judul_thread,
        req.body.pertanyaan,
        req.body.isdone,
        req.body.favorit,
        req.body.userid,
        moment(new Date())
    ];
    res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
    try{
        const { rows } = await db.query(text, values);
        return res.status(201).send(rows[0]);
    } catch(error){

        console.log(req.body);
        console.log(error);
        return res.status(400).send(error);
    }
  },

  /**
     * Update Thread
    * @param {object} req
    * @param {object} res
    * @returns {object} Thread Object
    */
   async updateThread(req, res){
    const findOneQuery = `SELECT * FROM thread
    WHERE id_thread = $1`;
    const updateOneQuery =`UPDATE thread
      SET
      judul_thread=$1,
      pertanyaan=$2,
      isdone=$3,
      favorit=$4
      WHERE id_thread=$5 returning *`;
    try{
        const { rows } = await db.query(findOneQuery, [req.params.id_thread]);
        if(!rows[0]) {
            return res.status(404).send({'message': 'id thread not found'});
        }
        const values = [
          req.body.judul_thread || rows[0].judul_thread,
          req.body.pertanyaan || rows[0].pertanyaan,
          req.body.isdone || rows[0].isdone,
          req.body.favorit || rows[0].favorit,
          req.params.id_thread
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);

    } catch(err){
        return res.status(400).send(err);
    }
  },

  /**
     * Get All Thread from matkul
     * @param {object} req
     * @param {object} res
     * @returns {object} Thread array
     */
    async getAll(req, res){
      const findAllQuery = 'SELECT * FROM thread WHERE kode_matkul=$1;';
      res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
      try {
        const { rows } = await db.query(findAllQuery, [req.params.kode_matkul]);
        if (!rows[0]) {
          console.log(req.params);
          return res.status(404).send({'message': 'thread not found'});
      }
        return res.status(200).send(rows);
      } catch(error) {
        return res.status(400).send(error);
      }
  },
  /**
     * Get Thread tertentu
     * @param {object} req
     * @param {object} res
     * @returns {object} Thread Object
     */
  async getOne(req, res){
    const findAllQuery = 'SELECT * FROM thread WHERE id_thread=$1;';
    res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');
    try {
      const { rows } = await db.query(findAllQuery, [req.params.id_thread]);
      if (!rows[0]) {
        console.log(req.params);
        return res.status(404).send({'message': 'Thread not found'});
    }
      return res.status(200).send(rows);
    } catch(error) {
      return res.status(400).send(error);
    }
},

/**
   * Get A Thread
   * @param {object} req
   * @param {object} res
   * @returns {object} Thread object
   */
  async search(req, res) {
    const text = 'SELECT * FROM thread WHERE pertanyaan LIKE '%' + $1 + '%'; ' ;
    try {
        const { rows } = await db.query(text, [req.params.pertanyaan]);
        if (!rows[0]) {
            return res.status(404).send({'message': 'reflection not found'});
        }
        return res.status(200).send(rows[0]);
      } catch(error){
          return res.status(400).send(error)
      }
  },

  /**
       * Update Like Thread
      * @param {object} req
      * @param {object} res
      * @returns {object} Thread Object
      */
    async likeThread(req, res){
      const findOneQuery = `SELECT * FROM thread
      WHERE id_thread = $1`;
      const updateOneQuery =`UPDATE thread
        SET
        judul_thread=$1,
        pertanyaan=$2,
        isdone=$3,
        favorit=$4
        WHERE id_thread=$5 returning *`;
      try{
          const { rows } = await db.query(findOneQuery, [req.params.id_thread]);
          if(!rows[0]) {
              return res.status(404).send({'message': 'id thread not found'});
          }
          const values = [
            req.body.judul_thread || rows[0].judul_thread,
            req.body.pertanyaan || rows[0].pertanyaan,
            req.body.isdone || rows[0].isdone,
            req.body.favorit || rows[0].favorit,
            req.params.id_thread
          ];
          const response = await db.query(updateOneQuery, values);
          return res.status(200).send(response.rows[0]);

      } catch(err){
          return res.status(400).send(err);
      }
    },
  /**
    * Delete thread
    * @param {object} req
    * @param {object} res
    * @returns {void} return status code 204
    */
  async deleteThread(req, res){
    const deleteQuery = 'DElETE FROM thread WHERE id_thread=$1 returning *';
    try{
        const { rows } = await db.query(deleteQuery, [req.params.id_thread]);
        if (!rows[0]){
            return res.status(404).send({'message': 'id thread not found'});
        }
        else{
        return res.status(204).send({'message': 'deleted'});
        }
    } catch(error){
      return res.status(400).send(error);
    }

  }
/**
   * Increase Favorit
   * @param {object} req
   * @param {object} res
   * @returns {object} Updated Thread

  async increaseFavorit(req, res){
    const findOneQuery = 'SELECT * FROM thread WHERE id_thread=$1';
    const updateOneQuery =`UPDATE thread
      SET favorit+=1 returning *`;
    try{
        const { rows } = await db.query(findOneQuery, [req.params.id]);
        if(!rows[0]) {
            return res.status(404).send({'message': 'thread not found'});
        }
        const values = [
            //ini begimana ya
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);

    } catch(err){
        return res.status(400).send(err);
    }
  },
*/

/**
   * Diselesaikan
   * @param {object} req
   * @param {object} res
   * @returns {object} Updated Thread

  async selesaikan(req, res){
    const findOneQuery = 'SELECT * FROM thread WHERE id_thread=$1';
    const updateOneQuery =`UPDATE thread
      SET isdone=1 returning *`;
    try{
        const { rows } = await db.query(findOneQuery, [req.params.id]);
        if(!rows[0]) {
            return res.status(404).send({'message': 'thread not found'});
        }
        const values = [
            //ini begimana ya
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);

    } catch(err){
        return res.status(400).send(err);
    }
  }
*/
}

export default Thread;
