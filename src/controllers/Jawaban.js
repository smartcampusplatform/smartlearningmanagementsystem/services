import moment, { updateLocale } from 'moment';
import db from '../db';
import { create } from 'domain';
import { getEnabledCategories } from 'trace_events';

const Jawaban = {

  /**
     * Create Jawaban
     * @param {object} req
     * @param {object} res
     * @returns {object} Jawaban object
     */
    async createJawaban(req, res){
      const text = `INSERT INTO
        jawaban(
      id_jawaban,
      id_thread,
      jawaban,
      favorit,
      userid,
      datecreated
      )
        VALUES($1, $2, $3, $4, $5, $6)
        returning *`;
      const values = [
          req.body.id_jawaban,
          req.body.id_thread,
          req.body.jawaban,
          req.body.favorit,
          req.body.userid,
          moment(new Date())
      ];

      try{
          const { rows } = await db.query(text, values);
          return res.status(201).send(rows[0]);
      } catch(error){
          return res.status(400).send(error);
      }
  },

  /**
 * Update Jawaban
 * @param {object} req
 * @param {object} res
 * @returns {object} Jawaban Object
 */
  async updateJawaban(req, res){
    const findOneQuery = `SELECT * FROM jawaban
    WHERE id_jawaban = $1`;
    const updateOneQuery =`UPDATE jawaban
      SET
      jawaban=$1,
      favorit=$2
      WHERE id_jawaban=$3 returning *`;
    try{
        const { rows } = await db.query(findOneQuery, [req.params.id_jawaban]);
        if(!rows[0]) {
            return res.status(404).send({'message': 'id jawaban not found'});
        }
        const values = [
          req.body.jawaban || rows[0].jawaban,
          req.body.favorit || rows[0].favorit,
          req.params.id_jawaban
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);

    } catch(err){
        return res.status(400).send(err);
    }
  },
  /**
   * Get Jawaban dari Thread tertentu
   * @param {object} req
   * @param {object} res
   * @returns {object} Jawaban array
   */
  async getJawaban(req, res){
      const findAllQuery = 'SELECT * FROM jawaban WHERE id_thread=$1;';
      res.setHeader('Access-Control-Allow-Origin', 'http://178.128.104.74:6015');

      try {
        const { rows } = await db.query(findAllQuery, [req.params.id_thread]);
        if (!rows[0]) {
          console.log(req.params);
          return res.status(404).send({'message': 'jawaban not found'});
      }
        return res.status(200).send(rows);
      } catch(error) {
        return res.status(400).send(error);
      }
  },

  /**
  * Delete Jawaban
  * @param {object} req
  * @param {object} res
  * @returns {void} return status code 204
  */
  async deleteJawaban(req, res){
    const deleteQuery = 'DElETE FROM jawaban WHERE id_jawaban=$1 returning *';
    try{
        const { rows } = await db.query(deleteQuery, [req.params.id_jawaban]);
        if (!rows[0]){
            return res.status(404).send({'message': 'id jawaban not found'});
        }
        else{
        return res.status(204).send({'message': 'deleted'});
        }
    } catch(error){
      return res.status(400).send(error);
    }

  }
    /**
     * Increase Favorit
     * @param {object} req
     * @param {object} res
     * @returns {object} Updated Thread

    async increaseFavorit(req, res){
      const findOneQuery = 'SELECT * FROM jawaban WHERE id_jawaban=$1';
      const updateOneQuery =`UPDATE jawaban
        SET favorit+=1 returning *`;
      try{
          const { rows } = await db.query(findOneQuery, [req.params.id]);
          if(!rows[0]) {
              return res.status(404).send({'message': 'thread not found'});
          }
          const values = [
              //ini begimana ya
          ];
          const response = await db.query(updateOneQuery, values);
          return res.status(200).send(response.rows[0]);

      } catch(err){
          return res.status(400).send(err);
      }
    }
  */
}

export default Jawaban;
