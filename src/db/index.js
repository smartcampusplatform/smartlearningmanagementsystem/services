import { Pool } from 'pg';
import dotenv from 'dotenv';

dotenv.config();

var connectionString= 'postgres://'+ process.env.DB_USER + ':'+ process.env.DB_PASS + '@' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME;

const pool = new Pool({
   /* connectionString: process.env.DATABASE_URL*/
   connectionString:connectionString
});

export default {
      /**
   * DB Query
   * @param {object} req
   * @param {object} res
   * @returns {object} object 
   */

   query(text, params){
       return new Promise((resolve, reject) => {
           pool.query(text, params)
           .then((res) => {
               resolve(res);
           })
           .catch((err) => {
               reject(err);
           })
       })
   }
}