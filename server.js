import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import 'babel-polyfill';
import Kurikulum from './src/controllers/Kurikulum';
import Silabus from './src/controllers/Silabus';
import Sap from './src/controllers/Sap';
import Ekivalensi from './src/controllers/Ekivalensi';
import Thread from './src/controllers/Thread';
import Jawaban from './src/controllers/Jawaban';
import Matkul from './src/controllers/Matakuliah';
import Materi from './src/controllers/Materi';
import bodyParser from 'body-parser';

dotenv.config();


const app = express()
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(require("body-parser").json());

app.use(cors())

app.use(express.json())
app.use

app.get('/', (req, res) => {
    return res.status(200).send({'message': 'Welcome to Smart Learning Management System'})
})
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/api/v2/kurikulum', Kurikulum.createKurikulum);
app.get('/api/v2/kurikulum', Kurikulum.getAllKurikulum);
app.get('/api/v2/kurikulum/wajib/:kode_prodi/:tahun_kurikulum', Kurikulum.getAllWajib);
app.get('/api/v2/kurikulum/wajib/:kode_prodi/:tahun_kurikulum/:semester', Kurikulum.getOneSemester);
app.get('/api/v2/kurikulum/pilihan/:kode_prodi/:tahun_kurikulum', Kurikulum.getAllPilihan);
app.put('/api/v2/kurikulum/:id_kurikulum', Kurikulum.updateKurikulum);
app.delete('/api/v2/kurikulum/:id_kurikulum', Kurikulum.deleteKurikulum);
app.post('/api/v2/silabus', Silabus.createSilabus);
app.get('/api/v2/silabus', Silabus.getAllSilabus);
app.get('/api/v2/silabus/:kode_matkul', Silabus.getOneSilabus);
app.put('/api/v2/silabus/:kode_matkul', Silabus.updateSilabus);
app.delete('/api/v2/silabus/:kode_matkul', Silabus.deleteSilabus);
app.post('/api/v2/sap', Sap.createSap);
app.get('/api/v2/sap', Sap.getAllSap);
app.get('/api/v2/sap/:kode_matkul', Sap.getOneSap);
app.put('/api/v2/sap/:id_sap', Sap.updateSap);
app.delete('/api/v2/sap/:id_sap', Sap.deleteSap);
app.get('/api/v2/ekivalensi', Ekivalensi.getAllEkivalensi);
app.get('/api/v2/ekivalensi/lama/:kode_matkul_lama', Ekivalensi.getEkivalensiLama);
app.get('/api/v2/ekivalensi/baru/:kode_matkul_baru', Ekivalensi.getEkivalensiBaru);
app.post('/api/v2/ekivalensi', Ekivalensi.createEkivalensi);
app.delete('/api/v2/ekivalensi/:id_ekivalensi', Ekivalensi.deleteEkivalensi);
app.get('/api/v2/materi/:kode_matkul', Materi.getAllMateri);
app.get('/api/v2/materi/:kode_matkul/:minggu', Materi.getOneMateri);

app.post('/api/v2/jawaban', Jawaban.createJawaban);
app.get('/api/v2/jawaban/:id_thread', Jawaban.getJawaban);
app.put('/api/v2/jawaban/:id_jawaban', Jawaban.updateJawaban);
app.delete('/api/v2/jawaban/:id_jawaban', Jawaban.deleteJawaban);

app.post('/api/v2/matakuliah', Matkul.createMatkul);
app.get('/api/v2/matakuliah', Matkul.getAll);
app.get('/api/v2/matakuliah/:id_matkul', Matkul.getOneMatkul);
app.put('/api/v2/matakuliah/:id_matkul', Matkul.updateMatkul);
app.delete('/api/v2/matakuliah/:id_matkul', Matkul.deleteMatkul);

app.post('/api/v2/thread', Thread.createThread);
app.get('/api/v2/thread/:kode_matkul', Thread.getAll);
app.get('/api/v2/thread/one/:id_thread' , Thread.getOne);
//app.get('/api/v2/thread/' + pertanyaan, Thread.search);
app.put('/api/v2/thread/:id_thread', Thread.updateThread);
app.delete('/api/v2/thread/:id_thread', Thread.deleteThread);

app.listen(80)
console.log('app running on port', 80);

