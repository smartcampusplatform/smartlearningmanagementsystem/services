"use strict";

var _express = _interopRequireDefault(require("express"));

var _dotenv = _interopRequireDefault(require("dotenv"));

require("babel-polyfill");

var _Kurikulum = _interopRequireDefault(require("./src/controllers/Kurikulum"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import ReflectionWithJsObject from './src/usingJSObject/controllers/Reflection';
//import ReflectionWithDB from './src/usingDB/controllers/Reflection';
_dotenv["default"].config(); //const Reflection = process.env.TYPE === 'db' ? ReflectionWithDB : ReflectionWithJsObject;


var app = (0, _express["default"])();
app.use(_express["default"].json());
app.get('/', function (req, res) {
  return res.status(200).send({
    'message': 'Welcome to Smart Learning Management System'
  });
});
app.get('/api/v1/kurikulumwajib/:semester', _Kurikulum["default"].getOneSemester);
/*
app.post('/api/v1/reflections', Reflection.create);
app.get('/api/v1/reflections', Reflection.getAll);
app.get('/api/v1/reflections/:id', Reflection.getOne);
app.put('/api/v1/reflections/:id', Reflection.update);
app.delete('/api/v1/reflections/:id', Reflection.delete);
*/

app.listen(3000);
console.log('app running on port', 3000);