--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ekivalensi; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.ekivalensi (
    id_ekivalensi character varying NOT NULL,
    semester_lama integer,
    kode_matkul_lama character varying,
    nama_matkul_lama character varying,
    sks_lama integer,
    jenis_lama character(1),
    semester_baru integer,
    kode_matkul_baru character varying,
    nama_matkul_baru character varying,
    sks_baru integer,
    jenis_baru character(1)
);


ALTER TABLE public.ekivalensi OWNER TO kelompok1;

--
-- Name: jawaban; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.jawaban (
    id_jawaban integer NOT NULL,
    id_thread integer,
    jawaban character varying,
    favorit integer,
    userid integer,
    datecreated timestamp without time zone
);


ALTER TABLE public.jawaban OWNER TO kelompok1;

--
-- Name: kurikulum; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.kurikulum (
    id_kurikulum character varying NOT NULL,
    kode_prodi integer,
    nama_fakultas character varying,
    nama_prodi character varying,
    tahun_kurikulum integer,
    semester integer,
    kode_matkul character varying,
    nama_matkul character varying,
    sks integer,
    wajib boolean,
    pilihan boolean
);


ALTER TABLE public.kurikulum OWNER TO kelompok1;

--
-- Name: matkul; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.matkul (
    id_matkul integer NOT NULL,
    kode_matkul character varying(16),
    nama_matkul character varying(50)
);


ALTER TABLE public.matkul OWNER TO kelompok1;

--
-- Name: prodi; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.prodi (
    kode_prodi integer NOT NULL,
    nama_fakultas character varying(16),
    nama_prodi character varying
);


ALTER TABLE public.prodi OWNER TO kelompok1;

--
-- Name: sap; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.sap (
    id_sap integer NOT NULL,
    kode_matkul character varying,
    minggu integer,
    topik character varying,
    subtopik character varying,
    capaian_belajar character varying,
    sumber_materi character varying
);


ALTER TABLE public.sap OWNER TO kelompok1;

--
-- Name: sap_id_sap_seq; Type: SEQUENCE; Schema: public; Owner: kelompok1
--

CREATE SEQUENCE public.sap_id_sap_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sap_id_sap_seq OWNER TO kelompok1;

--
-- Name: sap_id_sap_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kelompok1
--

ALTER SEQUENCE public.sap_id_sap_seq OWNED BY public.sap.id_sap;


--
-- Name: silabus; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.silabus (
    kode_matkul character varying(16) NOT NULL,
    nama_matkul character varying,
    silabus_ringkas character varying,
    silabus_lengkap character varying,
    luaran character varying,
    mata_kuliah_terkait character varying,
    kegiatan_penunjang character varying,
    pustaka character varying,
    panduan_penilaian character varying,
    catatan_tambahan character varying
);


ALTER TABLE public.silabus OWNER TO kelompok1;

--
-- Name: thread; Type: TABLE; Schema: public; Owner: kelompok1
--

CREATE TABLE public.thread (
    id_thread integer NOT NULL,
    kode_matkul character varying(16),
    judul_thread character varying(50),
    pertanyaan character varying,
    isdone boolean,
    favorit integer,
    userid integer,
    datecreated timestamp without time zone
);


ALTER TABLE public.thread OWNER TO kelompok1;

--
-- Name: sap id_sap; Type: DEFAULT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.sap ALTER COLUMN id_sap SET DEFAULT nextval('public.sap_id_sap_seq'::regclass);


--
-- Data for Name: ekivalensi; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.ekivalensi (id_ekivalensi, semester_lama, kode_matkul_lama, nama_matkul_lama, sks_lama, jenis_lama, semester_baru, kode_matkul_baru, nama_matkul_baru, sks_baru, jenis_baru) FROM stdin;
Ek2008-S1-STI-MA1101-182	1	MA1101	Kalkulus IA	4	W	1	MA1101	Matematika IA	4	W
Ek2008-S1-STI-FI1101-182	1	FI1101	Fisika Dasar IA	4	W	1	FI1101	Fisika Dasar IA	4	W
Ek2008-S1-STI-KI1101-182	1	KI1101	Kimia Dasar I A	3	W	1	KI1102	Kimia Dasar IB	2	W
Ek2008-S1-STI-KU1101-182	1	KU1101	Konsep Pengembangan Ilmu Pengetahuan	2	W	1	KU1101	Pengantar Rekayasa & Desain I	2	W
Ek2008-S1-STI-KU1071-182	1	KU1071	Pengenalan Teknologi Informasi A	2	W	1	KU1072	Pengenalan Teknologi Informasi B	2	W
Ek2008-S1-STI-KU102X-182	1	KU102X	Bahasa Inggris	2	W	1	KU102X	Bahasa Inggris (KU1021/1022/1023)	2	W
Ek2008-S1-STI-KU1001-182	1	KU1001	Olah Raga	2	W	1	KU1001	Olah Raga	2	W
Ek2008-S1-STI-MA1201-182	2	MA1201	Kalkulus IIA	4	W	2	MA1201	Matematika IIA	4	W
Ek2008-S1-STI-FI1201-182	2	FI1201	Fisika Dasar IIA	4	W	2	FI1201	Fisika Dasar IIA	4	W
Ek2008-S1-STI-KI1201-182	2	KI1201	Kimia Dasar II A	3	W	2	KI1202	Kimia Dasar IIB	2	W
Ek2008-S1-STI-KU1201-182	2	KU1201	Sistem Alam & Semesta	2	W	2	KU1201	Pengantar Rekayasa & Desain II	2	W
Ek2008-S1-STI-KU1071(2)-182	2	KU1071	Pengenalan Teknologi Informasi A	2	W	2	IF1210	Dasar Pemrograman	2	W
Ek2008-S1-STI-KU1011-182	2	KU1011	Tata Tulis Karya Ilmiah	2	W	2	KU1011	Tata Tulis Karya Ilmiah	2	W
Ek2008-S1-STI-EL1092-182	2	EL1092	Dasar Rangkaian Elektrik	2	W	2	EL1200	Pengantar Analisis Rangkaian	2	W
Ek2008-S1-STI-MA2072-182	3	MA2072	Matematika Teknik I	3	W	3	II2110	Matematika STI	3	W
Ek2008-S1-STI-II2091-182	3	II2091	Struktur Diskrit	3	W	3	II2110	Matematika STI	3	W
Ek2008-S1-STI-II2092-182	3	II2092	Probabilitas & Statistika	3	W	3	II2111	Probabilitas dan Statistik	3	W
Ek2008-S1-STI-EL2095-182	3	EL2095	Sistem Digital	3	W	3	EL2142	Sistem Digital & Mikroprosesor	4	W
Ek2008-S1-STI-EL2195-182	3	EL2195	Praktikum Sistem Digital	1	W	3	EL2142	Sistem Digital & Mikroprosesor	4	W
Ek2008-S1-STI-IF2031-182	3	IF2031	Algoritma & Struktur Data	3	W	3	IF2111	Algoritma dan Struktur Data	3	W
Ek2008-S1-STI-TI2106-182	3	TI2106	Manajemen (II)	3	W	3	TI3005	Organisasi & Manajemen Perusahaan Industri	2	W
Ek2008-S1-STI-TI3106-182	5	TI3106	Perilaku Organisasi (II)	3	W	3	TI3005	Organisasi & Manajemen Perusahaan Industri	2	W
Ek2008-S1-STI-IF2034-182	4	IF2034	Basis Data	3	W	3	IF2140	Pemodelan Basis Data	3	W
Ek2008-S1-STI-IF2033-182	4	IF2033	Pemrograman Berorientasi Objek	3	W	4	IF2210	Pemrograman Berorientasi Objek	3	W
Ek2008-S1-STI-EL2010-182	4	EL2010	Organisasi & Arsitektur Komputer	3	W	4	EL2244	Sistem & Arsitektur Komputer	3	W
Ek2008-S1-STI-II3042-182	6	II3042	Manajemen Sumber Daya Informasi	3	W	4	II2220	Manajemen Sumber Daya STI	3	W
Ek2008-S1-STI-II3044-182	6	II3044	Manajemen Proyek	3	W	4	II2221	Manajemen Proyek STI	3	W
Ek2008-S1-STI-II2040-182	4	II2040	Analisis & Perancangan Sistem	3	W	4	II2240	Analisis Kebutuhan Sistem	3	W
Ek2008-S1-STI-II2094-182	4	II2094	Sinyal & Sistem	3	W	\N	\N	\N	\N	\N
Ek2008-S1-STI-II3097-182	5	II3097	Jaringan Komputer	3	W	4	II2230	Jaringan Komputer	3	W
Ek2008-S1-STI-IF2036-182	4	IF2036	Rekayasa Perangkat Lunak	3	W	5	IF3152	Rekayasa Perangkat Lunak	3	W
Ek2008-S1-STI-IF3055-182	5	IF3055	Sistem Operasi	4	W	5	II3130	Sistem Operasi	3	W
Ek2008-S1-STI-II3063-182	5	II3063	Sistem Multimedia	3	W	5	II3150	Sistem Multimedia	3	W
Ek2008-S1-STI-II3090-182	5	II3090	Analisis Kebutuhan Informasi	3	W	5	II3121	Analisis Kebutuhan Enterprise	3	W
Ek2013-S1-STI-II3160-182	\N	\N	\N	\N	\N	5	II3160	Pemrograman Integratif	3	W
Ek2013-S1-STI-II3120-182	\N	\N	\N	\N	\N	5	II3120	Layanan Sistem dan Teknologi Informasi	3	W
Ek2008-S1-STI-II3061-182	5	II3061	Rekayasa Sistem	3	W	6	II3240	Rekayasa Sistem dan Teknologi Informasi	3	W
Ek2008-S1-STI-II3099-182	6	II3099	Interaksi Manusia & Komputer	3	W	6	II3231	Interaksi Manusia dg Komputer & Antarmuka	3	W
Ek2008-S1-STI-II4065-182	8	II4065	Pengembangan Aplikasi Mobile	3	P	6	II3260	Platform dan Pengembangan Aplikasi Mobile	3	W
Ek2008-S1-STI-II4044-182	8	II4044	Sistem Informasi Intellijen	3	P	6	EL4233	Dasar Sistem & Kendali Cerdas	3	W
Ek2008-S1-STI-II3062-182	6	II3062	Keamanan Informasi	3	W	6	II3230	Keamanan Informasi	3	W
Ek2008-S1-STI-II4043-182	8	II4043	Sistem Informasi Enterprise	3	P	6	II3220	Arsitektur Enterprise	3	W
Ek2008-S1-STI-II4091-182	8	II4091	Kerja Praktek	2	W	7	II4090	Kerja Praktek	2	W
Ek2008-S1-STI-II4096-182	7	II4096	Tugas Akhir I & Seminar	2	W	7	II4091	Tugas Akhir 1 & Seminar	2	W
Ek2008-S1-STI-II3098-182	7	II3098	Etika & Hukum Cyber	2	W	7	II4470	Hukum & Etika Teknologi Informasi	2	W
Ek2013-S1-STI-II4471-182	\N	\N	Keprofesian	\N	P	7	II4471	Kapita Selekta STI	2	W
Ek2008-S1-STI-II4099-182	8	II4099	Tugas Akhir II	4	W	8	II4092	Tugas Akhir 2	4	W
Ek2008-S1-STI-KU206X-182	6	KU206X	Agama dan Etika	2	W	8	KU206X	Agama dan Etika	2	W
Ek2008-S1-STI-KU2071-182	8	KU2071	Pancasila dan Kewarganegaraan	2	W	8	KU2071	Pancasila dan Kewarganegaraan	2	W
Ek2013-S1-STI-BI2001 -182	\N	\N	Lingkungan	\N	W	8	BI2001	Pengetahuan Lingkungan	2	W
Ek2008-S1-STI-II3094-182	6	II3094	Komunikasi Antar Personal	2	W	8	II4472	Komunikasi Interpersonal	2	W
\.


--
-- Data for Name: jawaban; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.jawaban (id_jawaban, id_thread, jawaban, favorit, userid, datecreated) FROM stdin;
2	1	Caranya coba kamu mulai fokus ke satu hal dulu :))	7	2	2019-04-29 06:32:03.375
3	1	It's okay brooh you dont have to be worry	7	4	2019-04-29 06:32:24.703
4	2	Kick ae ude	2	1	2019-04-29 06:33:02.867
5	2	Tampol sumpee	3	3	2019-04-29 06:33:16.774
6	3	Besok kalo ga hujan ya	6	13	2019-04-29 06:33:57.891
7	3	WKWKWK besok kalo ga hujan, canda ae	3	4	2019-04-29 06:34:13.248
8	4	CANDA KAMU, MAS!!!	2	13	2019-04-29 06:34:56.46
9	5	sd	2	13	2019-04-29 06:35:54.508
10	5	Maaf kepencet, sok ae isi	6	13	2019-04-29 06:36:09.659
11	6	Tyda	11	13	2019-04-29 06:36:30.911
12	6	WADUUUUU!!	6	3	2019-04-29 06:36:42.846
1	1	Ya kamu tu belajarnya yang bener dong hmmm, bambang!!	4	1	2019-04-29 06:31:41.608
\.


--
-- Data for Name: kurikulum; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.kurikulum (id_kurikulum, kode_prodi, nama_fakultas, nama_prodi, tahun_kurikulum, semester, kode_matkul, nama_matkul, sks, wajib, pilihan) FROM stdin;
Kur2013-S1-STI-MA1101-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	MA1101	Matematika IA	4	t	f
Kur2013-S1-STI-MA1201-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	MA1201	Matematika IIA	4	t	f
Kur2013-S1-STI-FI1101-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	FI1101	Fisika Dasar IA	4	t	f
Kur2013-S1-STI-FI1201-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	FI1201	Fisika Dasar IIA	4	t	f
Kur2013-S1-STI-KI1102-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	KI1102	Kimia Dasar IB	2	t	f
Kur2013-S1-STI-KI1202-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	KI1202	Kimia Dasar IIB	2	t	f
Kur2013-S1-STI-KU1101-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	KU1101	Pengantar Rekayasa & Desain I	2	t	f
Kur2013-S1-STI-KU1201-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	KU1201	Pengantar Rekayasa & Desain II	2	t	f
Kur2013-S1-STI-KU1072-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	KU1072	Pengenalan Teknologi Informasi B	2	t	f
Kur2013-S1-STI-KU1011-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	KU1011	Tata Tulis Karya Ilmiah	2	t	f
Kur2013-S1-STI-KU102X-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	KU102X	Bahasa Inggris (KU1021/1022/1023)	2	t	f
Kur2013-S1-STI-EL1200-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	EL1200	Pengantar Analisis Rangkaian	2	t	f
Kur2013-S1-STI-KU1001-182	182	STEI	Sistem dan Teknologi Informasi	2013	1	KU1001	Olah Raga	2	t	f
Kur2013-S1-STI-IF1210-182	182	STEI	Sistem dan Teknologi Informasi	2013	2	IF1210	Dasar Pemrograman	2	t	f
Kur2013-S1-STI-II2110-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	II2110	Matematika STI	3	t	f
Kur2013-S1-STI-II2230-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	II2230	Jaringan Komputer	3	t	f
Kur2013-S1-STI-II2111-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	II2111	Probabilitas dan Statistik	3	t	f
Kur2013-S1-STI-EL2244-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	EL2244	Sistem & Arsitektur Komputer	3	t	f
Kur2013-S1-STI-EL2142-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	EL2142	Sistem Digital & Mikroprosesor	4	t	f
Kur2013-S1-STI-II2220-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	II2220	Manajemen Sumber Daya STI	3	t	f
Kur2013-S1-STI-TI3005-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	TI3005	Organisasi & Manajemen Perusahaan Industri	2	t	f
Kur2013-S1-STI-II2221-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	II2221	Manajemen Proyek STI	3	t	f
Kur2013-S1-STI-IF2140-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	IF2140	Pemodelan Basis Data	3	t	f
Kur2013-S1-STI-II2240-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	II2240	Analisis Kebutuhan Sistem	3	t	f
Kur2013-S1-STI-IF2111-182	182	STEI	Sistem dan Teknologi Informasi	2013	3	IF2111	Algoritma dan Struktur Data	3	t	f
Kur2013-S1-STI-IF2210-182	182	STEI	Sistem dan Teknologi Informasi	2013	4	IF2210	Pemrograman Berorientasi Objek	3	t	f
Kur2013-S1-STI-II3150-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	II3150	Sistem Multimedia	3	t	f
Kur2013-S1-STI-II3260-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	II3260	Platform dan Pengembangan Aplikasi Mobile	3	t	f
Kur2013-S1-STI-II3130-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	II3130	Sistem Operasi	3	t	f
Kur2013-S1-STI-II3230-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	II3230	Keamanan Informasi	3	t	f
Kur2013-S1-STI-II3160-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	II3160	Pemrograman Integratif	3	t	f
Kur2013-S1-STI-II3231-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	II3231	Interaksi Manusia dg Komputer & Antarmuka	3	t	f
Kur2013-S1-STI-II3120-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	II3120	Layanan Sistem dan Teknologi Informasi	3	t	f
Kur2013-S1-STI-EL4233-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	EL4233	Dasar Sistem & Kendali Cerdas	3	t	f
Kur2013-S1-STI-II3121-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	II3121	Analisis Kebutuhan Enterprise	3	t	f
Kur2013-S1-STI-II3220-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	II3220	Arsitektur Enterprise	3	t	f
Kur2013-S1-STI-IF3152-182	182	STEI	Sistem dan Teknologi Informasi	2013	5	IF3152	Rekayasa Perangkat Lunak	3	t	f
Kur2013-S1-STI-II3240-182	182	STEI	Sistem dan Teknologi Informasi	2013	6	II3240	Rekayasa Sistem dan Teknologi Informasi	3	t	f
Kur2013-S1-STI-II4090-182	182	STEI	Sistem dan Teknologi Informasi	2013	7	II4090	Kerja Praktek	2	t	f
Kur2013-S1-STI-KU206X-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	KU206X	Agama dan Etika	2	t	f
Kur2013-S1-STI-II4091-182	182	STEI	Sistem dan Teknologi Informasi	2013	7	II4091	Tugas Akhir 1 & Seminar	2	t	f
Kur2013-S1-STI-KU2071-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	KU2071	Pancasila dan Kewarganegaraan	2	t	f
Kur2013-S1-STI-II4470-182	182	STEI	Sistem dan Teknologi Informasi	2013	7	II4470	Hukum & Etika Teknologi Informasi	2	t	f
Kur2013-S1-STI-BI2001-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	BI2001	Pengetahuan Lingkungan	2	t	f
Kur2013-S1-STI-II4471-182	182	STEI	Sistem dan Teknologi Informasi	2013	7	II4471	Kapita Selekta STI	2	t	f
Kur2013-S1-STI-II4092-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4092	Tugas Akhir 2	4	t	f
Kur2013-S1-STI-II4472-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4472	Komunikasi Interpersonal	2	t	f
Kur2013-S1-STI-EL4125-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	EL4125	Pengolahan Citra Digital	3	f	t
Kur2013-S1-STI-EL4126-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	EL4126	Robotika	3	f	t
Kur2013-S1-STI-EL4127-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	EL4127	Arsitektur & Komputasi Paralel	3	f	t
Kur2013-S1-STI-EL4132-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	EL4132	Teknik Biomedika	3	f	t
Kur2013-S1-STI-EP4072-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	EP4072	SCADA dan Manajemen Energi	3	f	t
Kur2013-S1-STI-IF4021-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	IF4021	Pemodelan dan Simulasi	3	f	t
Kur2013-S1-STI-IF4050-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	IF4050	Pembangunan Perangkat Lunak Berorientasi Service	3	f	t
Kur2013-S1-STI-IF4051-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	IF4051	Rekayasa Perangkat Lunak Berbasis Komponen	3	f	t
Kur2013-S1-STI-IF4062-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	IF4062	Pembangunan Aplikasi berbasis Grafik 3D	3	f	t
Kur2013-S1-STI-IF4071-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	IF4071	Pembelajaran Mesin	3	f	t
Kur2013-S1-STI-II4021-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4021	Sistem Pendukung Pengambilan Keputusan	2	f	t
Kur2013-S1-STI-II4022-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4022	Audit Teknologi Informasi	2	f	t
Kur2013-S1-STI-II4031-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4031	Kriptografi dan Koding	2	f	t
Kur2013-S1-STI-II4032-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4032	Analisis & Perancangan Kinerja Sistem	2	f	t
Kur2013-S1-STI-II4033-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4033	Forensik Digital	2	f	t
Kur2013-S1-STI-II4034-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4034	Pemrosesan Bahasa Alami dan Ucapan	2	f	t
Kur2013-S1-STI-II4051-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4051	Rekayasa Sistem Multimedia	2	f	t
Kur2013-S1-STI-II4061-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4061	Manajemen Data	2	f	t
Kur2013-S1-STI-II4062-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4062	Data Warehouse & Business Intelligence	2	f	t
Kur2013-S1-STI-II4071-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	II4071	Keprofesian STI	2	f	t
Kur2013-S1-STI-KU4095-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	KU4095	Kewirausahaan	2	f	t
Kur2013-S1-STI-MB4045-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	MB4045	Manajemen Investasi	3	f	t
Kur2013-S1-STI-MB4054-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	MB4054	Manajemen Kualitas	3	f	t
Kur2013-S1-STI-MR3002-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	MR3002	Manajemen Teknologi	3	f	t
Kur2013-S1-STI-MR4107-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	MR4107	Manajemen Kontrak	3	f	t
Kur2013-S1-STI-TI3004-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	TI3004	Ekonomi Teknik	2	f	t
Kur2013-S1-STI-TI4109-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	TI4109	Managemen Keuangan	3	f	t
Kur2013-S1-STI-TI4203-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	TI4203	Manajemen Pemasaran	3	f	t
Kur2013-S1-STI-TI4204-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	TI4204	Manajemen Sumber Daya Manusia	3	f	t
Kur2013-S1-STI-TL4002-182	182	STEI	Sistem dan Teknologi Informasi	2013	8	TL4002	Rekayasa Lingkungan	3	f	t
Kur2008-S1-STI-MA1101-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	MA1101	Kalkulus IA	4	t	f
Kur2008-S1-STI-FI1101-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	FI1101	Fisika Dasar IA	4	t	f
Kur2008-S1-STI-KI1101-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	KI1101	Kimia Dasar I A	3	t	f
Kur2008-S1-STI-KU1101-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	KU1101	Konsep Pengembangan Ilmu Pengetahuan	2	t	f
Kur2008-S1-STI-KU1071-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	KU1071	Pengenalan Teknologi Informasi A	2	t	f
Kur2008-S1-STI-KU102X-182	182	STEI	Sistem dan Teknologi Informasi	2008	1	KU102X	Bahasa Inggris	2	t	f
Kur2008-S1-STI-EL1092-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	EL1092	Dasar Rangkaian Elektrik	2	t	f
Kur2008-S1-STI-MA1201-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	MA1201	Kalkulus IIA	4	t	f
Kur2008-S1-STI-FI1201-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	FI1201	Fisika Dasar IIA	4	t	f
Kur2008-S1-STI-KI1201-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	KI1201	Kimia Dasar II A	3	t	f
Kur2008-S1-STI-KU1201-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	KU1201	Sistem Alam & Semesta	2	t	f
Kur2008-S1-STI-KU1001-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	KU1001	Olah Raga	2	t	f
Kur2008-S1-STI-KU1011-182	182	STEI	Sistem dan Teknologi Informasi	2008	2	KU1011	Tata Tulis Karya Ilmiah	2	t	f
Kur2008-S1-STI-MA2072-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	MA2072	Matematika Teknik I	3	t	f
Kur2008-S1-STI-II2091-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	II2091	Struktur Diskrit	3	t	f
Kur2008-S1-STI-II2092-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	II2092	Probabilitas & Statistika	3	t	f
Kur2008-S1-STI-EL2095-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	EL2095	Sistem Digital	3	t	f
Kur2008-S1-STI-EL2195-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	EL2195	Praktikum Sistem Digital	1	t	f
Kur2008-S1-STI-IF2031-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	IF2031	Algoritma & Struktur Data	3	t	f
Kur2008-S1-STI-TI2106-182	182	STEI	Sistem dan Teknologi Informasi	2008	3	TI2106	Manajemen (II)	3	t	f
Kur2008-S1-STI-IF2033-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	IF2033	Pemrograman Berorientasi Objek	3	t	f
Kur2008-S1-STI-IF2034-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	IF2034	Basis Data	3	t	f
Kur2008-S1-STI-IF2036-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	IF2036	Rekayasa Perangkat Lunak	3	t	f
Kur2008-S1-STI-EL2010-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	EL2010	Organisasi & Arsitektur Komputer	3	t	f
Kur2008-S1-STI-II2040-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	II2040	Analisis & Perancangan Sistem	3	t	f
Kur2008-S1-STI-II2094-182	182	STEI	Sistem dan Teknologi Informasi	2008	4	II2094	Sinyal & Sistem	3	t	f
Kur2008-S1-STI-IF3055-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	IF3055	Sistem Operasi	4	t	f
Kur2008-S1-STI-II3061-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	II3061	Rekayasa Sistem	3	t	f
Kur2008-S1-STI-II3063-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	II3063	Sistem Multimedia	3	t	f
Kur2008-S1-STI-II3090-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	II3090	Analisis Kebutuhan Informasi	3	t	f
Kur2008-S1-STI-II3097-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	II3097	Jaringan Komputer	3	t	f
Kur2008-S1-STI-TI3106-182	182	STEI	Sistem dan Teknologi Informasi	2008	5	TI3106	Perilaku Organisasi (II)	3	t	f
Kur2008-S1-STI-KU206X-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	KU206X	Agama dan Etika	2	t	f
Kur2008-S1-STI-II3042-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3042	Manajemen Sumber Daya Informasi	3	t	f
Kur2008-S1-STI-II3044-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3044	Manajemen Proyek	3	t	f
Kur2008-S1-STI-II3062-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3062	Keamanan Informasi	3	t	f
Kur2008-S1-STI-II3094-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3094	Komunikasi Antar Personal	2	t	f
Kur2008-S1-STI-II3099-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3099	Interaksi Manusia & Komputer	3	t	f
Kur2008-S1-STI-II3292-182	182	STEI	Sistem dan Teknologi Informasi	2008	6	II3292	Pilihan Bebas /Minor	3	f	t
Kur2008-S1-STI-II4096-182	182	STEI	Sistem dan Teknologi Informasi	2008	7	II4096	Tugas Akhir I & Seminar	2	t	f
Kur2008-S1-STI-II3098-182	182	STEI	Sistem dan Teknologi Informasi	2008	7	II3098	Etika & Hukum Cyber	2	t	f
Kur2008-S1-STI-II4149-182	182	STEI	Sistem dan Teknologi Informasi	2008	7	II4149	MK Pilihan II	6	f	t
Kur2008-S1-STI-YYYYYY-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	YYYYYY	Kuliah Manajemen Wajib ITB	2	t	f
Kur2008-S1-STI-II4099-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4099	Tugas Akhir II	4	f	t
Kur2008-S1-STI-II4091-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4091	Kerja Praktek	2	f	t
Kur2008-S1-STI-KU2071-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	KU2071	Pancasila dan Kewarganegaraan	2	f	t
Kur2008-S1-STI-EL2040-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL2040	Elektronika	3	f	t
Kur2008-S1-STI-EL2090-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL2090	Medan Elektromagnetik	3	f	t
Kur2008-S1-STI-EL2140-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL2140	Praktikum Elektronika	1	f	t
Kur2008-S1-STI-EL3092-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL3092	Pengolahan Sinyal Digital	3	f	t
Kur2008-S1-STI-EL3096-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL3096	Sistem Mikroprosesor & Praktikum	3	f	t
Kur2008-S1-STI-EL3192-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EL3192	Praktikum Pengolahan Sinyal Digital	1	f	t
Kur2008-S1-STI-EP3071-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EP3071	Mesin Mesin Elektrik	3	f	t
Kur2008-S1-STI-EP3072-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	EP3072	Elektronika Daya	3	f	t
Kur2008-S1-STI-ET2080-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	ET2080	Jaringan Telekomunikasi	3	f	t
Kur2008-S1-STI-ET3081-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	ET3081	Sistem Komunikasi	3	f	t
Kur2008-S1-STI-FK3206-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	FK3206	Fisiologi Olahraga	2	f	t
Kur2008-S1-STI-IF3035-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3035	Sistem Basis Data	3	f	t
Kur2008-S1-STI-IF3037-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3037	Rekayasa Perangkat Lunak Lanjut	3	f	t
Kur2008-S1-STI-IF3038-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3038	Pemrograman Internet	3	f	t
Kur2008-S1-STI-IF3053-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3053	Grafika Komputer & Visualisasi	3	f	t
Kur2008-S1-STI-IF3054-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3054	Intelegensia Buatan	3	f	t
Kur2008-S1-STI-IF3056-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3056	Sistem Terdistribusi	3	f	t
Kur2008-S1-STI-IF3058-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	IF3058	Kriptografi	3	f	t
Kur2008-S1-STI-II3065-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II3065	Pengolahan Sinyal dalam Waktu Diskrit	3	f	t
Kur2008-S1-STI-II4040-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4040	Intelligent Bisnis	3	f	t
Kur2008-S1-STI-II4041-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4041	Perencanaan Sistem Informasi	3	f	t
Kur2008-S1-STI-II4042-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4042	Sistem Pendukung Keputusan	3	f	t
Kur2008-S1-STI-II4043-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4043	Sistem Informasi Enterprise	3	f	t
Kur2008-S1-STI-II4044-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4044	Sistem Informasi Intellijen	3	f	t
Kur2008-S1-STI-II4045-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4045	Audit Sistem Informasi	3	f	t
Kur2008-S1-STI-II4060-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4060	Teori dan Sistem Antrian	3	f	t
Kur2008-S1-STI-II4061-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4061	Rekayasa Sistem Multimedia	3	f	t
Kur2008-S1-STI-II4062-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4062	Kriptografi dan Koding	3	f	t
Kur2008-S1-STI-II4063-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4063	Forensik Komputer dan Jaringan	3	f	t
Kur2008-S1-STI-II4064-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4064	Analisis dan Design Kinerja Sistem	3	f	t
Kur2008-S1-STI-II4065-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4065	Pengembangan Aplikasi Mobile	3	f	t
Kur2008-S1-STI-II4093-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4093	Pengembangan Keprofesian/Komunitas A	2	f	t
Kur2008-S1-STI-II4097-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4097	Jaringan Komputer Lanjut	3	f	t
Kur2008-S1-STI-II4140-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4140	Paradigma Pemrograman	3	f	t
Kur2008-S1-STI-II4142-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4142	Kualitas Data	3	f	t
Kur2008-S1-STI-II4144-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4144	Kerekayasaan Informasi	4	f	t
Kur2008-S1-STI-II4160-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4160	Jaringan Informasi	3	f	t
Kur2008-S1-STI-II4162-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4162	Sistem Probabilistik dan Stokastik	3	f	t
Kur2008-S1-STI-II4164-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4164	Pemrograman Internet / Web	3	f	t
Kur2008-S1-STI-II4166-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4166	Keamanan Informasi Lanjut	3	f	t
Kur2008-S1-STI-II4193-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4193	Pengembangan Keprofesian/Komunitas B	3	f	t
Kur2008-S1-STI-II4293-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	II4293	Pengembangan Keprofesian/Komunitas C	4	f	t
Kur2008-S1-STI-TI2105-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI2105	Pengantar Ekonomika	2	f	t
Kur2008-S1-STI-TI3005-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI3005	Ekonomi Teknik	2	f	t
Kur2008-S1-STI-TI4002-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI4002	Manajemen Rekayasa Industri	3	f	t
Kur2008-S1-STI-TI4005-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI4005	Manajemen Proyek	3	f	t
Kur2008-S1-STI-TI4102-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI4102	Kewirausahaan & Pengembangan Enterprais	3	f	t
Kur2008-S1-STI-TI4205-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TI4205	Manajemen Inovasi	3	f	t
Kur2008-S1-STI-TL2105-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TL2105	Kesehatan Lingkungan	3	f	t
Kur2008-S1-STI-TL4001-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TL4001	Metodologi AMDAL	3	f	t
Kur2008-S1-STI-TL4002-182	182	STEI	Sistem dan Teknologi Informasi	2008	8	TL4002	Rekayasa Lingkungan	3	f	t
\.


--
-- Data for Name: matkul; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.matkul (id_matkul, kode_matkul, nama_matkul) FROM stdin;
3	II3240	Matkul Paling Menyenangkan Yaitu Reksti
1	II3230	Keamanan Informasi
2	II3220	Arsitektur Enterprise
\.


--
-- Data for Name: prodi; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.prodi (kode_prodi, nama_fakultas, nama_prodi) FROM stdin;
182	STEI	Sistem dan Teknologi Informasi
132	STEI	Teknik Elektro
135	STEI	Teknik Informatika
180	STEI	Teknik Tenaga Listrik
181	STEI	Teknik Telekomunikasi
183	STEI	Teknik Biomedis
\.


--
-- Data for Name: sap; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.sap (id_sap, kode_matkul, minggu, topik, subtopik, capaian_belajar, sumber_materi) FROM stdin;
1	MA1101	1	Pendahuluan	Informasi perkuliahan\r\nBilangan real \r\nPertaksamaan dan nilai mutlak	Tidak ada data	Tidak ada data
2	MA1101	2	Pendahuluan dan Limit	Sistem Koordinat\r\nGrafik Persamaan \r\nFungsi dan grafiknya\r\nOperasi pada fungsi\r\nFungsi trigonometri	Tidak ada data	Tidak ada data
3	MA1101	3	Limit	Pengantar limit\r\nLimit Fungsi\r\nTeorema limit\r\nLimit Fungsi Trigonometri\r\nLimit tak hingga dan limit di tak hingga	Tidak ada data	Tidak ada data
4	MA1101	4	Limit dan Turunan	Kekontinuan\r\nDua masalah dengan satu tema\r\nTurunan\r\nAturan penetuan turunan	Tidak ada data	Tidak ada data
5	MA1101	5	Turunan	turunan fungsi trigonometri\r\nAturan rantai,\r\nturunan tingkat tinggi, turunan fungsi implisit,	Tidak ada data	Tidak ada data
6	MA1101	6	Turunan	Laju yang berkaitan, diferensial dan hampiran\r\nMaksimum dan minimum,\r\nKemonotonan dan kecekungan	Tidak ada data	Tidak ada data
7	MA1101	7	Penggunaan Turunan	ekstrim local dan ekstrim pada selang buka,\r\nPemodelan Matematika \r\nGrafik fungsi dengan menggunakan kalkulus	Tidak ada data	Tidak ada data
8	MA1101	8	Penggunaan Turunan	Teorema Nilai Rata-rata Turunan, \r\nantiturunan,\r\npengantar persamaan diferensial	Tidak ada data	Tidak ada data
9	MA1101	9	Review\r\nUTS	Tidak ada data	Tidak ada data	Tidak ada data
10	MA1101	10	Integral	Luas daerah\r\nIntegral tentu, \r\nTeorema Dasar Kalkulus Pertama\r\nTeorema Dasar Kalkulus Kedua dan metode substitusi	Tidak ada data	Tidak ada data
11	MA1101	11	Integral	Teorema Nilai Rata-rata Integral, \r\nIntegrasi numerik\r\nLuas daerah pada bidang benda putar	Tidak ada data	Tidak ada data
12	MA1101	12	Penggunaan integral	Volume benda putar: metode lempeng sejajar, cakram, dan cincin\r\nVolume benda putar: metode kulit tabung\r\nKerja (bagian tekanan fluida tidak ikut)\r\nMomen dan pusat massa	Tidak ada data	Tidak ada data
13	MA1101	13	Fungsi Transenden	Fungsi logaritma natural, fungsi invers dan turunannya\r\nFungsi eksponen\r\nFungsi eksponen dan logaritma umum	Tidak ada data	Tidak ada data
14	MA1101	14	Fungsi Transenden	pertumbuhan dan peluruhan eksponensial, \r\npersamaan diferensial orde satu	Tidak ada data	Tidak ada data
15	MA1101	15	Fungsi Transenden	Fungsi invers trigonometri dan turunannya, \r\nfungsi hiperbolik dan inversnya (hanya sinh dan cosh)\r\nPersiapan UAS	Tidak ada data	Tidak ada data
16	MA1101	16	Ujian Akhir Semester	Tidak ada data	Tidak ada data	Tidak ada data
17	MA1201	1	Teknik Pengintegralan	7.1 Aturan Dasar Pengintegralan.\r\n7.2 Integral Parsial. \r\n7.3 Integral Trigonometri	Tidak ada data	Tidak ada data
18	MA1201	2	Teknik Pengintegralan	7.4 Substitusi yang Merasionalkan. \r\n7.5 Integrasi Fungsi Rasional. \r\n7.6 Strategi Pengintegralan.	Tidak ada data	Tidak ada data
19	MA1201	3	Bentuk Taktentu dan Integral tak Wajar	8.1 Bentuk Tak tentu jenis 0/0. \r\n8.2 Bentuk Tak tentu Lainnya. \r\n8.3 Integral Tak wajar: Limit Tak hingga dari Integral. \r\n8.4 Integral Tak wajar: Integran tak hingga.	Tidak ada data	Tidak ada data
20	MA1201	4	Deret Tak Hingga	9.1 Barisan Tak hingga. 9.2 Deret Tak hingga. 9.3 Deret Positif: Uji Integral. \r\n9.4 Deret Positif: Uji-uji lainnya.	Tidak ada data	Tidak ada data
21	MA1201	5	Deret Tak Hingga	9.5 Deret Berganti Tanda, Konvergensi Mutlak, dan Konvergensi Bersyarat. \r\n9.6 Deret Pangkat. \r\n9.7 Operasi Pada Deret Pangkat.	Tidak ada data	Tidak ada data
22	MA1201	6	Deret Tak hingga\r\nIrisan Kerucut dan Koordinat Polar	9.8 Deret Taylor dan Deret Maclaurin. \r\n9.9 Hampiran Taylor Untuk Sebuah Fungsi. 10.1 & 10.2: Parabola, Elips dan hiperbola (hanya persamaan dalam &#119909;&#119910; dan grafiknya)	Tidak ada data	Tidak ada data
23	MA1201	7	Geometri di Bidang dan Ruang	10.4 Representasi Parametrik dari Kurva di Bidang\r\n11.2 Vektor. \r\n11.3 Hasil Kali Titik. 11.4 Hasil Kali Silang	Tidak ada data	Tidak ada data
24	MA1201	8	Geometri di Bidang dan Ruang	11.5 Fungsi Bernilai Vektor dan Gerak Kurvilinear. \r\n11.6 Garis dan Garis Singgung di Ruang. \r\n11.8 Permukaan di Ruang.	Tidak ada data	Tidak ada data
25	MA1201	9	Turunan di ruang berdimensi n	Keterdiferensialan, turunan berarah dan gradient, aturan rantai, bidang singgung dan hampiran	Tidak ada data	Tidak ada data
26	MA1201	10	Turunan di Ruang Ber- dimensi n\r\nUTS	12.1 Fungsi dengan Dua Peubah atau Lebih. \r\n12.2 Turunan Parsial.\r\nReview & persiapan Ujian 1	Tidak ada data	Tidak ada data
27	MA1201	11	Turunan di Ruang Ber- dimensi n	12.3 Limit dan Kekontinuan \r\n12.4 Keterdiferensialan 12.5 Turunan Berarah dan Gradien \r\n12.6 Aturan Rantai.	Tidak ada data	Tidak ada data
28	MA1201	12	Turunan di Ruang Ber- dimensi n	12.7 Bidang Singgung dan Hampiran.\r\n12.8 Maksimum dan Minimum. 12.9 Metode Lagrange.	Tidak ada data	Tidak ada data
29	MA1201	13	Integral Lipat	13.1 Integral Lipat Dua atas Persegi Panjang. 13.2 Integral Berulang. 13.3 Integral Lipat Dua atas Daerah Bukan Persegi Panjang.	Tidak ada data	Tidak ada data
30	MA1201	14	Integral Lipat	10.5 Sistem Koordinat Polar.\r\n13.4 Integral Lipat Dua dalam Koordinat Polar. 13.5 Penerapan Integral Lipat Dua.	Tidak ada data	Tidak ada data
31	MA1201	15	Persamaan diferensial	15.1 Persamaan Diferensial Linear Homogen. \r\n15.2 Persamaan Diferensial Linear Tak homogen Orde Dua.	Tidak ada data	Tidak ada data
32	MA1201	16	Persamaan Diferensial\r\nUAS	15.3 Penerapan Persamaan Diferensial Orde Dua.\r\nReview & persiapan Ujian Akhir Semester	Tidak ada data	Tidak ada data
33	FI1101	1	Kinematika  Benda Titik	Overview Fisika, Review\r\nVektor, Review Kinematika\r\nBenda titik	Setelah mengikuti kuliah\r\nmahasiswa diharapkan\r\nmemahami dan mampu\r\nmenerap-kan konsep-konsep\r\nkinematika \r\ndan dapat melakukan\r\nanalisa dimensi.\r\n	K
34	FI1101	2	Kinematika	Kecepatan dan percepatan. \r\nPersamaan Kinematika,\r\nGerak 1 dimensi, gerak\r\n2-3 dimensi, dan\r\nkecepatan relatif.	Setelah mengikuti kuliah\r\nmahasiswa diharapkan\r\nmemahami dan dapat\r\nmenggunakan konsep-konsep\r\nvektor,\r\n kinematika dan melakukan\r\nanalisa grafik dalam\r\nmenyelesaikan dan\r\nmenganalisa  gerak 1, 2\r\ndan 3 dimensi.	K
35	FI1101	3	Dinamika benda titik	Inersia, \r\nHukum Newton I, II danIII. \r\nGaya dangerak Aplikasi\r\nHukum Newton: Benda dalam\r\nkeadaan seimbang dan\r\ndinamik, \r\nDiagram gaya.	Setelah mengikuti kuliah\r\nini mahasiswa diharapkan\r\nmemahami hukum-hukum\r\nNewton untuk gerak \r\ndi bidang datar dan mampu\r\nmenggambarkan diagram\r\ngaya dan menerapkan\r\nhubungan gaya dan gerak\r\nuntuk berbagai keadaan.	K,R
36	FI1101	4	Dinamika benda titik	Gaya gesekan, gaya\r\nnormal, gaya tegangan,\r\ngaya gravitasi Newton.\r\nAnalisis benda yang\r\ntergantung atau\r\nbertumpuk, benda dalam\r\nkatrol, gerak melingkar,\r\ndan gaya sentripetal.	Setelah mengikuti kuliah\r\nini mahasiswa diharapkan\r\nmampu menyelesaikan\r\npersoalan \r\ndinamika system\r\nbenda titik: sistem benda\r\nterhubung katrol,  benda\r\nbertumpuk dan dinamika\r\ngerak melingkar	K,R
37	FI1101	5	Usaha dan energi	Definisi usaha, energi\r\nkinetik, dan teorema\r\nusaha-energi kinetik.\r\nEnergi potensial. \r\nGaya konsevatif. Hukum\r\nkekekalan energi. Gaya\r\ntak konservatif.	Setelah kuliah ini\r\nmahasiswa diharapkan\r\nmampu menyelesaikan\r\npersoalan mekanika\r\n dengan konsep\r\nusaha-energi kinetik.\r\nMemahami hubungan\r\ngayakonservatif, \r\nenergi potensial dan\r\nhukum kekekalan\r\nenergikinetik, serta\r\nmemahami penggunaan konsep \r\nkekekalan energi mekanik\r\njika gaya tak konservatif\r\nikut terlibat	K,R
38	FI1101	6	Momentum linear 	Momentum dan impuls,\r\nSistem partikel, hukum\r\nkekekalan momentum\r\nlinear, peristiwa\r\ntumbukan.\r\nGerak titik pusat massa	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMemahami hubungan impuls,\r\nperubahan momentum dan\r\ngaya rata-rata.\r\nMemahami konsep gerak\r\ntitik pusat massa.\r\nMenggunakan hukum\r\nkekekalan momentum linier.\r\nMampu menyelesaikan\r\npersoalan tumbukan.	K,R
39	FI1101	7	Benda Tegar	Statika dan\r\nDinamika rotasi sistem\r\nbenda tegar	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMampu menyelesaikan\r\npersoalan sederhana pada\r\nstatika sistem benda tegar.\r\nMemahami besaran-besaran\r\nrotasi dan memahami\r\nanalogi dinamika rotasi\r\ndan dinamika translasi.	K,R
40	FI1101	8	Benda Tegar	Dinamika Rotasi sistem\r\nbenda tegar,Gerak\r\nmenggelinding	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMampu menyelesaikan\r\npersoalan dinamika rotasi\r\nsistem benda tegar. \r\nMampu menyelesaikan\r\npersoalan dinamika gerak\r\nmenggelinding degan\r\nkonsep kekekalan energi.	K,R\r\n\r\nU
41	FI1101	9	Elastisitas dan osilasi	Stress, strain, modulus\r\nYoung, Modulus geser dan\r\nmodulus benda (bulk),\r\nOsilasi harmonik dan\r\nosilasi teredam, resonansi.	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMampu menyelesaikan\r\npersoalan elastisitas\r\nbahan dan\r\nOsilasi harmonik sederhana.\r\nMemahami osilasi teredam,\r\nterpaksa dan peristiwa\r\nresonansi.	K 
42	FI1101	10	Gelombang Mekanik	Gelombang tali,\r\nGelombang bunyi,\r\nSuperposisi gelombang,\r\nGelombang berdiri,\r\nResonansi, Efek Doppler	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMemahami konsep gelombang\r\nmekanik dan menerapkan\r\npersamaan gelombang\r\nmekanik pada masalah\r\nsederhana.  Memahami dan\r\ndapat menyelesaikan\r\npersoalan superposisi\r\ngelombang termasuk\r\ngelombang berdiri \r\ndan menerapkan efek\r\nDoppler pada persoalan\r\nsederhana.	K,R
43	FI1101	11	Fluida Statik dan Dinamik	Tekanan hidrostatik\r\nGaya Archimedes\r\nHukum Kontinuitas\r\nHukum Bernoulli	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMampu menerapkan konsep\r\ntekanan hidrostatik, dan\r\nGaya Archimedes pada\r\npersoalan sederhana.\r\nMampu menyelesaikan\r\npersoalan dinamika fluida\r\ndengan hukum Kontinuitas\r\ndan hukum Bernoulli	K,R
44	FI1101	12	Teori Kinetik Gas	Gas ideal, asas\r\nekipartisi energi, energi\r\ndalam, kapasitas kalor	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMemahami konsep gas ideal.\r\nMenganalisis dan\r\nmenyelesaikan persoalan\r\nsederhana gas ideal\r\ndengan menggunakan konsep\r\nasas ekipartisi energi,\r\nenergi dalam, kapasitas kalor.	K,R
45	FI1101	13	Hukum -0 dan 1 Thermodinamika	Keseimbangan termal,\r\nProses kuasistatik umum,\r\nProses khusus (isobar,\r\nisovolum, isotherm,\r\nadiabatik)\r\nDiagram (P,V), Usaha, \r\nHukum I Termodinamika	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMemahami hukumke -0.\r\nMemahami dan menerapkan\r\nhukum ke-1 \r\ntermodinamika untuk\r\nproses kuasistatik baik\r\nyang khusus ataupun umum.	K,R
46	FI1101	14	Hukum -2 Thermodinamika	Proses siklus,\r\nEfisiensi,\r\nKonsep hukum II\r\nThermodinamika:\r\nClausius\r\nKelvin\r\nCarnot\r\nPengenalan entropi	Setelah kuliah ini\r\nmahasiswa diharapkan:\r\nMengerti konsep mesin\r\npanas dan mesin\r\npendingin. Dapat\r\nmeghitung efisiensi dari\r\nproses siklus. Mengenal\r\nistilah entropi.	K 
47	FI1101	15	Pelaksanaan RBL /\r\nPraktikum mandiri	Tidak ada data	Tidak ada data	K
48	FI1101	16	Tidak ada data	Tidak ada data	Tidak ada data	U
49	FI1201	1	Elektrostatika: Hukum\r\nCoulomb & Medan listrik	Muatan listrik\r\nGaya dan medan listrik\r\noleh muatan diskrit &\r\nmuatan terdistribusi kontinu.	Memahami prinsip Gaya\r\ninteraksi muatan (Hukum \r\nColoumb) dan konsep \r\nmedan listrk.\r\nMampu menyelesaikan\r\npersoalan  medan listrik\r\nyang ditimbulkan oleh\r\nmuatan diskrit dan muatan\r\nkontinu	K
50	FI1201	2	Elektrostatika: Hukum Gauss	Fluks listrik, Hukum Gauss \r\nKonduktor dan  isolator,\r\nmuatan induksi.	Memahami hukum Gauss dan\r\nmampu menggunakannya\r\nuntuk menyelesaikan\r\npersoalan medan listrik\r\ndalam konduktor/isolator.	K
51	FI1201	3	Elektrostatika: Potensial\r\nlistrik. 	Energi potensial listrik dan\r\nPotensial listrik oleh\r\nmuatan diskrit dan kontinu.\r\n	Memahami konsep energi\r\npotensial dan potensial\r\nlistrik yang disebabkan\r\noleh muatan diskrit dan\r\nmuatan yang terdistribusi\r\nkontinu	K,R
52	FI1201	4	Elektrostatika:\r\nKapasitor dan dielektrik	Kapasitor (pelat sejajar,\r\nbola sepusat, silinder\r\nsesumbu), susunan\r\nkapasitor, dielektrik.	Mampu memahami beberapa\r\nstruktur kapasitor dan\r\nmenyelesaikan persoalan\r\nyang berkaitan dengannya 	K,R
53	FI1201	5	Magnetostatika:\r\nMedan Magnet	Gaya Lorentz, gaya magnet\r\npada konduktor berarus	Memahami konsep medan\r\nmagnet dan pengaruhnya\r\nterhadap muatan yang bergerak	K,R
54	FI1201	6	Magnetostatika:\r\nMedan Magnet oleh Arus\r\nlistrik	Hukum Biot-Savart\r\nHukum Ampere\r\nGaya antar kawat berarus	Memahami hukum\r\nBiot-Savart dan Hukum\r\nAmpere serta mampu\r\nmenggunakannya untuk\r\nmenyelesaikan persoalan\r\nmedan magnet	K,R
55	FI1201	7	Induksi elektromagnetik	Hukum Faraday-Lenz\r\nInduktansi diri dan mutual.	Memahami hukum Faraday\r\ndan hukum Lenz serta\r\nmampu menggunakannya\r\nuntuk menyelesaikan\r\npersoalan yang berkaitan\r\ndengan  GGL induksi	K,R
56	FI1201	8	Arus bolak balik (AC)	Arus & tegangan rms,\r\nImpedansi, analisis\r\nrangkaian RLC seri,\r\nresonansi. 	Memahami konsep arus dan\r\ntegangan rms dan\r\nkaitannya dengan\r\nimpendansi rangkaian\r\nMampu menganalisa\r\nrangkaian RLC seri dan\r\nmampu menyelesaikan\r\npersoalan yang berkaitan\r\ndengannya.	K,R
57	FI1201	9	Gelombang elektromagnetik	Persamaan Maxwell dan\r\npers. Diff. Gelombang EM\r\nPers. Gel. EM\r\nEnergi gelombang EM,\r\nvektor Poynting, Polarisasi	Memahami persamaan\r\nMaxwell dan kaitannya\r\ndengan persamaangelombang\r\nelektromagnetik \r\nMampun menguasai\r\npersoalan yang berkaitan\r\ndengan sifat dan\r\nparameter gelombang\r\nelektromagnetik	K,R
58	FI1201	10	Interferensi	Interferensi 2 celah dan\r\nN celah, Interferensi\r\nlapisan tipis	Memahami fenomena\r\ninterferensi dan mampu\r\nmenyelesaikan persoalan\r\nyang berkaitan dengannya	K,R
59	FI1201	11	Difraksi	Difraksi,\r\nInterferensi-Difraksi	Memahami fenomena\r\ndifraksi dan pengaruhnya\r\nterhadap interferensi	K,R
60	FI1201	12	Fisika Modern: \r\nTeori Relativitas Khusus\r\n	Relativitas waktu dan\r\npanjang, transformasi\r\nLorentz, relativitas\r\nkecepatan, momentum dan\r\nenergi.	Memahami konsep\r\nkerelativitasan dan\r\nakibat-akibatnya serta\r\nmampu menyelesaikan\r\npersoalan yang berkaitan\r\ndengannya.	K,R
61	FI1201	13	Fisika Modern: \r\nFoton dan gelombang materi	Foton, gelombang-cahaya,\r\nefek fotolistrik,\r\nmomentum foton, elektron\r\ndan gelombang-materi.	Memahami konsep\r\nkuantisasi, dualisme\r\npartikel-gelombang dan\r\nbeberapa konsekuensinya	K,R
113	KU1011	2	Ejaan I	Penulisan huruf, kata\r\ndan unsur serapan	Mahasiswa dapat menulis\r\nsesuai dengan kaidah ejaan	K
62	FI1201	14	Fisika Atom & Fisika material	Topik-topik khusus yang\r\nberkaitan dengan kekinian\r\nseperti: laser,\r\nsemi-superkonduktor,\r\nfisika nuklir, nanoscience.	Menguasai  beberapa \r\ntopik yang berkaitan\r\ndengan  perkembangan sain\r\n& teknologi terkini.	K,R
63	FI1201	15	RBL	Merancang peralatan\r\nsederhana yang\r\nmenggunakan konsep konsep\r\nFisika Dasar I	Melatih daya kreativitas\r\nserta mampu menerapkan\r\nkonsep Fisika Dasar I\r\nmelalui pembuatan\r\nperalatan sederhana.	K
64	KI1102	0	Belum ada	Belum ada	Belum ada	Belum ada
65	KI1202	0	Belum ada	Belum ada	Belum ada	Belum ada
66	KU1101	1	Peran rekayasa dan desain dalam masyarakat dan profesi insinyur	-\tPengantar\r\n-\tPengertian rekayasa dan desain\r\n-\tPeran insinyur dan lingkupnya \r\n-\tPenciri insinyur yang baik \r\n-\tProfesi insinyur di Indonesia dan di dunia\r\n	Mahasiswa dapat menjelaskan pentingnya peran rekayasa dan desain dalam masyarakat dan memahami ciri-ciri insinyur yang baik 	Ref 1, bab 1\r\nRef 2, bab1 ,5\r\n
67	KU1101	2	Aspek-aspek dalam rekayasa dan keterkaitannya	-\tKerangka profesi insinyur\r\n-\tEtika profesi insinyur\r\n-\tAspek ekonomi dan sosial\r\n-\tAspek multidisiplin\r\n	Mahasiswa dapat menjelaskan aspek etika, tanggung jawab dan konteks sosial, ekonomi, dan multidisiplin dalam kerekayasaan	Ref 1, bab 1,16\r\nRef 2, bab 2,5 \r\nwww.pii.or.id\r\n
68	KU1101	3	Elemen kunci dalam analisis rekayasa	-\tPengantar analisis engineering\r\n-\tUnit (SI dll) dan konversi\r\n-\tGaya, berat dan massa\r\n-\tAngka penting\r\n-\tKomunikasi teknik \r\n-\tStandar teknik, patent, trade mark, copyright, trade secret\r\n	Mahasiswa dapat menjelaskan pentingnya elemen kunci dalam analisis rekayasa	Ref 1, bab 2\r\nRef 2, bab 3.10 sd 3.15\r\nRef 2, bab 4.4 sd 4.7\r\nRef 2, bab 6\r\n
69	KU1101	4	Langkah penyelesaian masalah	-\tMetode need-know-how-solve	Mahasiswa dapat menerapkan proses penyelesaian masalah	Ref 1, bab 3\r\nRef3, bab 3\r\n
70	KU1101	5	Energi : bentuk, konversi dan konservasi	-\tPenggunaan energi\r\n-\tEnergi adalah kemampuan untuk melakukan kerja\r\n-\tBentuk-bentuk energi\r\n-\tKonversi energi\r\n-\tKonservasi energi\r\n	Mahasiswa dapat menjelaskan pentingnya konsep energi dalam rekayasa	Ref 1, bab 4\r\nRef 2, bab 13\r\n
71	KU1101	6	UTS	Tidak ada data	Tidak ada data	Tidak ada data
72	KU1101	7	Rekayasa mekanikal	-\tSiklus Motor bakar, turbin\r\n-\tPemilihan dan rekayasa material\r\n-\tProduksi\r\n-\tKonstruksi\r\n	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik mesin\r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan mekanikal \r\n	Ref 1, bab 6,11,12,13,15
73	KU1101	8	Rekayasa elektrikal dan elektronik	-\tPembangkit energi listrik\r\n-\tMicro processor \r\n-\tPrinsip kerja komputer\r\n-\tTeknologi komunikasi\r\n-\tSistem kendali\r\n	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik elektro dan elektronika \r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan elektro dan elektronika\r\n	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik elektro dan elektronika \r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan elektro dan elektronika\r\n
74	KU1101	9	Rekayasa kimia dan proses	-\tKonversi energi kimia\r\n-\tProses kimia\r\n-\tBio engineering dan bio medical\r\n	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik kimia dan bio-proses\r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan kimia dan bio-proses\r\n	Tidak ada data
75	KU1101	10	Rekayasa sipil dan lingkungan binaan	Rekayasa berbasis ilmu sipil dan lingkungan	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik sipil dan lingkungan binaan\r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan sipil dan lingkungan binaan\r\n	Ref 1, bab 12
76	KU1101	11	Rekayasa kebumian	-\tGeologi dan pertambangan	-\tMahasiswa dapat menjelaskan lingkup aplikasi kerekayasaan dalam bidang teknik kebumian dan pertambangan \r\n-\tMahasiswa dapat menjelaskan dasar-dasar / prinsip utama kerekayasaan kebumian dan pertambangan\r\n	Tidak ada data
77	KU1101	12	Pengenalan rekayasa  sesuai grup displin	Tidak ada data	Tidak ada data	Tidak ada data
78	KU1101	13	Pengenalan rekayasa  sesuai grup displin	Tidak ada data	Tidak ada data	Tidak ada data
79	KU1101	14	Pengenalan rekayasa  sesuai grup displin	Tidak ada data	Tidak ada data	Tidak ada data
80	KU1101	15	Pengenalan rekayasa  sesuai grup displin	Tidak ada data	Tidak ada data	Tidak ada data
81	KU1101	16	UAS	Tidak ada data	Tidak ada data	Tidak ada data
82	KU1201	1	Pengantar desain rekayasa	-\tSifat desain rekayasa\r\n-\tSifat perancang\r\n-\tMengelola proyek desain\r\n-\tAturan dalam desain\r\n-\tPerlunya pendekatan sistematis\r\n-\tLangkah-langkah dalam proses desain rekayasa\r\n	-\tSifat desain rekayasa\r\n-\tSifat perancang\r\n-\tMengelola proyek desain\r\n-\tAturan dalam desain\r\n-\tPerlunya pendekatan sistematis\r\n-\tLangkah-langkah dalam proses desain rekayasa\r\n	Ref 1, bab 17
83	KU1201	2	Pendefinisian masalah	Tidak ada data	Mahasiswa dapat memformulasikan permasalahan desain	Ref 1, bab 18
84	KU1201	3	Pembuatan alternatif konsep	Tidak ada data	Mahasiswa dapat membuat beberapa alternatif konsep desain	Ref 1, bab 19
85	KU1201	4	Evaluasi alternatif dan pemilihan konsep	Tidak ada data	Mahasiswa dapat mengevaluasi dan memilih konsep desain	Ref 1, bab 20
86	KU1201	5	Desain detail	Tidak ada data	Mahasiswa dapat mendetailkan desain sederhana hingga dapat dibuat	Ref 1, bab 21
87	KU1201	6	Penyajian desain	Tidak ada data	Mahasiswa dapat mengkomunikasikan desain yang dihasilkan secara sistematis	Ref 1, bab 22
88	KU1201	7	Pembuatan dan pengujian	Tidak ada data	Mahasiswa mengetahui strategi dan cara pembuatan dan pengujian produk hasil desain	Ref 1, bab 23
89	KU1201	8	Evaluasi prestasi	Tidak ada data	Mahasiswa dapat mengevalusi prestasi produk hasil desain terhadap persyaratan yang ditetapkan	Ref 1, bab 24
90	KU1201	9	Pelaporan	Tidak ada data	Mahasiswa dapat membuat laporan proyek desain	Ref 1, bab 25
91	KU1201	10	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
92	KU1201	11	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
93	KU1201	12	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
94	KU1201	13	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
95	KU1201	14	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
96	KU1201	15	Proyek kelompok	Tidak ada data	Tidak ada data	Tidak ada data
111	KU1072	15	Etika pemanfaatan teknologi informasi	-\tEtika penggunaan komputer dan pemanfaatan teknologi informasi\r\n-\tIsu-isu seputar etika penggunaan jaringan komputer	-\tMenjelaskan etika terkait pemanfaatan softcopy\r\n-\tMenjelaskan etika bekerja di dunia internet\r\n	Tidak ada data
112	KU1011	1	Ragam Bahasa	Ragam bahasa tulis ilmiah\r\ndan cirinya	Mahasiswa dapat\r\nmenyebutkan berbagai\r\nragam bahasa beserta cirinya	K
97	KU1072	1	Pengantar Berpikir Komputasional dan Pemrograman Prosedural 	-\tPemanfaatan komputasi dalam berbagai bidang\r\n-\tHardware, software, dan program\r\n-\tDari source code menjadi program (kompilasi/interpretasi)\r\n-\tPengenalan cara berpikir komputasional \r\n-\tPengenalan paradigma pemrograman dan pemrograman prosedural\r\n-\tStruktur dasar program prosedural: data + algoritma\r\n-\tPengenalan flow chart dan notasi algoritmik sebagai alternatif untuk menuliskan alur berpikir prosedural\r\n-\tPengenalan bahasa pemrograman yang dipilih\r\n-\tProgram pertama dan hasil eksekusinya\r\n	-\tMenjelaskan apa itu berpikir komputasional dan teknik-teknik di dalamnya\r\n-\tMenjelaskan bagaimana komputasi dimanfaatkan dalam keilmuan fakultas/sekolah\r\n-\tMemahami representasi dan pemrosesan data dan program dalam mesin komputer\r\n-\tMenjelaskan bagaimana proses dari source code menjadi program dengan menggunakan kompilator/interpreter\r\n-\tMenjelaskan apa yang dimaksud paradigma pemrograman prosedural\r\n	Tidak ada data
98	KU1072	2	Struktur dasar program prosedural	-\tPengenalan construct program yang akan diajarkan dalam kuliah\r\n-\tInput – Proses – Output dalam program\r\n-\tData: Variabel, type (dasar dan bentukan), konstanta, \r\n-\tAlgoritma: ekspresi (aritmatika, relasional, dan logika); input/output\r\n-\tDekomposisi algoritmik: sekuens\r\n-\tContoh kasus\r\n	-\tMengetahui construct program prosedural yang akan diajarkan di kuliah\r\n-\tMenjelaskan proses dasar dalam komputer dan program: Input – Proses – Output\r\n-\tMemaham makna, cara deklarasi, dan penggunaan beberapa aspek data dalam program yaitu variabel, type (dasar dan bentukan), serta konstanta\r\n-\tMemahami beberapa aspek dalam dekomposisi algoritmik dalam bentuk sekuens dan dapat memanfaatkan ekspresi serta perintah input/output\r\n	Tidak ada data
99	KU1072	3	Pencabangan (analisis kasus)	-\tKonsep dasar percabangan (analisis kasus): 1 kasus (if-then), 2 kasus komplementer (if-then-else), banyak kasus (if-then-else bersarang/switch/case-of).\r\n-\tContoh kasus\r\n	-\tMemahami makna dan penggunaan analisis kasus untuk 1 kasus, 2 kasus komplementer, dan banyak kasus.\r\n-\tMemahami persoalan yang dapat dikonversi menjadi program sederhana yang mengandung analisis kasus\r\n	Tidak ada data
100	KU1072	4	Pengulangan	-\tKonsep dasar pengulangan\r\n-\tPengulangan dengan for, \r\n-\tContoh kasus\r\n	-\tMemahami makna dan penggunaan pengulangan for, repeat-until, dan while-do.\r\n-\tMemahami persoalan yang dapat dikonversi menjadi program sederhana yang mengandung pengulangan for, repeat-until, dan while-do.\r\n	Tidak ada data
101	KU1072	5	Subprogram (fungsi dan prosedur)	-\tKonsep subprogram(fungsi dan prosedur)\r\n-\tMemanfaatkan subprogram sebagai sarana untuk dekomposisi program\r\n-\tContoh kasus\r\n	-\tMemahami makna dan penggunaan subprogram sebagai sarana untuk mendekomposisi program\r\n-\tMemahami persoalan yang dapat dikonversi menjadi program sederhana yang mengandung subprogram.\r\n	Tidak ada data
102	KU1072	6	Latihan-1	Review konsep dasar pemrograman prosedural dengan latihan soal-soal, s.d. materi subprogram	Menyelesaikan soal-soal latihan dan mengaplikasikan solusi persoalan tadi ke dalam suatu program sederhana yang mengandung analisis kasus, pengulangan, dan subprogram.	Tidak ada data
103	KU1072	7	Array	-\tKonsep array\r\n-\tMengisi array\r\n-\tMembaca array dan menuliskan ke layar\r\n-\tMemroses data array: traversal array (contoh: menjumlah isi seluruh array, mencari nilai rata-rata, mencari nilai maksimum/minimum), dan searching (mencari suatu nilai dalam array – menghasilkan indeks)\r\n-\tContoh kasus\r\n	-\tMemahami makna dan penggunaan array sebagai struktur data koleksi objek\r\n-\tMemahami persoalan yangdapat dikonversi menjadi program sederhana \r\n	Tidak ada data
104	KU1072	8	Latihan-2	Review konsep dasar pemrograman prosedural melalui latihan soal, fokus pada materi array 	Menyelesaikan suatu persoalan sederhana dan mengaplikasikan solusi persoalan tadi ke dalam suatu program sederhana.	Tidak ada data
105	KU1072	9	Matriks	-\tMatriks sebagai array 2 dimensi\r\n-\tMengisi matriks\r\n-\tMembaca matriks dan menuliskan ke layar\r\n-\tMemroses data matriks: traversal matriks (contoh: menjumlahkan isi matriks)\r\n-\tMemroses 2 buah matriks (contoh: menambahkan 2 matriks, mengalikan 2 matriks)\r\n	-\tMemahami makna dan penggunaan matriks sebagai array 2 dimensi.\r\n-\tMemahami persoalan yang dapat dikonversi menjadi program sederhana yang mengandung matriks\r\n	Tidak ada data
106	KU1072	10	File sekuensial	-\tKonsep file sekuensial\r\n-\tMembaca file sekuensial dan memroses hasilnya\r\n-\tMenulis file sekuensial\r\n-\tContoh kasus\r\n	-\tMemahami makna dan penggunaan file sekuensial\r\n-\tMemahami persoalan yang dapat dikonversi menjadi program sederhana yang mengandung file sekuensial\r\n	Tidak ada data
107	KU1072	11	Latihan-3	Review konsep dasar pemrograman prosedural melalui contoh kasus	Menyelesaikan suatu persoalan sederhana dan mengaplikasikan solusi persoalan tadi ke dalam suatu program sederhana.	Tidak ada data
108	KU1072	12	Sistem Komputer dan Jaringan Komputer	-\tPengantar dunia digital\r\n-\tSistem komputer, CPU dan memori, piranti input, output, dan penyimpanan\r\n-\tMenjelaskan jenis-jenis perangkat lunak dan pemanfaatannya\r\n-\tTeknologi jaringan komputer, internet, dan web\r\n-\tIsu keamanan dan resiko penggunaan teknologi informasi\r\n	-\tMenjabarkan peran-peran penting komputer dalam kehidupan modern\r\n-\tMendeskripsikan dasar-dasar struktur dan organisasi komputer\r\n-\tMenjelaskan fungsi komponen-komponen internal perangkat keras komputer dan bagaimana mereka saling berinteraksi\r\n-\tMenjelaskan contoh-contoh piranti input/output dan berbagai jenis piranti penyimpan dan bagaimana memanfaatkannya\r\n-\tMenjelaskan kategori-kategori dasar perangkat lunak dan hubungan kerjanya\r\n-\tMenjelaskan peranan sistem operasi dalam sistem komputer modern\r\n-\tMenjabarkan teknologi yang memungkinkan terbentuknya telekomunikasi\r\n-\tMenjabarkan sifat dan fungsi LAN dan WAN\r\n-\tMenjabarkan tujuan, karakteristik dan penggunaan intranet dan ekstranet\r\n-\tMendiskusikan penggunaan dan implikasi e-mail, instant messaging, blogging, teleconferencing dan bentuk komunikasi online lainnya\r\n-\tMenjabarkan  isu utama keamanan yang terkait pengguna komputer, administrator sistem komputer, dan pihak yang mempunyai kewenangan hukum\r\n	Tidak ada data
109	KU1072	13	Pemanfaatan IT di berbagai bidang	-\tAplikasi produktivitas: pengolah kata, spreadsheet, presentasi.\r\n-\tWeb applications: jejaring sosial, blog, e-commerce, video sharing, collaborative tools, dll.\r\n-\tPemanfaatan IT di berbagai bidang: aplikasi di fakultas/sekolah\r\n	-\tMenjabarkan bagaimana aplikasi produktivitas mengubah cara dan pola bekerja.\r\n-\tMenjabarkan fungsi dasar dan lanjut dari aplikasi produktivitas.\r\n-\tMenjabarkan berbagai jenis web applications yang banyak dijumpai (termasuk e-commerce dan e-business).\r\n-\tMenjelaskan berbagai teknologi (baik hardware maupun software) yang terkait dengan bidang keilmuan fakultas/sekolah\r\n	Tidak ada data
110	KU1072	14	Isu sosial politik, hukum, dalam pemanfaatan teknologi informasi	Isu sosial politik, hukum, dalam pemanfaatan teknologi informasi	-\tMenjabarkan bagaimana pertumbuhan eksplosif internet mengubah cara orang menggunakan komputer dan teknologi informasi\r\n-\tMendiskusikan dampak sosial dan etika teknologi informasi dalam masyarakat\r\n-\tMenjelaskan mengapa meng-kopi perangkat lunak tanpa ijin melawan hokum\r\n-\tMenjabarkan beberapa jenis kejahatan komputer dan mendiskusikan langkah dan teknik pencegahan kejahatan yang mungkin dilakukan\r\n	Tidak ada data
114	KU1011	3	Ejaan II 	Pemakaian tanda baca	Mahasiswa dapat\r\nmenggunakan tanda baca	K
115	KU1011	4	Morfologi I	Pembentukan kata	Mahasiswa mampu memilih\r\ndan  membentuk  kata yang\r\nbenar melalui pengimbuhan	K
116	KU1011	5	Morfologi II	Kata bentukan dalam kalimat	Mahasiswa mampu\r\nmenggunakan kata bentukan\r\nyang benar dalam kalimat	K
117	KU1011	6	Tata Kalimat I	Pola Kalimat Baku 	Mahasiswa mampu menyusun\r\nkalimat dengan dengan\r\nberbagai struktur yang baku	K
118	KU1011	7	Tata Kalimat II	Kalimat Efektif dan\r\nVariasi Kalimat 	Mahasiswa mampu menyusun\r\ngagasan dalam kalimat\r\nyang ringkas, jelas,\r\nlogis, lugas, baku, dan\r\nsistematis serta mampu\r\nmenyusun kalimat yang\r\nbervariasi sesuai dengan\r\ntujuan gagasan yang\r\ndisampaikan	K
119	KU1011	8	UJIAN TENGAH SEMESTER	Tidak ada data	UJIAN TENGAH SEMESTER	U
120	KU1011	9	Tata Istilah, Definisis,\r\ndan Silogisme	Pembentukan dan pemakaian\r\nistilah, penyusunan\r\ndefinisi, serta penarikan\r\nsimpulan	Mahasiswa mampu\r\nmenggunakan istilah,\r\nmendefnisikan, serta\r\nmenarik simpulan dengan\r\nbenar dan tepat	K
121	KU1011	10	Paragraf I	Syarat dan jenis paragraf	Mahasiswa mampu menyusun\r\ntulisan dalam paragraf\r\nyang memenuhi syarat\r\ndengan berbagai jenis\r\nsesuai dengn keperluan	K
122	KU1011	11	Paragraf II	Pengembangan paragraf	Mahasiswa mampu\r\nmengembangkan paragraph\r\nsesuai dengan konsepnya	K
123	KU1011	12	Karya Tulis ilmiah 	Kriteria ilmiah,\r\npemilihan topik,\r\npenentuan tema, dan\r\npenyusunan ragangan	Mahasiswa mampu\r\nmenentukan topik, tema,\r\ndan judul karangan serta\r\nmampu menyususun kerangka\r\nisi karangan	K
124	KU1011	13	Komponen Karangan	Komponen bagian awal,\r\nisi/utama, dan pelengkap\r\nakhir (penyudah)	Mahasiswa mampu\r\nmenyususun komponen\r\nkarangan yang diperlukan\r\ndan dimana ditempatkannya	K
125	KU1011	14	Bagian Utama Karangan	Bab pendahuluan, Bab\r\npermasalahan, Bab\r\nAnalisis, Bab Simpulan\r\ndan Saran	Mahasiswa mampu\r\nmenuangkan gagasandalam\r\nbentuk baba demi bab,\r\npasal demi pasal dsb.\r\nSesuai dengan sistemtika\r\nkarangan	K
126	KU1011	15	Konvensi Naskah	Pengetikan, pengutipan,\r\ndan pustaka	Mahasiswa mampu menyusun\r\nkarangan dalam bentuk\r\ntulisan yang sesuai\r\ndengan konvensi naskah	K
127	KU1011	16	UJIAN AKHIR SEMESTER	UJIAN AKHIR SEMESTER	UJIAN AKHIR SEMESTER	U
128	KU102X	0	Belum ada	Belum ada	Belum ada	Belum ada
129	EL1200	1	Importance of Electric Circuits in Engineering World.\r\nBasic Concept\r\n	History & introduction\r\nSystems of units.\r\nCharge and current.\r\nVoltage.\r\nPower and energy.\r\nCircuit Elements.\r\nApplications\r\n	Know the importance of electric circuits in Engineering World.\r\nUnderstand relationship between charge & current, power & energy.\r\nKnow circuit elements and the metric units of electrical quantities\r\n	Alexander & Sadiku : Chapter 1
130	EL1200	2	Basic Laws	Ohm’s law.\r\nKirchhoff’s laws.\r\nSeries resistors and voltage division. \r\nParallel resistors and current division.\r\nWye-Delta transformations.\r\nApplications.\r\n	Analyze circuit SYSTEMS using direct application of Kirchoff\\'s Current and Voltage Laws along with Ohm\\'s Law.\r\n\r\n	Alexander & Sadik: Chapter 2
131	EL1200	3	Methods of Analysis	Nodal analysis.\r\nNodal analysis with \r\nvoltage sources.\r\nNodal Analysis by inspection.\r\n\r\n	Apply node-voltage analysis techniques to analyze circuit behavior.\r\nApply cramer’s rule and Gaussian elimination to solve simultaneous equations.\r\n\r\n	Alexander & Sadiku: Chapter 3: 3.1 – 3.3, 3,6
132	EL1200	4	Methods of Analysis	Mesh analysis.\r\nMesh analysis with current sources.\r\nNodal analyses by inspection.\r\nApplications.\r\n	Apply mesh-current analysis techniques to analyze circuit behavior.\r\n\r\n	Alexander & Sadiku: Chapter 3: 3.4 – 3.9
133	EL1200	5	Circuits Theorems	Linearity property, superposition, source transformation.\r\n\r\n	Apply circuit theorems (superposition, source transformation) to analyze circuit behaviour.	Alexander & Sadiku: Chapter 4: 4.1 – 4.4
134	EL1200	6	Circuits Theorems	Thevenin & Norton theorems, maximum power transfer. \r\nApplications.\r\n	Apply circuit theorems (Thevenin & Norton theorems, maximum power transfer) to analyze circuit behaviour.	Alexander & Sadiku: Chapter 4: 4.5 – 4.10
135	EL1200	7	Operational Amplifiers	Ideal op-amp, inverting & non-inverting amplifiers.	Analyse an ideal op. amp in various applications in dc circuits.	Alexander & Sadiku: Chapter 5: 5.1 – 5.5
136	EL1200	8	Operational Amplifiers	Summing & difference amplifiers, cascaded op amps circuits. Applications.	Analyse an ideal op. amp in various applications in dc circuits.	Alexander & Sadiku: Chapter 5: 5.6 – 5.10
137	EL1200	9	Capacitors and Inductors	Capacitors.\r\nSeries and parallel capacitors.\r\nInductors.\r\nSeries and parallel inductors.\r\nApplications (differentiator, integrator, and analog computer).\r\n	Explain the characteristics of capacitor and inductor as a circuit element.	Alexander & Sadiku: Chapter 6
138	EL1200	10	First-Order Circuits	Source-free RC circuits.\r\nSource-free RL circuits.\r\nSingularity functions. \r\n\r\n	Compute initial conditions for current and voltage in first order R-L and R-C capacitor and inductor circuits.\r\nConstruct any waveform of signal using step and ramp functions.\r\n	Alexander & Sadiku: Chapter 7: 7.1 – 7.4
139	EL1200	11	First-Order Circuits	Step response of an RC circuit.\r\nStep response of an RL circuit.\r\nFirst-order op amp circuits.\r\nTransient analysis using SPICE.\r\nApplications.\r\n	Compute time response of current and voltage in first order R-L and R-C circuits.	Alexander & Sadiku: Chapter 7: 7.5 – 7.9
140	EL1200	12	Second-Order Circuits	Finding initial and final values.\r\nSource-free series RLC circuits.\r\nSource-free parallel RLC circuits.\r\nStep response of a series RLC circuit.\r\nStep response of a parallel  RLC circuit.\r\n	Compute initial conditions for current and voltage in second order RLC circuits.\r\nCompute time response of current and voltage in second order RLC circuits.\r\n	Alexander & Sadiku: Chapter 8: 8.1 – 8.4
141	EL1200	13	Second-Order Circuits 	General second-order circuits.\r\nSecond-order op amp circuits.\r\nDuality.\r\nSPICE analysis of RLC circuits\r\nApplications.\r\n	Compute time response of current and voltage in general second order RLC circuits.	Alexander & Sadiku: Chapter 8: 8.5 – 8.11
142	EL1200	14	Sinusoids and Phasors 	Sinusoids.\r\nPhasors.\r\nPhasor relationships for circuits elements.\r\nImpedance and admittance.\r\n	Understand the relationship between phasor concept and its diagram to sinusoidal signals and RLC elements in circuits.	Alexander & Sadiku: Chapter 9: 9.1 – 9.4
143	EL1200	15	Sinusoids and Phasors 	Kirchhoff’s laws in frequency domain.\r\nImpedance combinations.\r\nApplications.\r\n	Understand the relationship between phasor concept and its diagram to sinusoidal signals and RLC elements in circuits.\r\nConvert problems involving differential equations into circuit analysis problems using phasors and complex impedances\r\n\r\n	Alexander & Sadiku: Chapter 9: 9.5 – 9.8
255	II3160	3	Format dan pengkodean data non-teks	1. Format dan pengkodean data suara, gambar, dan video \r\n2. Metadata	Tidak ada data	Tidak ada data
144	KU1001	1	Pengantar MKOR dan Materi Kesehatan, kebugaran, dan pembinaan karakter	Makna kesehatan dan kebugaran jasmani, olahraga dan olahraga kesehatan, dan pengembangan nilai-nilai positif olahraga	Mahasiswa mampu memahami pentingnya kesehatan, kebugaran , dan memiliki karakter positif	Tidak ada data
145	KU1001	2	Tes kebugaran jasmani awal semester	Tes lari sejauh 2400 meter	Mahasiswa mengetahui kondisi kebugaran awal semester	Tidak ada data
146	KU1001	3	Latihan kondisi fisik & pengembangan wawasan tentang prinsip-prinsip pelatihan	•\tKekuatan, Daya tahan, kelentukan, kelincahan, metode dan sistem latihan tahanan, latihan sirkuit\r\n•\tAspek latihan, definisi latihan, prinsip-prinsip pelatihan\r\n•\tGames Cabor\r\n•\tReview Teori Kesehatan, kebugaran, dan olahraga	Mahasiswa mampu memahami pentingnya latihan kondisi fisik	Tidak ada data
147	KU1001	4	Latihan  kekuatan dan daya tahan  otot lokal & pengembangan wawasan tentang latihan kondisi fisik	•\tPush up, sit up, back up,  half squat\r\n•\tLatihan general conditioning\r\n•\tGames Cabor\r\n•\tReview Teori Aspek Pendidikan Kesehatan dalam Olahraga	Mahasiswa mampu melakukan bentuk-bentuk latihan kekuatan otot lokal sehingga memiliki kekuatan otot lengan, otot perut, otot punggung, dan otot tungkai	Tidak ada data
148	KU1001	5	Latihan Kelentukan dan Kelincahan	•\tPeregangan statis, peregangan dinamis, peregangan pasif, Shuttle run, zig-zag, squat thrush\r\n•\tGames Cabor\r\n•\tReview Teori Prinsip-prinsip Pelatihan	•\tMahasiswa mampu melakukan bentuk-bentuk latihan kelentukan dengan ruang gerak seluas-luasnya dalam persendian\r\n•\tMahasiswa mampu mengubah arah dengan cepat dan tepat pada waktu bergerak tanpa kehilangan keseimbangan.\r\n•\tMembentuk softskills	Tidak ada data
149	KU1001	6	Latihan daya tahan kecepatan	•\tLatihan sprint \r\n•\tLari jarak menengah\r\n•\tGames Cabor\r\n•\tReview Teori Latihan Kondisi Fisik	•\tMahasiswa mampu melakukan bentuk-bentuk latihan dan mampu mempertahankan kecepatannya dalam waktu yang relatif lama.\r\n•\tMembentuk softskills	Tidak ada data
150	KU1001	7	Latihan daya tahan kecepatan\r\n(lanjutan)	•\tLatihan sprint \r\n•\tLari jarak menengah\r\n•\tGames Cabor\r\n•\tReview Teori Gizi Olahraga	•\tMahasiswa mampu melakukan bentuk-bentuk latihan dan mampu mempertahankan kecepata.\r\n•\tMembentuk softskills	Tidak ada data
151	KU1001	8	UTS Praktek	Tes lari 2,4 Km dan Batterey Test	Mahasiswa mengetahui kondisi kebugaran tengah semester	Tidak ada data
152	KU1001	9	Latihan daya tahan, kekuatan, dan kecepatan	•\tLatihan sprint \r\n•\tCircuit Training \r\n•\tGames Cabor	•\tMahasiswa mampu melakukan bentuk-bentuk latihan dan mampu mempertahankan kecepatannya dalam waktu yang relatif lama.\r\n•\tMembentuk softskills	Tidak ada data
153	KU1001	10	Latihan daya tahan, kecepatan, dan kekuatan\r\n(lanjutan)	•\tLari jarak menengah \r\n•\tLari jarak pendek\r\n•\tPush up, sit up, back up,  half squat\r\n•\tGames Cabor	•\tMahasiswa mampu melakukan bentuk-bentuk latihan dan mampu mempertahankan kecepatannya dalam waktu yang relatif lama.\r\n•\tMembentuk softskills	Tidak ada data
154	KU1001	11	Latihan daya tahan resviratory cardio vascular	•\tInterval Running\r\n•\t Push up, sit up, back up,  half squat\r\n•\tGames Cabor	•\tMahasiswa mampu memelihara dan mempertahankan kecepatan dan daya tahan pernafasan, jantung, dan peredaran darah.\r\n•\tMembentuk softskills	Tidak ada data
155	KU1001	12	Latihan daya tahan resviratory cardio vascular (lanjutan)	•\tFartlek (speed play), lari lintas alam\r\n•\tPush up, sit up, back up,  half squat\r\n•\t interval runing\r\n•\tGames Cabor	•\tMahasiswa mampu memelihara dan meningkatkan daya tahan pernafasan, jantung, dan peredaran darah.\r\n•\tMembentuk softskills	Tidak ada data
156	KU1001	13	Latihan daya tahan resviratory cardio vascular (lanjutan)	•\tFartlek (speed play), lari lintas alam,  interval running\r\n•\tPush up, sit up, back up,  half squat\r\n•\tGames Cabor	•\tMahasiswa mampu memelihara dan meningkatkan daya tahan pernafasan, jantung, dan peredaran darah.\r\n•\tMembentuk softskills	Tidak ada data
157	KU1001	14	Latihan daya tahan resviratory cardio vascular (lanjutan)	•\tFartlek (speed play), lari lintas alam,  interval running\r\n•\tPush up, sit up, back up,  half squat\r\n•\tGames Cabor	•\tMahasiswa mampu memelihara dan meningkatkan daya tahan pernafasan, jantung, dan peredaran darah.\r\n•\tMembentuk softskills	Tidak ada data
158	KU1001	15	UAS Praktek	Tes lari 2,4 Km dan Batterey Test	Mahasiswa mengetahui kondisi kebugaran akhir semester	Tidak ada data
159	KU1001	16	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
160	IF1210	1	Konsep dan Lingkungan\r\nPemrograman	Konsep:\r\nWhat is Programming\r\nFundamentals concept\r\nPraktek:\r\nWarming up untuk submission\r\n	Mahasiswa memahami konsep\r\ndasar pemrograman\r\nMahasiswa dapat\r\nmenggunakan lingkungan\r\nuntuk setoran tugas\r\npemrograman\r\n	K
161	IF1210	2	Tidak ada data	Konsep:\r\nProgramming Tools and\r\nEnvironment, dimanakah\r\nperan bahasa pemrograman\r\nPraktek:\r\nMemprogram dengan\r\nsederhana dan menarik\r\n	Mahasiswa memahami tools,\r\nlingkungan, dan term-term\r\ndunia pemrograman\r\n\r\nMahasiswa dapat\r\n“memprogram” pada\r\nlingkungan sederhana \r\ndengan Scratch, Bob!,\r\natau lainnya (tugas\r\nkompetisi)\r\n	K
162	IF1210	3	Berpikir abstrak dan\r\nfungsional untuk problem\r\nsolving	Konsep: Berpikir abstrak\r\ndan fungsional \r\nPraktek:\r\nOperasi Aritmatika dan\r\noperasi sederhana lainnya\r\n	Mahasiswa memahami dan\r\nmampu berpikir\r\nabstrak/model untuk\r\nproblem solving\r\nMahasiswa memahami cara\r\nberpikir fungsional\r\nsebagai cara untuk\r\nproblem solving \r\n	K
163	IF1210	4	Tidak ada data	Konsep:\r\nPengantar Rekurens \r\nPraktek:\r\nManipulasi atom dan list\r\n	Mahasiswa memahami notion\r\nrekurense dan mampu\r\nmengmplementasikannya \r\nprogram sederhana 	K
164	IF1210	5	Tidak ada data	Konsep:\r\nRekurens& functional\r\nprogramming\r\nPraktek:\r\nOperasi max, min, search\r\nKuis:\r\nKonsep Pemrograman dan\r\nPemrograman Fungsional	Mahasiswa memahami notion\r\nrekurense dan mampu\r\nmengmplementasikannya \r\nuntuk menentukan nilai\r\nmaksimum, minimum, dan\r\nmencari nilai tertentu\r\ndalam list	K/Q
165	IF1210	6	Tidak ada data	Konsep:\r\nRekurens& functional\r\nprogramming\r\nPraktek:\r\nS-expression (aspek\r\nfungsi sebagai parameter\r\nfungsi)\r\n\r\n	Mahasiswa memahami notion\r\nrekurense dan mampu\r\nmengmplementasikan\r\nprogram sederhana dengan\r\nparameter fungsi	K
166	IF1210	7	Berpikir modular,\r\ndekomposisi problem dan modu.\r\nProcedural Programming (1)\r\n	Konsep:\r\nProblem decomposition,\r\nModularisasi. Gambaran\r\nfungsi, prosedur\r\nPraktek:\r\nData+aksi\r\nTipe, Variable, Scope &\r\nlifetime, Constant\r\nTulis hello,I/O, \r\nekspresi, sekuense,\r\nkondisional\r\n	Mahasiswa mampu berpikir\r\nmodular dan melakukan\r\ndekomposisi problem\r\nMahasiswa mengerti konsep\r\npemrograman prosedural\r\ndan mampu membuat program\r\nsederhana (s.d kondisional)\r\n	K
167	IF1210	8	UTS – konsep dan praktek\r\ndengan autograder	Tidak ada data	Tidak ada data	U
168	IF1210	9	Berpikir modular,\r\ndekomposisi modul, dan\r\nprocedural\r\nProcedural Programming (2)\r\n	Konsep:\r\nPengenalan pola,\r\ngeneralisasi pola untuk\r\nmendefinisikan abstraksi\r\natau model\r\nPraktek:\r\nfungsi, procedure\r\n	Mahasiswa mampu melakukan\r\ndekomposisi modul\r\nsederhana dan\r\nmengimplementasikannya\r\ndalam bentuk fungsi dan\r\nprosedur dalam program.	K
169	IF1210	10	Tidak ada data	Konsep:\r\nSource code standards,\r\nbest practices of programming\r\nPraktek:\r\nArray sederhana dan loop\r\n	Mahasiswa mempunyai\r\npraktek pemrograman yang baik\r\nMahasiswa mampu membuat\r\nprogram yang memanipulasi\r\narray secara sederhana\r\n(print, inisialisasi,\r\njumlah dan banyaknya nilai) \r\n	K
170	IF1210	11	Tidak ada data	Konsep:\r\nProgram Taxonomy dan\r\nkemampuan programming\r\nyang dibutuhkan\r\n\r\nPraktek:\r\nFile I/O\r\n	Mahasiswa memahami\r\ntaksonomi programming dan\r\ndapat memetakan\r\nkompetensi dan kemampuan\r\nprogrammer pada setiap\r\njenis/levelnya.\r\nMahasiswa mampu membuat\r\nmembaca dan menulis \r\ndari/ke file \r\n	K
171	IF1210	12	Small Project/case study	Tidak ada data	Mahasiswa mampu merangkum\r\nkemampuan pemrogramannya\r\nuntuk menyesaikan problem\r\ndalam projek skala kecil 	K
172	IF1210	13	Pemrograman Prosedural\r\nKeterbatasan dan\r\ntantangan memprogram \r\n	Konsep:\r\nLimit of Computing\r\nPraktek:\r\nIntro to C\r\n	Mahasiswa memahami\r\npersoalan-persoalan\r\npemrograman yang “tidak\r\nbiasa”\r\nMahasiswa mampu\r\nmenyelesaikan persaolan\r\npemrpograman yang tidak\r\nbiasa secara “manual”\r\n(eksekusi cara\r\npenyelesaian yang diberikan)\r\n	K
173	IF1210	14	Tidak ada data	Kuis:\r\nPrmograman Prosedural\r\nKonsep:\r\nComplexity, Program\r\nKompleks skala besar dan\r\ndata besar (Intro to\r\nCombinatorics algorithm)\r\nPraktek:\r\nPascal to C (Pascal like)\r\n	Mahasiswa memahami\r\npersoalan-persoalan\r\npemrograman yang kompleks\r\ndengan data besar\r\n\r\nMahasiswa memiliki sense\r\nbahwa berganti bahasa\r\npemrograman itu “mudah”\r\ndengan melakukan “replace\r\nteks”\r\n	K/Q
174	IF1210	15	Tidak ada data	Konsep:\r\nProgram Kompleks dan data\r\nbesar (strategy\r\nalgorithmic), algorithm\r\ndesign, data analysis and\r\nvisualization\r\nPraktek:\r\nPascal to C (Pascal like)\r\nEksplore matlab, excel,\r\nmap reduce\r\n	Mahasiswa memahami\r\npersoalan-persoalan\r\npemrograman yang kompleks\r\ndengan data besar\r\n\r\nMahasiswa memahami\r\npanorama persoalan\r\ncomputing, pemrosesan\r\ndata,  dan tantangannya.\r\n	K
175	IF1210	16	UAS	Tidak ada data	Tidak ada data	U
176	II2110	0	Belum ada	Belum ada	Belum ada	Belum ada
177	II2230	0	Belum ada	Belum ada	Belum ada	Belum ada
178	II2111	0	Belum ada	Belum ada	Belum ada	Belum ada
179	EL2244	1	Introduction	Computer Abstraction\r\nData and Information\r\nCompilation System\r\nHardware Organization\r\nArchitecture and Organization\r\nStructure and Function\r\nOperating System\r\nPersonal and Career\r\n\r\n	Understanding the abstraction of computer system\r\nUnderstanding that information is bits in context\r\nUnderstanding how compilation systems work\r\nArticulate differences between computer organization and computer architecture.\r\nIdentify some of the components of a computer.\r\nIdentify structure and function of computer\r\nUnderstanding that the operating system manages the hardware\r\nDescribe how IT engineer uses or benefits from computer architecture and system\r\n	CSAPP Ch 1,\r\nCOaA Ch 1\r\n
180	EL2244	2	Computer Evolution and Performance	History of computers\r\nPerformance Analysis\r\n	Identify some contributors to computer architecture and organization and relate their achievements to the knowledge area.\r\nUnderstand the factors that contribute to computer performance.\r\nUnderstand the limitations of performance metrics.\r\nSelect the most appropriate performance metric when evaluating a computer.\r\n	COaD Ch 1,\r\nCOaA Ch 2\r\n
181	EL2244	3	Computer Components and Interconnection	Top level view of computer\r\nVon Neumann machine\r\nInstruction cycle\r\nDatapath and Control\r\n	Explain the organization of a von Neumann machine and its machine and its major functional units.\r\nExplain how a computer fetches from memory and executes an instruction.\r\nArticulate the strengths and weakness of the von Neumann architecture.\r\n	COaD Ch 4,\r\nCOaA Ch 3\r\n
182	EL2244	4	Data Representation, Integer  Representation and Operations,\r\nFloating Point\r\n	Binary representation and operation\r\nBig endian and little endian machine\r\nUnsigned and Signed Numbers. Range, Arithmetic Operations\r\nIEEE754, Representation, Range, Precision, Rounding, and Arithmetic operations\r\n	Appreciate how numerical values are represented in digital computers.\r\nUnderstand the limitations of computer arithmetic and the effects of errors on calculations.\r\nAppreciate how numerical values are represented in digital computers.\r\nUnderstand the limitations of computer arithmetic and the effects of errors on calculations.\r\n	CSAPP Ch 2\r\n\r\nCSAPP Ch2\r\n\r\nCSAPP Ch2\r\n
183	EL2244	5	Processor Intel’s Instruction Set Architecture	Data Formats, Accesing Information, ALU Ops, Control	Explain the relationship between the representation of machine level operation at the binary level and their representation by a symbolic assembler.\r\nWrite small programs and fragments of assembly language code to demonstrate an understanding of machine level operations.\r\nImplement some fundamental high-level programming constructs at the machine-language level.\r\n	CSAPP Ch3
184	EL2244	6	Processor Intel’s Instruction Set Architecture	Control and Procedure	Explain the relationship between the representation of machine level operation at the binary level and their representation by a symbolic assembler.\r\nWrite small programs and fragments of assembly language code to demonstrate an understanding of machine level operations.\r\nImplement some fundamental high-level programming constructs at the machine-language level.\r\n	CSAPP Ch3
185	EL2244	7	Processor Intel’s Instruction Set Architecture	Array allocations, Structures, and unions	Explain the relationship between the representation of machine level operation at the binary level and their representation by a symbolic assembler.\r\nWrite small programs and fragments of assembly language code to demonstrate an understanding of machine level operations.\r\nImplement some fundamental high-level programming constructs at the machine-language level.\r\n	CSAPP Ch3
186	EL2244	8	Midterm	Tidak ada data	Tidak ada data	Tidak ada data
187	EL2244	9	Memory Hierarchy	Memory Technology, Cache, \r\nVirtual memory\r\n	Identify the main types of memory technology.\r\nExplain the effect of memory latency and bandwith on performance.\r\nExplain the use of memory hierarchy to reduce the effective memory latency.\r\nDescribe the principles of memory management.\r\nExplain the use of memory hierarchy to reduce the effective memory latency.\r\nDescribe the principles of memory management.\r\nDesign an interface to memory.\r\n	COaD Ch5, CSAPP Ch6\r\n\r\nCOaD Ch6, CSAPP Ch10\r\n
188	EL2244	10	I/O Subsystem	Peripherals and Storage	Explain how to use interrupts to implement I/O control and data transfers.\r\nWrite small interrupt service routines and I/O drivers using assembly language.\r\nIdentify various types of buses in a computer system.\r\nDescribe data access from a magnetic disk drive.\r\nAnalyze and implement interfaces.\r\nCompute the various parameters of performance for standard I/O types.\r\nExplain the basic nature human computer interaction devices.\r\nDescribe data access from magnetic and optical disk drives.\r\nUnderstand how to interface and use peripheral chips.\r\nWrite sufficient EPROM-based system software to create a basic stand-alone system\r\nSpecify and design simple computer interfaces.\r\n	COaD Ch6
189	EL2244	11	Operating System	Tidak ada data	Understanding that the operating system manages the hardware 	COaD Ch
190	EL2244	12	Introduction to Superscalar, Parallel and Distributed System	Tidak ada data	Discuss how various architectural enhancements affect system performance\r\nExplain the differences between different paradigms and their usefulness and applicability.\r\nDiscuss how to apply parallel processing approaches to design scalar and superscalar processors\r\nUnderstand how client server model works in a decentralized fashion.\r\nUnderstand how agents work and how they solve simple tasks.\r\n\r\n	COaD Ch7
191	EL2244	13	Enterprises Computer System and Architecture	Data Center, Disaster Recovery Center, Large Scale Storage System, Data Replication DC-DRC	Understand the concept of DC-DRC\r\nUnderstand the storage technology\r\nUnderstand the impotance of storage system in modern Enterprise Computer System\r\nKnow the tipical configuration of  Enterprise Computer System\r\n\r\n	Berbabagai sumber
192	EL2244	14	Tidak ada data	Data Center Design and Standard. SOA.	Know SOA\r\nKnow Data Center Standard\r\nKnow how to design Data Center\r\n	Berbabagai sumber
193	EL2244	15	Cloud Computing System	Cloud Computing Services, Architecture, Performance. Case Study: Global Cloud Computing provider.	Understand the Concept of Cloud Computing\r\nUnderstand CC Services\r\nUnderstand the performances issues of CC\r\n\r\n	Berbabagai sumber
194	EL2142	1	Introduction	Tidak ada data	1.Identify some contributors to digital logic and microprocessor system, and relate their achievements to the knowledge area.\r\n2.Explain why Boolen logic is important to this subject.\r\n3.Articulate why gates are the fundamental elements of a digital system.\r\n4.Describe how electrical engineering uses or benefits from digital logic and microprocessor system.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 1\r\n(Ronald, Neal, Moss) Chapter 1\r\n
195	EL2142	2	Boolean Algebra + Logic Circuit	Tidak ada data	1.Derive and manipulate switching functions that form the basis of digital circuits.\r\n2.Apply digital system design principles and techniques.\r\n3.Model and simulate a digital system using schematic diagrams.\r\n4.Model and simulate a digital system using a hardware description language, such as VHDL or Verilog.\r\n5.Understand timing issues in digital systems and know how to study these via digital circuit simulation.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 2\r\n
196	EL2142	3	Implementation Technology	Tidak ada data	1.Realize switching functions with networks of logic gates.\r\n2.Explain and apply fundamental characteristics of relevant electronic technologies, such as propagation delay, fan-in, fan-out, and power dissipation and noise margin.\r\n3.Utilize programmable devices such as FPGAs and PLDs to implement digital system designs.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 3\r\n
197	EL2142	4	Optimized Implementation of Logic Functions - KMAP\r\n\r\nNumber Representation & Arithmetic Circuit\r\n	Tidak ada data	1.Derive and manipulate switching functions that form the basis of digital circuits.\r\n2.Reduce switching functions to simplify circuits used to realize them.\r\n\r\n1.Work with binary number systems and arithmetic.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 4\r\n\r\n(S. Brown and Z. Vranesic)\r\nChapter 5\r\n
198	EL2142	5	Combinational Circuit Building Blocks 	Tidak ada data	1.Analyze and explain uses of small- and medium-scale logic functions as building blocks.\r\n2.Analyze and design combinational logic networks in a hierarchical, modular approach, using standard and custom logic functions.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 6\r\n
199	EL2142	6	Sequential Circuit Elements 	Tidak ada data	1.Contrast the difference between a memory element and a register.\r\n2.Indicate some uses for sequential logic.\r\n3.Design and describe the operation of basic memory elements.\r\n4.Analyze circuits containing basic memory elements.\r\n5.Apply the concepts of basic timing issues, including clocking, timing constrains, and propagation delays during the design process.\r\n6.Analyze and design functional building blocks and timing concepts of digital systems.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 7\r\n
200	EL2142	7	Synchronous State Machine	Tidak ada data	1.Analyze the behaviour of synchronous machines.\r\n2.Design synchronous sequential machines.\r\n3.Reduce the number of states to simplify circuits used to realize them.\r\n	(S. Brown and Z. Vranesic)\r\nChapter 8\r\n
201	EL2142	8	Midterm Exam (Digital System)	Tidak ada data	Tidak ada data	(S. Brown and Z. Vranesic)\r\nChapter 1-8\r\n
202	EL2142	9	Microprocessor System Architecture\r\nStack and Interupt\r\n	Tidak ada data	1.Describe microprocessor system\r\n2.Describe bus and its timing concept\r\n3.Realize von Neumann vs Harvard machine \r\n\r\n1.Realize role of stack on interrupt\r\n2.Analyze relation between interrupt and stack\r\n	(Ronald, Neal, Moss)\r\n(Ronald, Neal, Moss)\r\n
203	EL2142	10	Address Decoder	Tidak ada data	1.Create memory map\r\n2.Design address decoder using digital logic circuit\r\n3.Count and describe memory timing access\r\n	(Ronald, Neal, Moss)
204	EL2142	11	Software Architecture	Tidak ada data	1.Design and describe super loop architecture\r\n2.Design and describe foreground background (interrupt) architecture\r\n3.Design and analyze program flow chart\r\n	(Ronald, Neal, Moss)
205	EL2142	12	Peripheral	Tidak ada data	1.Describe internal peripheral of microcontroller\r\n2.Design and analyze software to use timer, ADC, PWM\r\n3.Design and analyze software and hardware for ADC\r\n4.Design PWM application\r\n	(Ronald, Neal, Moss)
206	EL2142	13	Serial Communication	Tidak ada data	1.Describe serial communication\r\n2.Design serial communication software\r\n	(Ronald, Neal, Moss)
207	EL2142	14	Interfacing\r\n\r\nInterfacing\r\n	Digital I/O\r\n\r\nAnalog I/O\r\n	1.Design hardware and software to access LED, switch, 7 segment display, LED matrix, keypad matrix\r\n\r\n1.Design hardware and software to access analog device, e.g. temperature sensor and light sensor\r\n	(Ronald, Neal, Moss)
208	EL2142	15	Microprocessor System Toolchain and Design 	Tidak ada data	1.Design, implementation software, debugging and analyze\r\n2.Simple microprocessor system design\r\n	(Ronald, Neal, Moss)
209	EL2142	16	Final Exam (Microprocessor System)	Tidak ada data	Tidak ada data	(Ronald, Neal, Moss)
210	II2220	0	Belum ada	Belum ada	Belum ada	Belum ada
211	TI3005	1	Pengantar\t	•Konsep dasar manajemen\r\n•Evolusi teori manajemen\r\n•Fungsi-fungsi manajemen (P-O-A-C)\r\n\t	•Mahasiswa memahami dengan baik prinsip-prinsip dasar manajemen\r\n•Mahasiswa memahami perkembangan teori manajemen dan implementasinya sampai saat ini\r\n•Mahasiswa memahami peran dan fungsi dari manajemen \r\n	K
212	TI3005	2	Proses Manajemen (Perencanaan)\t	•Konsep dasar Perencanaan\r\n•Metoda Perencanaan (Strategi Manajemen)\t	•Mahasiswa memahami dengan baik tahap-tahap proses perencanaan\r\n•Mahasiswa memahami dengan baik tahap-tahap manajemen strategis, dan pemilihan strategi yang sesuai dengan visi-misi organisasi\r\n	K
213	TI3005	3	Pengertian Dasar Organisasi\t	•Konsep dasar organisasi\r\n•Ruang lingkup organisasi\r\n•Evolusi Teori Organisasi\t	•Mahasiswa memahami konsep dan definisi organisasi\r\n•Mahasiswa memahami dengan baik perkembangan teori organisasi dan implementasinya\r\n	K
256	II3160	3	Konversi data	1. Tujuan konversi data\r\n2. Metode konversi data	Tidak ada data	Tidak ada data
214	TI3005	4	Dimensi Kontekstual : Analisis Lingkungan (1)\t	•Organisasi sebagai sistem terbuka\r\n•Jenis-jenis lingkungan\r\n•Strategi menghadapi lingkungan\t	•Mahasiswa mampu mengidentifikasi lingkungan organisasidan memahami cara-cara menghadapi perubahan lingkungan\r\n	K
215	TI3005	5	Dimensi Kontekstual : Analisis Lingkungan (2)\t	•Model Organisasi dikaitkan dengan lingkungan (Mekanistik & Organik)\r\n•Differentiation & Integration\t	•Mahasiswa mampu menganalisisbentuk organisasi berdasarkan kebutuhan lingkungan\r\n	K
216	TI3005	6	Dimensi Kontekstual: Teknologi Organisasi\t	•Definisi teknologi\r\n•Model Kontingensi dari ketergantungan teknologi\r\n•Implikasi teknologi pada struktur organisasi\t	•Mahasiswa memahami definisi teknologi dalam konteks organisasi dan pengaruh teknologi terhadap bentuk organisasi\r\n	K
217	TI3005	7	Dimensi Struktural : Birokrasi (1)\t	•Hubungan birokrasi dengan ukuran & desain organisasi\r\n•Sifat sifat birokrasi & peran birokrasi dalam desain organisasi\t	•Mahasiswa memahami fungsi dan penggunaan birokrasi dalam organisasi\r\n	K
218	TI3005	8	Dimensi Struktural : Birokrasi (2)	•Basis otoritas dalam organisasi\r\n•Hubungan ukuran organisasi   birokrasi\r\n	•Mahasiswa memahami fungsi dan penggunaan birokrasi dalam organisasi\r\n	K
219	TI3005	9	UTS	UTS	UTS	U
220	TI3005	10	Struktur Organisasi dan Design (1)\t	Jenis-jenis Struktur Organisasi (Functional, Product Structure & Matrix Structure)\t	•Mahasiswa memahami kelebihan dan kekurangan masing-masing jenis struktur organisasi\r\n•Mahasiswa memahami penggunaan masing-masing jenis struktur organisasi\r\n	K
221	TI3005	11	Struktur Organisasi dan Design (2)\t	Pendekatan Mintzberg dan jenis-jenis struktur organisasi	Tidak ada data	K
222	TI3005	12	Pendekatan Perancangan Organisasi (1)	•Pendekatan Top-Down (MBO perspektif)\r\n•Pendekatan Bottom-Up (Organization Climate, Culture)\t\r\n	•Mahasiswa memahami kelebihan dan kekurangan masing-masing pendekatan perancangan organisasi\r\n•Mahasiswa memahami penggunaan masing-masing struktur organisasi\r\n	K
223	TI3005	13	Pendekatan Perancangan Organisasi (2)	•Pendekatan Proses (power, politik & planning) \r\n•Pendekatan Pemilihan Perancangan (Pendekatan Strategi dan Struktur)\r\n	•Mahasiswa memahami kelebihan dan kekurangan masing-masing pendekatan perancangan organisasi\r\n•Mahasiswa memahami penggunaan masing-masing struktur organisasi\r\n	K
224	TI3005	14	Sasaran dan Efektivitas Organisasi\t	•Definisi, Kriteria Saran, Jenis-Jenis Sasaran\r\n•Pengukuran Efektivitas Organisasi \t	•Mahasiswa memahami pemilihan kriteria sasaran yang sesuai bagi organisasi\r\n•Mahasiswa memahami metode-metode pengukuran efektivitas organisasi\r\n	K
225	TI3005	15	Implementasi Organisasi \t	Penyusunan Fungsi & Tugas Pokok (Job Design) \t	Mahasiswa memahami cara dan proses perancangan jabatan 	K
226	TI3005	16	UAS	UAS	UAS	U
227	II2221	0	Belum ada	Belum ada	Belum ada	Belum ada
228	IF2140	0	Belum ada	Belum ada	Belum ada	Belum ada
229	II2240	0	Belum ada	Belum ada	Belum ada	Belum ada
230	IF2111	0	Belum ada	Belum ada	Belum ada	Belum ada
231	IF2210	1	Prinsip Fundamental OOP,\r\nmotivasi dan aplikasi OOP.\r\nRelease Tugas Besar I/C++\r\n	- Paradigma OO dan\r\nmekanisme abstraksi\r\n- Motivasi dan aplikasi OOP\r\n- Prinsip fundamental OO\r\n(breadth): object, class,\r\nenkapsulasi, inheritance,\r\noperasi-dasar objek,\r\nobjek life time, genericity\r\n	Tidak ada data	Tidak ada data
232	IF2210	2	C++: Konsep & Pemrograman\r\nDasar OO 	C++: class-object, ctor,\r\ndtor, cctor, operator\r\nassignment, object life\r\ntime, pointer &\r\nreference, enkapsulasi,\r\nconst, static	Tidak ada data	Tidak ada data
233	IF2210	3	C++: nested class,\r\noperator overloading\r\nC++: inheritance &\r\npolymorphism\r\n	C++: nested class,\r\nfriend, operator\r\noverloading\r\n(insert/update/operasi\r\naritmatika, operasi io)\r\nC++: inheritance &\r\npolymorphism, dynamic\r\nbinding, enkapsulasi\r\ndalam inheritance \r\nStudi kasus: stack\r\n	Tidak ada data	Tidak ada data
234	IF2210	4	C++: generik, exception,\r\nassertion, standard\r\ntemplate library	C++: konsep generik,\r\ntemplate function, kelas\r\ngenerik\r\nC++: exception,\r\nassertion, standard\r\ntempalte library (STL)\r\n	Tidak ada data	Tidak ada data
235	IF2210	5	Pemrograman OO Dasar\r\ndengan Java\r\nKuis 1 Konsep OO dan C++\r\nRelease Tugas Besar II/Java \r\n	Java: Pengantar Java,\r\nJVM,\r\nTransformasi/Konversi\r\nprogram C++ ke Java	Tidak ada data	Tidak ada data
236	IF2210	6	Java: Generik, Exception\r\nHandling, Asersi\r\nJava: JUnit\r\n	Tidak ada data	Tidak ada data	Tidak ada data
237	IF2210	7	Java: Multithreading\r\nPembahasan Kuis 1, Review\r\nuntuk UAS\r\n	Tidak ada data	Tidak ada data	Tidak ada data
238	IF2210	8	UTS	Tidak ada data	Tidak ada data	Tidak ada data
239	IF2210	9	Pengantar Analisis dan\r\nDesign Berorientasi Objek	Tidak ada data	Mahasiswa memahami cara\r\nberpikir dari probuntuk\r\nmenganalisis problem dan\r\nmerepresentasi/desain\r\nsolusi dengan paradigma OO	Tidak ada data
240	IF2210	10	Java API/Frameworks dan\r\nJava Platforms (1)	Java Collection Framework\r\nPrinsip Dasar Java Swing \r\n	Tidak ada data	Tidak ada data
241	IF2210	11	Java API/Frameworks dan\r\nJava Platforms (2)\r\n\r\nPembahasan UTS \r\n	Prinsip Dasar J2ME \r\n\r\nJava Reflection & Case\r\nStudy: Plugin	Tidak ada data	Tidak ada data
242	IF2210	12	Pemrograman Dasar OO\r\ndengan C#	Pengantar .NET\r\nOverview C#, termasuk\r\nexception handling,\r\nnamespace, dan IO\r\n	Tidak ada data	Tidak ada data
243	IF2210	13	C#: Generic\r\nKuis 2 Java & C# \r\n	Tidak ada data	Tidak ada data	Tidak ada data
244	IF2210	14	Fitur spesifik C#	Delegates, Events, &\r\nLamda Expressions\r\nRuntime Type ID,\r\nReflection, Attributes\r\nCollections\r\nLINQ & PLINQ\r\n	Tidak ada data	Tidak ada data
245	IF2210	15	- Pembahasan Kuis 2\r\n- Kuliah Penutup\r\n	Tidak ada data	Tidak ada data	Tidak ada data
246	IF2210	16	UAS	Tidak ada data	Tidak ada data	Tidak ada data
247	II3150	0	Belum ada	Belum ada	Belum ada	Belum ada
248	II3260	0	Belum ada	Belum ada	Belum ada	Belum ada
249	II3130	0	Belum ada	Belum ada	Belum ada	Belum ada
250	II3230	0	Belum ada	Belum ada	Belum ada	Belum ada
251	II3160	1	Sistem komputasi modern	Pengenalan sistem komputasi saat ini, termasuk cloud computing	Tidak ada data	Tidak ada data
252	II3160	1	Integrasi sistem dan metodenya	1. Tujuan integrasi sistem\r\n2. Tantangan integrasi\r\n3. Metode integrasi	Tidak ada data	Tidak ada data
253	II3160	2	Pengenalan sistem komputer dan lingkungan pemrograman	1. Pengenalan lingkungan sistem operasi \r\n2. Pengenalan perangkat lunak bantu yang dapat digunakan di atas sistem operasi untuk pemrograman	Tidak ada data	Tidak ada data
254	II3160	2	Format dan pengkodean data teks	1. ASCII dan pengkodean lain\r\n2. Unicode	Tidak ada data	Tidak ada data
257	II3160	4	Penyimpanan data	1. Media penyimpanan\r\n2. Files\r\n3. Basis data	Tidak ada data	Tidak ada data
258	II3160	4	Pengambilan data	1. Metode pengambilan data\r\n2. Presentasi data	Tidak ada data	Tidak ada data
259	II3160	5	Aplikasi dan platform	1. Sistem komputer dan aplikasi\r\n2. Platform integrasi	Tidak ada data	Tidak ada data
260	II3160	5	Arsitektur jaringan	1. Sistem terpusat\r\n2. Sistem terdistribusi	Tidak ada data	Tidak ada data
261	II3160	6	Pengembangan aplikasi	1. Jenis aplikasi\r\n2. Paradigma pengembangan aplikasi	Tidak ada data	Tidak ada data
262	II3160	6	Framework pengembangan aplikasi	1. Tujuan penggunaan framework\r\n2. Macam-macam framework	Tidak ada data	Tidak ada data
263	II3160	7	Bahasa pemrograman	1. Jenis bahasa pemrograman\r\n2. Contoh bahasa pemrograman	Tidak ada data	Tidak ada data
264	II3160	7	Pemilihan bahasa pemrograman	1. Faktor pemilihan bahasa pemrograman\r\n2. Bahasa pemrograman populer	Tidak ada data	Tidak ada data
265	II3160	8	Ujian Tengah Semester (UTS)	Ujian tertulis	Tidak ada data	Tidak ada data
266	II3160	9	Antarmuka pemrograman aplikasi	1. Tujuan antarmuka pemrograman\r\n2. Pembuatan antarmuka pemrograman	Tidak ada data	Tidak ada data
267	II3160	9	Antarmuka pengguna	1. Antarmuka pengguna dan perangkat\r\n2. Bahasa pemrograman antarmuka pengguna	Tidak ada data	Tidak ada data
268	II3160	10	Scripting dan bahasa pemrogramannya	1. Tujuan scripting\r\n2. Bahasa pemrograman untuk scripting	Tidak ada data	Tidak ada data
269	II3160	10	Automasi proses dengan script	Contoh automasi menggunakan script	Tidak ada data	Tidak ada data
270	II3160	11	Sistem kontrol versi perangkat lunak	Sistem kontrol versi perangkat lunak dan fungsinya	Tidak ada data	Tidak ada data
271	II3160	11	Pengenalan satu sistem kontrol versi: git	Pengenalan cara kerja salah satu sistem kontrol perangkat lunak, yaitu git	Tidak ada data	Tidak ada data
272	II3160	12	Network socket	Cara pertukaran data melalui Internet menggunakan network socket	Tidak ada data	Tidak ada data
273	II3160	12	Aplikasi menggunakan socket	1. Cara pemrograman network socket dalam berbagai bahasa pemrograman\r\n2. Contoh aplikasi menggunakan network socket	Tidak ada data	Tidak ada data
274	II3160	13	Protokol dan layanan messaging	Pengantar protokol dan layanan messaging menggunakan publish/subscribe	Tidak ada data	Tidak ada data
275	II3160	13	Teknologi dan aplikasi berbasis web	1. Pengenalan teknologi web, khususnya HTTP.\r\n2. Contoh aplikasi berbasis web	Tidak ada data	Tidak ada data
276	II3160	14	Pemrograman aplikasi berbasis web	Teknik pemrograman aplikasi berbasis web	Tidak ada data	Tidak ada data
277	II3160	14	Arsitektur berorientasi layanan dan web service	1. Pengenalan arsitektur berorientasi layanan\r\n2. Web service dan teknologi pendukungnya	Tidak ada data	Tidak ada data
278	II3160	15	Pemrograman web service	Pemrograman untuk membuat web service	Tidak ada data	Tidak ada data
279	II3160	15	Ringkasan penutup tentang teknologi untuk sistem terintegrasi	1. Ringkasan secara keseluruhan tentang sistem terintegrasi dan teknologi pendukungnya\r\n2. Tren ke depan	Tidak ada data	Tidak ada data
280	II3160	16	Ujian Akhir Semester (UAS)	Ujian tertulis	Tidak ada data	Tidak ada data
281	II3231	0	Belum ada	Belum ada	Belum ada	Belum ada
282	II3120	0	Belum ada	Belum ada	Belum ada	Belum ada
283	EL4233	1	Intro to the course	\r\n•Course objective, course syllabus\r\n•Intro to intelligent systems\r\n•Characteristic of intelligent systems\r\n\r\n	•Understand basic principles of intelligent system method\r\n•Compare model based design and intelligent system method\r\n	Nurofuzzy and Soft-computing
284	EL4233	2	Fuzzy set	\r\n•Basic definition and terminology\r\n•Membership function\r\n•Continuous and Discrete fuzzy set\r\n•Fuzzy set examples\r\n	•Understand fuzzy set\r\n•Characterize fuzzy set using membership function \r\n•Provide examples of fuzzy set\r\n•Compare crisp and fuzzy sets\r\n	Nurofuzzy and Soft-computing
285	EL4233	3	Fuzzy set operations \r\nMF formulation\r\n	\r\n•Fuzzy intersection, fuzzy union, fuzzy complement, fuzzy subset\r\n•MF shape and formulation\r\n	•Perform fuzzy set operations\r\n•Formulate MF\r\n	Neurofuzzy and Soft-computing
286	EL4233	4	Matlab programming of fuzzy concepts	•Matlab code of MF\r\n•Fuzzy set operation in Matlab\r\n	•Program MF in matlab\r\n•Program fuzzy set operation in Matlab\r\n	Neurofuzzy and Soft-computing
287	EL4233	5	Fuzzy Relation and Fuzzy Rules	•Fuzzy relation\r\n•Fuzzy composition : max-min composition, max-product composition\r\n•Fuzzy rules\r\n•Linguistic variables\r\n	•Understand fuzzy relation and its MF\r\n•Perform fuzzy composition\r\n•Determine MF of fuzzy rule\r\n•Derived composite linguistic values from primary linguistic values \r\n	Neurofuzzy and Soft-computing
288	EL4233	6	Fuzzy Reasoning and Its Matlab program	•Fuzzy reasoning\r\n•Matlab code for fuzzy relations, fuzzy rule composition\r\n	•Understand fuzzy reasoning\r\n•Program fuzzy relation, fuzzy rule composition in Matlab\r\n	Neurofuzzy and Soft-computing
289	EL4233	7	Fuzzy Inference System (FIS)	•FIS with single antecedent\r\n•Mamdani FIS with multiple antecedents\r\n	•Compute FIS for single and multiple antecedents\r\n•Perform graphical representation of FIS with two-inputs and one output\r\n	Neurofuzzy and Soft-computing
290	EL4233	8	Fuzzy Control	•Fuzzy control architecture\r\n•Fuzzy control components : fuzzification, defuzzification, fuzzy rules, fuzzy inference\r\n	•Comprehend fuzzy control\r\n•Compute defuzzification\r\n	Neurofuzzy and Soft-computing\r\nFuzzy Control and Identification\r\n
291	EL4233	9	Fuzzy Control Design and Matlab	•Fuzzy rules construction\r\n•Fuzzy control in Matlab\r\n	•Develop fuzzy rules based on ideal respons\r\n•Design fuzzy control using Matlab\r\n	Fuzzy Control and Identification
292	EL4233	10	Fuzzy control design examples and fuzzy embedded control	•Fuzzy control applications\r\n•Fuzzy rules examples\r\n•Fuzzy embedded processor\r\n•Fuzzy programming\r\n	•Design fuzzy control\r\n•Understand fuzzy embedded processor/controller\r\n•Fuzzy programming on microcontroller\r\n	Fuzzy Control and Identification
293	EL4233	11	Intro to Mobile robots	•Intro to mobile robots\r\n•Robot sensor\r\n•Robot control\r\n	•Undersand mobile robot\r\n•Understand robot sensor\r\n•Understand robot control \r\n	Mobile robot
294	EL4233	12	Biological Neural Networks and neuron Model	•Biological neural networks\r\n•Neuron model and computation\r\n	•Understand basic principles of biological neural networks\r\n•Comprehend neuron model\r\n•Compute output of nuron model given input signal\r\n	Neural Networks : A Comprehensive Foundation
295	EL4233	13	ANN learning	•Supervised learning\r\n•Perceptron model and perceptron learning algorithm\r\n•Adaptive linear networks\r\n•Matlab examples\r\n	•Understand supervised learning\r\n•Train perceptron to form classification\r\n•Comprehend adaptive linear networks and its learning algorithm\r\n	Neural Networks : A Comprehensive Foundation
296	EL4233	14	Feedforward Multilayer Neural Networks and Backpropagation	•MLNN topology\r\n•MLNN forward computation\r\n•MLNN backward computation\r\n•Matlab examples\r\n	•Comprehend MLNN \r\n•Perform forward computation\r\n•Perform back-propagation\r\n	Neural Networks : A Comprehensive Foundation
297	EL4233	15	ANN training with Backpropagation and Applications	•MLNN  training\r\n•ANN applications in pattern recognition and in control\r\n	•Understand ANN training mechanism\r\n•Understand ANN applications\r\n	Neural Networks : A Comprehensive Foundation
298	II3121	0	Belum ada	Belum ada	Belum ada	Belum ada
299	II3220	0	Belum ada	Belum ada	Belum ada	Belum ada
300	IF3152	0	Belum ada	Belum ada	Belum ada	Belum ada
301	II3240	0	Belum ada	Belum ada	Belum ada	Belum ada
302	II4090	0	Belum ada	Belum ada	Belum ada	Belum ada
303	KU206X	0	Belum ada	Belum ada	Belum ada	Belum ada
304	II4091	0	Belum ada	Belum ada	Belum ada	Belum ada
305	KU2071	1	Pendahuluan	•\tOutline kuliah\r\n•\tKontrak kuliah\r\n•\tEkspektasi yang diharapkan dari kuliah ini\r\n	•\tMenjelaskan mata kuliah ini sebagai salah satu mata kulian Pengembangan Kepribadian.\r\n•\tMenjelaskan apa yang akan dipelajari dari mata kuliah ini.\r\n•\tMenjelaskan soft skill apa yang diharapkan sesudah mengikuti mata kuliah ini\r\n	K
306	KU2071	2	Filsafat Pancasila	•\tPancasila sebagai falsafah dan dasar Negara	•\tMenjelaskan sejarah terbentuknya Pancasila.\r\n•\tMenjelaskan hakikat Pancasila bagi NKRI\r\n•\tMenjelaskan implikasi Pancasila bagi kehidupan berbangsa dan bernegara.\r\n	K&D
307	KU2071	3	Identitas Nasional	•\tIdentitas individu dan kelompok\r\n•\tIdentitas Nasional\r\n       Atribut \r\n•\tIdentitas nasional\r\n•\tPeran identitas nasional dalam pergaulan bangsa-bangsa di dunia\r\n\r\n	•\tMenjelaskan pengertian dan pentingnya identitas\r\n•\tMenjelaskan identitas dan atribut identitas nasional\r\n•\tMenjelaskan pentingnya identitas nasional dalam pergaulan dunia.\r\n	K&D
308	KU2071	4	Politik dan Strategi	•\tSistem konstitusi\r\n•\tSistem politik dan ketatanegaraan\r\n•\tSistem kekuasaan Negara\r\n	•\tMengenal system konstitusi Indonesia\r\n•\tDapat menganalisa sistem ketatanegaraan NKRI\r\n•\tDapat menganalisa sistem kekuasaan Negara NKRI\r\n	K & diskusi
309	KU2071	5	Sistem Pemeritahan Daerah	•\tOtonomi Daerah Sebagai sistem pemerintahan Daerah\r\n•\tImplikasi Otonomi Daerah\r\n	•\tMenjelaskan sistem Pemerintahan daerah dengan prinsip desentralisasi. \r\n•\tMenganalisis implikasi Otonomi Daerah terhadap negara.\r\n	K
310	KU2071	6	Pemerintahan Yang Baik dan Berwibawa	•\tKarakteristik Clean and Good Governance\r\n•\tTujuan dan manfaat clean and good goverrnance\r\n	M•\tMenjelaskan karakteristik clean and good governance\r\n•\tMendiskusikan tujuan dan manfaat clean and good governance\r\n	K
311	KU2071	7	Demokrasi Indonesia	•\tKonsep dan prinsip demokrasi\r\n•\tPerkembangan demokrasi di Indonesia\r\n•\tPartisipasi Warga dalam Negara demokratis\r\n	•\tMemahami konsep demokrasi\r\n•\tMenganalisis perkembangan demokratisasi di Indonesia.\r\n•\tMenganalisis tingkatan dan jenis pertisipasi warga sesuai dengan kualitas demokrasi di suatu Negara\r\n	K
312	KU2071	8	UTS	Tidak ada data	Tidak ada data	U
313	KU2071	9	Masyarakat Madani	-•\tPengertian dari masyarakat madani\r\n•\tKarakter masyarakat madani\r\n•\tPilar penegak masyarakat madani\r\n•\tMasyarakat madani dan demokrasi\r\n	•\tMenjelaskan pengertian masyarakat madani\r\n•\tMenjelaskan karakteristik masyarakat madani dalam sebuah Negara\r\n•\tMenjelaskan pilar-pilar yang membentuk masyarakat madani\r\n•\tMenjelaskan fungsi masyarakat madani dalam mewujudkan demokratisasi	K
314	KU2071	10	Hukum dan Hak asasi manusia (HAM) 	-•\tPengertian hukum\r\n•\tKaitan hukum dan HAM\r\n•\tPenegakan HAM di Indonesia\r\n	•\tMemahami konsep hukum\r\n•\tMemahami kaitan hokum dengan HAM\r\n•\tMemahami persoalan-persoalan HAM di Indonesia \r\n	K
315	KU2071	11	Geopolitik dan geostrategi Indonesia	•\tWilayah sebagai ruang hidup dan pemerintahan dalam aspek asta gatra.\r\n•\tKetahanan nasional dalam konsep geostrategi\r\n	Mahasiswa dapat\r\nmenjelaskan bagaimana\r\nIndonesia merumuskan\r\nWasantara	K
316	KU2071	12	Presentasi makalah kelompok	Tidak ada data	•\tMemahami konsep dan realitas ketatanegaran Indonesia \r\n•\tMencari solusi terhadap persoalan bangsa\r\n	diskusi
317	KU2071	13	Presentasi makalah kelompok	Tidak ada data	Mahasiswa dapat\r\nmenjelaskan arti Tannas\r\nsebagai kondisi maupun\r\nsebagai metode berfikir\r\nkomprehensif	diskusi
318	KU2071	14	Presentasi makalah kelompok	Tidak ada data	Mahasiswa dapat\r\nmenjelaskan kondisi\r\nTannas setiap aspek	diskusi
319	KU2071	15	Presentasi makalah kelompok	Tidak ada data	Mahasiswa dapat\r\nmenjelaskan arti dan\r\nmenunjukkan bentuk\r\nperwujudannya	diskusi
320	KU2071	16	UAS	Tidak ada data	Tidak ada data	U
321	II4470	0	Belum ada	Belum ada	Belum ada	Belum ada
322	BI2001	1	Pendahuluan: Tinjauan Umum Isu Lingkungan	&#61607;Modal alam (natural capital)\r\n&#61607; Tapak ekologi (ecological footprint)\r\n&#61607; Permasalahan umum lingkungan\r\n&#61607; Faktor penyebab masalah lingkungan\r\n&#61607; Prinsip-prinsip keberlanjutan\r\n	Mahasiswa dapat menjelaskan secara umum: \r\n&#61607;\tkondisi permasalahan lingkungan saat ini\r\n&#61607;\tpengertian modal alam \r\n&#61607;\tpentingnya prinsip keberlanjutan \r\n	K
323	BI2001	2	Konsep Ekosistem	&#61607;\tSistem pendukung kehidupan: atmosfer, hidrosfer, geosfer, biosfer\r\n&#61607;\tPengertian ekosistem\r\n&#61607;\tKomponen ekosistem: biotik dan abiotik\r\n&#61607;\tProses ekosistem: aliran energi dan daur materi\r\n	Mahasiswa dapat:\r\n&#61607;\tmenjelaskan konsep ekosistem  \r\n&#61607;\tmengidentifikasi struktur (komponen) dan fungsi (proses) ekosistem \r\n	K
324	BI2001	3	Ekosistem Sebagai Modal Alam (Natural Capital)	&#61607;\tIklim dan pembentukan bioma/ekosistem\r\n&#61607;\tEkosistem terestrial\r\n&#61607;\tEkosistem akuatik\r\n&#61607;\tEkosistem transisi/lahan basah\r\n	Mahasiswa dapat: \r\n&#61607;\tmenjelaskan peran iklim dalam pembentukan ekosistem  \r\n&#61607;\tmengidentifikasi jenis-jenis ekosistem alami yang berbeda\r\n&#61607;\tmenjelaskan kepentingan ekosistem sebagai modal alam \r\n	K
325	BI2001	4	Populasi Manusia dan Dampaknya	&#61607;\tPertumbuhan populasi manusia & faktor penyebabnya\r\n&#61607;\tMasalah kependudukan; demografi\r\n&#61607;\tManusia dan degradasi modal alam\r\n&#61607;\tSolusi yang berkelanjutan\r\n	Mahasiswa dapat:\r\n&#61607;\tmendeskripsikan kondisi dan kecenderungan pertumbuhan populasi manusia\r\n&#61607;\tmenjelaskan hubungan kependudukan dan kondisi lingkungan\r\n&#61607;\tmenjelaskan peran manusia dalam mendegradasi modal alam \r\n&#61607;\tmendeskripsikan solusi yang berkelanjutan\r\n	K
326	BI2001	5	Isu-Isu Lingkungan Global	&#61607;\tIsu-isu lingkungan pada forum internasional\r\n&#61607;\tJasa lingkungan (ecosystem services) sebagai penentu kesejahteraan manusia\r\n	Mahasiswa dapat:\r\n&#61607;\tmenyatakan isu-isu lingkungan yang tengah berkembang pada forum internasional\r\n&#61607;\tmenjelaskan pengertian jasa lingkungan dan kaitannya dengan kesejahteraan manusia\r\n	K
327	BI2001	6	Keberlanjutan Keanekaragaman Hayati: Evolusi & Interaksi Spesies	&#61607;\tPengertian keanekaragaman hayati (KH) pada tingkat genetik, spesies, ekosistem & fungsional\r\n&#61607;\tKH sebagai modal alam \r\n&#61607;\tEvolusi KH\r\n&#61607;\tPertumbuhan populasi\r\n&#61607;\tInteraksi spesies\r\n	Mahasiswa dapat: \r\n&#61607;\tmenjelaskan pengertian KH dan kepentingannya sebagai modal alam\r\n&#61607;\tmenjelaskan proses evolusi KH\r\n&#61607;\tmenjelaskan interaksi antar spesies dan mengidentifikasi contoh-contoh\r\n	K
328	BI2001	7	Keberlanjutan Keanekaragaman Hayati 2: 	&#61607;\tAncaman terhadap KH\r\n&#61607;\tKonservasi KH: pendekatan spesies\r\n&#61607;\tKonservasi KH: pendekatan ekosistem\r\n&#61607;\tContoh-contoh kasus di Indonesia\r\n	Mahasiswa dapat\r\n&#61607;\tmendeskripsikan ancaman terhadap KH\r\n&#61607;\tmenjelaskan pendekatan konservasi KH berbasis spesies dan ekosistem\r\n&#61607;\tmemberikan contoh-contoh kasus\r\n	K
329	BI2001	8	UTS	Tidak ada data	Tidak ada data	U
330	BI2001	9	Keberlanjutan Sumberdaya Alam 1: Lahan, Tanah & Air  	&#61607;\tTanah & sumberdaya pangan\r\n&#61607;\tKetahanan pangan\r\n&#61607;\tIsu-isu lingkungan terkait produksi pangan: pengelolaan hama dll.\r\n&#61607;\tSumberdaya air\r\n&#61607;\tIsu-isu lingkungan terkait air: penyediaan air tawar, distribusi air, pencemaran, banjir dll.\r\n&#61607;\tSolusi berkelanjutan\r\n&#61607;\tContoh-contoh kasus di Indonesia\r\n	Mahasiswa dapat:\r\n&#61607;\tmenjelaskan kepentingan lahan, tanah dan air sebagai sumberdaya\r\n&#61607;\tmenjelaskan isu-isu utama terkait penyediaan pangan dan penggunaan air\r\n&#61607;\tmenjelaskan alternatif solusi yang berkelanjutan\r\n	K
331	BI2001	10	Keberlanjutan Sumberdaya Alam 2: Mineral & Energi	&#61607;\tProses geologi \r\n&#61607;\tSumberdaya mineral & dampak penggunaannya\r\n&#61607;\tEnergi tak terbarukan\r\n&#61607;\tEfisiensi energi & energi terbarukan\r\n&#61607;\tSolusi berkelanjutan\r\n&#61607;\tContoh-contoh kasus di Indonesia\r\n	Mahasiswa dapat:\r\n&#61607;\tmenjelaskan proses geologi yang membentuk sumberdaya mineral dan energi tak terbarukan\r\n&#61607;\tmenjelaskan aspek energi, efisiensi energi dan isu terkait\r\n&#61607;\tmenjelaskan alternatif solusi yang berkelanjutan\r\n	K
332	BI2001	11	Keberlanjutan Kualitas Lingkungan 1: Risiko Lingkungan & Kesehatan Manusia	&#61607;\tRisiko lingkungan\r\n&#61607;\tRisiko biologis, ekotoksikologi: penyakit\r\n&#61607;\tRisiko kimiawi\r\n&#61607;\tAtmosfer & pencemaran udara\r\n&#61607;\tSumber utama pencemaran udara\r\n&#61607;\tPerubahan iklim dan penipisan lapisan ozon\r\n&#61607;\tSolusi berkelanjutan\r\n&#61607;\tContoh-contoh kasus di Indonesia\r\n	Mahasiswa dapat:\r\n&#61607;\tmengidentifikasi sumber risiko lingkungan yang mengancam kesehatan manusia\r\n&#61607;\tmenjelaskan isu lingkungna terkait perubahan iklim dan atmosfer\r\n&#61607;\tmenjelaskan alternatif solusi yang berkelanjutan\r\n	K
333	BI2001	12	Keberlanjutan Kualitas  Lingkungan 2: Limbah & Lingkungan Perkotaan	&#61607;\tMacam-macam limbah dan permasalahan yang terkait\r\n&#61607;\tIsu-isu lingkungan perkotaan: urbanisasi/kependudukan, transportasi dll.\r\n&#61607;\tSolusi berkelanjutan\r\n&#61607;\tContoh-contoh kasus di Indonesia\r\n	Mahasiswa dapat:\r\n&#61607;\tmengidentikasi perbedaan antar jenis limbah\r\n&#61607;\tmenjelaskan isu-isu utama terkait  lingkungan perkotaan\r\n&#61607;\tmenjelaskan alternatif solusi yang berkelanjutan\r\n	K
334	BI2001	13	Mahasiswa dapat:\r\n&#61607;\tmengidentikasi perbedaan antar jenis limbah\r\n&#61607;\tmenjelaskan isu-isu utama terkait  lingkungan perkotaan\r\n&#61607;\tmenjelaskan alternatif solusi yang berkelanjutan\r\n	&#61607;\tKaitan sistem ekonomi dengan biosfer\r\n&#61607;\tPemberian nilai terhadap modal alam, pengendalian pencemaran, dan pemanfaatan sumberdaya \r\n&#61607;\tPenggunaan perangkat ekonomi dalam penanganan masalah lingkungan\r\n&#61607;\tKaitan kemiskinan & masalah lingkungan\r\n	Mahasiswa dapat:\r\n&#61607;\tmenjelaskan prinsip-prinsip ekonomi penting yang terkait dengan isu lingkungan\r\n&#61607;\tmenjelaskan penggunaan perangkat ekonomi dalam penanganan masalah lingkungan \r\n&#61607;\tmenjelaskan isu kemiskinan sebagai salah satu sumber masalah lingkungan\r\n	K
335	BI2001	14	Keberlanjutan Manusia Sebagai Masyarakat 2: Politik dan Lingkungan	&#61607;\tPeran pemerintah dalam menciptakan keberlanjutan masyarakat \r\n&#61607;\tKebijakan lingkungan: hukum dan peraturan\r\n&#61607;\tPeran organisasi non-pemerintah dan LSM\r\n&#61607;\tKasus-kasus khusus Indonesia \r\n	Mahasiswa dapat:\r\n&#61607;\tmenjelaskan peran pemerintah dalam menetapkan kebijakan lingkungan\r\n&#61607;\tmenyatakan contoh-contoh terkait kebijakan lingkungan (terutama di Indonesia) \r\n	K
336	BI2001	15	Pandangan Hidup (Worldview), Etika & Keberlanjutan Lingkungan	&#61607;\tPandangan hidup (worldview) lingkungan: planetary management, stewardship & environmental wisdom\r\n&#61607;\tPeran pendidikan dalam mendukung tercapainya keberlanjutan\r\n&#61607;\tContoh-contoh keberhasilan\r\n	Mahasiswa dapat:\r\n&#61607;\tMenjelaskan adanya perbedaan pandangan (worldview) terhadap lingkungan\r\n&#61607;\tMenjelaskan peran pendidikan dalam tercapainya keberlanjutan lingkungan\r\n&#61607;\tmemberikan worldview pribadi tentang lingkungan dan mendiskusikannya \r\n&#61607;\tmemberikan rangkuman, kesimpulan dan pendapat terhadap keseluruhan materi mata kuliah\r\n	K
337	BI2001	16	UAS	Tidak ada data	Tidak ada data	U
338	II4471	0	Belum ada	Belum ada	Belum ada	Belum ada
339	II4092	0	Belum ada	Belum ada	Belum ada	Belum ada
340	II4472	0	Belum ada	Belum ada	Belum ada	Belum ada
342	II4444	2	Pemrograman Dasar	Java: Pengantar Python,\r\nJVM,\r\nTransformasi/Konversi\r\n	paham syntax	web
343	II4444	3	Pemrograman Dasar Tahap awal	Java: Pengantar Python, pascal,\r\nJVM,\r\nTransformasi/Konversi\r\n	paham syntax python	web, buku
\.


--
-- Data for Name: silabus; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.silabus (kode_matkul, nama_matkul, silabus_ringkas, silabus_lengkap, luaran, mata_kuliah_terkait, kegiatan_penunjang, pustaka, panduan_penilaian, catatan_tambahan) FROM stdin;
MA1101	Matematika IA	Sistem Bilangan Real, Pertaksamaan, Fungsi dan Limit, Turunan, Penggunaan Turunan, Integral, Penggunaan Integral dan Fungsi Transenden	Mata kuliah ini adalah bagian pertama dari seri kalkulus untuk bidang-bidang sains dan rekayasa. Materi yang dibahas antara lain, Sistem Bilangan Real, Pertaksamaan, Fungsi dan Limit, Turunan, Penggunaan Turunan, Integral, Penggunaan Integral dan Fungsi Transenden	Setelah mengikuti kuliah ini, mahasiswa diharapkan memiliki:\r\nKeterampilan teknis baku yang didukung oleh konsep, rumus, metode, dan penalaran yang sesuai;Pola berpikir yang kritis, logis dan sistematis,; serta kreativitas dalam pemecahan masalah yang terkait dengan Matematika;Kemampuan mengkomunikasikan hasil pemikiran dan pekerjaannya baik secara lisan maupun tulisan;Kesiapan untuk mempelajari mata kuliah lain, yang memerlukan matematika sebagai prasyarat, secara mandiri.	Tidak ada data	Tidak ada data	\n                                    Dale Varberg, Edwin Purcel and Steve Rigdon,\n                                    Calculus,\n                                    9,\n                                    Prentice Hall,\n                                    \n                                \n                                    James Stewart,\n                                    Calculus,\n                                    4,\n                                    Brooks/Cole Publishing Company,\n                                    \n                                \n                                    Thomas,\n                                    Calculus,\n                                    11,\n                                    Pearson Education,\n                                    \n                                	Tidak ada data	Tidak ada data
MA1201	Matematika IIA	Teknik Pengintegralan, Bentuk tak tentu dan Integral tak wajar, Deret Takhingga, Geometri di Bidang dan Ruang, Turunan di Rn , Integral Lipat Dua, Persamaan Diferensial Biasa.	Matakuliah ini adalah bagian kedua dari seri Kalkulus untuk bidang-bidang sains dan rekayasa. Materi yang dibahas antara lain, Teknik Pengintegralan, Bentuk tak tentu dan Integral tak wajar, Deret Takhingga, Geometri di Bidang dan Ruang, Turunan di Rn , Integral Lipat Dua, Persamaan Diferensial Biasa	Setelah mengikuti kuliah ini, mahasiswa diharapkan memiliki:\r\nKeterampilan teknis baku yang didukung oleh konsep, rumus, metode, dan penalaran yang sesuai;Pola berpikir yang kritis, logis dan sistematis; serta kreativitas dalam pemecahan masalah yang terkait dengan MatematikaKemampuan mengkomunikasikan hasil pemikiran dan pekerjaannya baik secara lisan maupun tulisan;Kesiapan untuk mempelajari matakuliah lain, yang memerlukan matematika sebagai prasyarat, secara mandiri.	Tidak ada data	Tidak ada data	\n                                    Dale Varberg, Edwin Purcell and Steve Rigdon,\n                                    Calculus,\n                                    9,\n                                    Prentice Hall,\n                                    2007\n                                \n                                    James Stewart,\n                                    Calculus,\n                                    4,\n                                    Brooks/Cole Publishing Company,\n                                    1999\n                                \n                                    Thomas,\n                                    Calculus,\n                                    11,\n                                    Pearson Education,\n                                    2005\n                                	Tidak ada data	Tidak ada data
FI1101	Fisika Dasar IA	Mekanika(Kinematika, Dinamika, Usaha – Energi), Gelombang Mekanik, Fluida \r\n(Statika dan Dinamika) dan Termofisika (Teori Kinetik Gas dan Termodinamika)	Kinematika Benda Titik,  Gerak Relatif,  Dinamika  Benda Titik (hukum-hukum Newton dengan konsep gaya, usaha dan energi, impuls dan momentum, hukum-hukum kekekalan), Dinamika Sistem Benda Titik (pusat massa). Gerak Rotasi (momentum sudut, rotasi benda tegar dengan sumbu tetap), Elastisitas dan Osilasi, Gelombang Mekanik, Statika dan Dinamika fluida,  Termofisika (teori kinetik gas, kalor dan usaha, hukum I termodinamika, efisiensi, siklus Carnot)	Menguasai konsep teoritis gejala fisis yang menyangkut berbagai gerakMenerapkan pemahamannya pada berbagai kasusMemahami dan mampu mengaplikasikan konsep usaha-energi dalam menyelesaikan persoalan mekanika sederhanaMampu merumuskan, menyelesaikan dan menganalisis persoalan statika dan dinamika sistem benda tegar.sederhanaMemahami dan mampu menyelesaikan persoalan statika fluida dan dinamika fluidaMemahami  hukum-hukum termodinamika dan mampu menyelesaikan dan menganalisis persoalan termodinamikaMemiliki kemampuan untuk merancang dan menyiapkan percobaan untuk memahami mekanika NewtonMemiliki kemampuan untuk melakukan percobaan dan mencatat hasil percobaan menggunakan peralatan yang sesuai untuk percobaan mekanika Newton.Memiliki kemampuan untuk melakukan percobaan yang memenuhi standar keselamatan dan kesehatanMemiliki kemampuan untuk menganalisa dan meninterprestasi data dalam percobaan Newton dengan menggunakan kemampuan matematika dan fisikaDapat mendesain suatu alat sederhana menggunakan konsep pada Fisika dasar IA	Tidak ada data	Tidak ada data	\n                                    Halliday, D., Resnick, R., and Walker, J.,\n                                    Principle of Physics,\n                                    9th,\n                                    John Wiley & Sons,\n                                    2011\n                                \n                                    Serway, R.A,\n                                    Physics for \r\nScientists \r\nand Engineers,\n                                    ,\n                                    Sander College,\n                                    1996\n                                \n                                    Alonso, M. & Finn, E.J.,\n                                    Physics,\n                                    ,\n                                    Addison Wesley,\n                                    1992\n                                	Tidak ada data	Tidak ada data
FI1201	Fisika Dasar IIA	Listrik Magnet,  Gelombang Elektromagnetik dan Fisika Modern	Elektrostatik (medan dan gaya listrik), Hukum Gauss,  Energi Potensial Listrik,  Potensial Listrik. Kapasitor. Magnetostatik, GGL Induksi Magnetik.  Arus Bolak-Balik,  Gelombang Elektromagnetik. Fisika Modern, Fisika Atom	Mengerti konsep dan prinsip dasar dalam elektromagnetisme dan fisika modernMenunjukkan kemampuan untuk melakukan eksperimen dalam mengukur besar medan magnet dalam solenoidMenunjukkan kemampuan melakukan eksperimen dalam mengukur arus dan potensial efektif dari rangkaian arus bolak balikDapat menggunakan amperemeter dan voltmeter untuk sumber arus searah dan dapat menganalisa jembatan WheatstoneMenunjukkan kemampuan melakukan eksperimen interferensi dan difraksiDapat menghitung gaya dan medan listrik yang dihasilkan oleh muatan diskrit dan kontinu menggunakan hukum Coulomb dan hukum GaussDapat menghitung energi potensial dan potensial listrik di sekitar muatan diskrit dan kontinu, dan mengaplikasikannya ke sistem kapasitorDapat menghitung medan magnet yang dihasilkan oleh arus listrik (hukum Biot Savart dan hukum Ampere)Dapat mengaplikasikan hukum induksi magnetik Faraday dan Lenz untuk menghasilkan electromotive Force (EMF)Dapat memecahkan masalah rangkaian arus searah dan bolak-balikMenjelaskan besaran-besaran gelombang elektromagnetik, energi gelombang, daya gelombang dan intensitas gelombang.Memecahkan persoalan pola interferensi N celah dan pola difraksi untuk celah lebar dan N celah (interferensi-difraksi)Dapat memecahkan problem relativitas khusus Einstein dan dualism partikel-gelombangDapat menganalisa eksperimen Fisika Modern (efek fotolistrik)Dapat mendesain alat sederhana menggunakan konsep Fisika dasar IIA (RBL)	Tidak ada data	Tidak ada data	\n                                    Halliday, D., Resnick, R., and Walker, J.,\n                                    Principle of Physics,\n                                    9th,\n                                    John Wiley & Sons,\n                                    2011\n                                \n                                    Serway, R.A,\n                                    Physics for \r\nScientists \r\nand Engineers,\n                                    ,\n                                    Sander College,\n                                    1996\n                                \n                                    Alonso, M. & Finn, E.J.,\n                                    Physics,\n                                    ,\n                                    Addison Wesley,\n                                    1992\n                                	Tidak ada data	Tidak ada data
KI1102	Kimia Dasar IB	Stoikhiometri, struktur atom dan molekul, wujud zat, dan energetika kimia	Unsur, senyawa dan tabel periodik, konsep mol, rumus empiris, rumus molekul, pereaksi pembatas, rendemen dan stoikiometri, reaksi kimia (asam-basa dan redoks) dalam larutan, struktur molekul, atom dan mekanika kuantum, ikatan kimia, struktur dan teori ikatan, wujud zat, diagram fasa, sifat gas, gaya antar molekul, dan termokimia kimia.	Mahasiswa mampu menjelaskan prinsip-prinsip kimia yang mendasari fenomena yang terjadi di alam sekitar	Tidak ada data	Tidak ada data	\n                                    Chang, R.,,\n                                    Essential Chemistry,,\n                                    2nd,\n                                    McGraw Hill, New York,\n                                    2000\n                                \n                                    Brady J.E., and J. Holum  R.,,\n                                    Chemistry, the study\r\nof Matter and its\r\nchanges.,\n                                    2nd,\n                                    John Wiley, New York,\n                                    1996\n                                	Tidak ada data	Tidak ada data
KI1202	Kimia Dasar IIB	Sifat-sifat larutan, kinetika kimia, kesetimbangan kimia, konsep asam-basa, kesetimbangan asam -basa, elektrokimia, reaksi nuklir.	Larutan ideal, sifat koligatif,  larutan elektrolit dan non-elektrolit, kuosien reaksi, tetapan kesetimbangan, prinsip Le Chatelier, faktor yang mempengaruhi kesetimbangan, energi bebas Gibbs dalam kesetimbangan, hukum laju, orde reaksi, waktu paro, faktor yang mempengaruhi laju reaksi, mekanisme reaksi, teori tumbukan dan keadaan transisi, teori asam-basa, pH, larutan penyangga, hidrolisis, reaksi asam-basa, Ksp, sel Volta, potensial reduksi standar, notasi sel, persamaan Nernst, sel elektrolisis, hukum Faraday, partikel penyusun inti atom, hukum laju peluruhan, waktu paro, tetapan laju peluruhan, reaksi inti fisi dan fusi, cacat massa, aplikasi radionuklida.	Mahasiswa mampu menjelaskan prinsip-prinsip kimia yang mendasari fenomena yang terjadi di alam sekitar.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU1101	Pengantar Rekayasa & Desain I	Kuliah ini berisi materi mengenai peran rekayasa dan desain dalam masyarakat, profesi insinyur, aspek-aspek dalam rekayasa, elemen kunci dalam analisis rekayasa, langkah penyelesaian masalah, konsep energi, konversi dan konservasi, penerapan prinsip sains dan matematika dalam rekayasa serta pengenalan beberapa disiplin rekayasa dan interaksinya. 	Kuliah ini berisi materi mengenai peran rekayasa dan desain dalam masyarakat, profesi insinyur, aspek-aspek dalam rekayasa, elemen kunci dalam analisis rekayasa, langkah penyelesaian masalah, konsep energi, konversi dan konservasi, penerapan prinsip sains dan matematika dalam rekayasa serta pengenalan beberapa disiplin rekayasa dan interaksinya.  Setelah mengikuti kuliah ini diharapkan mahasiswa termotivasi mempelajari ilmu rekayasa. Kuliah diberikan melalui tatap muka. Alat penilaian adalah pekerjaan rumah, tugas kelompok, ujian tengah semester dan ujian akhir semester.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU1201	Pengantar Rekayasa & Desain II	Matakuliah ini memberikan pemahaman terhadap rekayasa dan desain secara lebih mendalam melalui kegiatan proyek dalam kelompok.	Silabus Lengkap\tMatakuliah ini memberikan pemahaman terhadap rekayasa dan desain secara lebih mendalam melalui kegiatan proyek dalam kelompok. 	Tidak ada data	KU1101 Pengantar Rekayasa & Desain I (Prasyarat Sudah Ambil)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU1072	Pengenalan Teknologi Informasi B	Mata kuliah ini memberikan kecakapan dasar teknologi informasi untuk kehidupan akademik dan profesi pesertanya serta kemampuan berpikir komputasional melalui pemrograman dasar dalam paradigma prosedural	Berpikir komputasional melalui pemrograman komputer dasar dengan paradigma prosedural, terdiri atas: \r\n-\tMakna dan teknik berpikir komputasional\r\n-\tPemrograman komputer dengan paradigma prosedural\r\n-\tBeberapa construct dasar dalam pemrograman prosedural: I/O, sekuens, analisis kasus, pengulangan, subprogram, array/matriks, file eksternal\r\n-\tMenuliskan alur berpikir prosedural dengan flow chart dan notasi algoritmik\r\n-\tSintaks bahasa pemrograman bahasa pemrograman (C/Pascal/Fortran/dll)\r\nPengantar teknologi informasi, terdiri atas:\r\n-\tSistem dan organisasi komputer (perangkat keras dan  lunak) serta jaringan telekomunikasi\r\n-\tPemanfaatan teknologi inforamsi dalam berbagai aspek kehidupan, khususnya dalam kehidupan akademik dan profesional.\r\n-\tImplikasi penggunaan teknologi informasi dalam berbagai aspek kehidupan manusia.	Tidak ada data	KU1071 Pengenalan Teknologi Informasi A (Ambil Salah Satu Saja)	Tidak ada data	\n                                    G. Beekman and B. Beekman,\n                                    Digital Planet: Tomorrow’s Techology and You,\n                                    10,\n                                    Prentice Hall,\n                                    2012\n                                \n                                     Walter Savitch,,\n                                    C++ : Problem Solving with C++,\n                                    8,\n                                    ,\n                                    \n                                \n                                     Walter Savitch,,\n                                    Pascal: An Introduction to the Art and Science of Programming,\n                                    4,\n                                    ,\n                                    \n                                	Tidak ada data	Tidak ada data
KU1011	Tata Tulis Karya Ilmiah	Mahasiswa\r\nmendapatkan materi\r\nejaan; tata kata;\r\ntata kalimat;\r\nistilah,\r\nsilogisme, dan\r\ndefinisi; paragraf;\r\nperancangan karya\r\ntulis ilmiah;\r\npenyusunan kerangka;\r\nkomponen karya tulis\r\nilmiah; serta\r\nkonvensi naskah.	Mahasiswa\r\nmendapatkan materi\r\nragam bahasa tulis\r\nilmiah dan cirinya;\r\npenulisan huruf,\r\nkata, unsur serapan,\r\ndan pemakaian tanda\r\nbaca; pembentukan\r\nkata dan pemakaian\r\nbentukan kata dalam\r\nkalimat; pola\r\nkalimat baku,\r\nkalimat\r\nefektif, dan variasi\r\nkalimat; tata\r\nistilah, definisi,\r\ndan silogisme;\r\nsyarat, jenis, dan\r\npengembangan\r\nparagraf; pemilihan\r\ntopik, tema, judul,\r\ndan\r\npenyusunan ragangan;\r\nbab pendahuluan,\r\npermasalahan,\r\nanalisis, dan\r\nsimpulan;\r\npelengkap awal dan\r\npelengkap akhir; \r\npengetikan,\r\npengutipan, dan\r\npustaka.	Mahasiswa terampil berkomunikasi baik lisan maupun tulisan menggunakan bahasa Indonesia yang benar dengan baik dan menjunjung tinggi asas orisinalitas (kejujuran) sesuai dengan profesi masing-masing di dunia kerja	Tidak ada data	Tidak ada data	\n                                    Alwi Hasan.et.al.,\n                                    Tata Bahasa Baku Bahasa Indonesia.,\n                                    ,\n                                    Jakarta : Balai Pustaka,\n                                    \n                                \n                                    Keraf, Gorys,,\n                                    Komposisi .,\n                                    ,\n                                    Ende Flores : Nusa Indah,\n                                    \n                                \n                                    Depdikbud RI.,\n                                    Pedoman Pembentukan Istilah.,\n                                    ,\n                                    Jakarta:Balai Pustka,\n                                    \n                                \n                                    Depdikbud RI.,\n                                    Pedoman Umum Ejaan Baku,\n                                    ,\n                                    Jakarta:Balai Pustaka,\n                                    \n                                \n                                    Seksi Bahasa Indonesia Dep.Sosio Teknologi-ITB 200,\n                                    Bahasa Indonesia Ilmiah Tata Tulis Karya Ilmiah Latihan.,\n                                    ,\n                                    Bandung Penerbit ITB,\n                                    \n                                	Tidak ada data	Tidak ada data
KU102X	Bahasa Inggris (KU1021/1022/1023)	Tidak ada data	Tidak ada data	Tidak ada data	KU1021 Bahasa Inggris: Pemahaman Teks Akademik (Substitusi)KU1022 Bahasa Inggris: Penulisan Teks Akademik (Substitusi)KU1023 Bahasa Inggris: Teknik Presentasi (Substitusi)KU1024 Bahasa Inggris (Substitusi)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
EL1200	Pengantar Analisis Rangkaian	Konsep dasar, hukum-2 dasar, metoda analisis, teorema rangkaian, operational amplifier, kapasitor & induktor, rangkaian orde-1, rangkaian orde-2, sinusoida dan fasor, dan analisis sinusoida dalam keadaan tunak.	Konsep Dasar: muatan dan arus, Tegangan, daya dan energi, elemen rangkaian.\r\nHukum dasar: hukum Ohm dan Kirchhoff, seri resistor & pembagi tegangan, resistor paralel dan pembagi arus, konversi delta-Wye.\r\nMetode analisis: Nodal dan Mesh.\r\nTeorema rangkaian: superposisi, transformasi sumber, teorema Thevenin & Norton, transfer daya maksimum.\r\nOperasional Amplifier: ideal op amp, inverter & non-inverter amplifier, rangkaian penguat penjumlah, penguat pengurang, dan penguat cascade.\r\nRangkaian orde pertama: sumber bebas dan “step” respon dari rangkaian  RL dan RC , dan fungsi tunggal.\r\nRangkaian Orde kedua : sumber bebas dan “step” respon dari rangkaian seri & parallel RLC.\r\nRangkaian sinusoidal: konsep fasor, dan hubungannya pada elemen rangkaian, pemodelan rangkaian dan analisis steady state  orde 1 dan orde 2.\r\n	At the end of this subject, students should be able to:1. Understand the fundamental concepts of charge, current, voltage, power, energy, and circuit elements.2. Apply basic laws (Ohm & Kirchhoff) for analysis resistive networks using nodal and mesh analysis.3. Analyse resistive networks and simplify complicated networks using various circuit theorems (superposition, source transformation, Thevenin & Norton, maximum power transfer).4. Analysis and design simple networks containing operational amplifier.5. Deal with circuit containing energy storage elements.6. Determine transient and steady state response of the first and second order circuits.7. Perform phasor frequency domain analysis.8. Use SPICE to analyst DC and AC circuits.9. Use Matlab as a tool to solve the network problems.	FI1101 Fisika Dasar IA (Prasyarat Sudah Ambil)MA1101 Matematika IA (Prasyarat Sudah Ambil)	Tidak ada data	\n                                    Charles K. Alexander & Matthew N.O.Sadiku,\n                                    Fundamentals of     Electric Circuits,\n                                    5th,\n                                    Mc Graw-Hill International,\n                                    2013\n                                \n                                    R.C. Dorf & J. A. Svoboda,\n                                    Introduction to Electric Circuits,\n                                    6th,\n                                    John Wiley & Sons,\n                                    2004\n                                	Tidak ada data	Tidak ada data
II4092	Tugas Akhir 2	Pada mata kuliah ini, mahasiswa melakukan analisis masalah dan solusi, desain, implementasi solusi, serta presentasi tulisan dan lisan atas tugas akhir yang dikerjakannya.	Topik-topik yang dibahas mencakup: gambaran umum tugas akhir, kerja mandiri, penulisan laporan tugas akhir, isu plagiarisme, presentasi, dan penulisan abstrak tugas akhir.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU1001	Olah Raga	Pada kuliah ini mahasiswa dibekali pengetahuan tentang pentingnya berolahraga dengan diberikan teori tentang kesehatan, Kebugaran jasmani, dan Olah raga, Gizi olah raga, Prinsip-prinsip Pelatihan, dan Latihan Kondisi Fisik sehingga saat kuliah praktek mampu berolahraga dengan benar dan tujuan pelatihan dapat tercapai.	Pada kuliah ini mahasiswa dibekali pengetahuan tentang teori dan praktek olahraga yang meliputi kesehatan, kebugaran, dan Olah raga, Gizi Olah raga, Prinsip-prinsip Pelatihan, Latihan Kondisi Fisik dan berbagai macam game cabang olahraga	Setelah mengikuti kuliah ini diharapkan mahasiswa mampu menjaga dan meningkatkan derajat kebugaran jasmani dan mampu memahami nilai-nilai positif dari olahraga serta dapat mengaplikasikannya dalam kehidupan di kampus atau masyarakat umum	Tidak ada data	Tidak ada data	\n                                    Bompa, T.O,\n                                    Theory and Methodology of Training, Iowa,\n                                    ,\n                                    Kendal/Hunt Publishing Company,\n                                    1994\n                                \n                                    Daniel Goleman,\n                                    Emotional Intellegence,,\n                                    ,\n                                    PT. Gramedia.Pustaka,\n                                    1997\n                                \n                                    Giriwijoyo, S., Y.S. dkk,\n                                    Manusia dan Olahraga,\n                                    ,\n                                    ITB,\n                                    2005\n                                \n                                    Harsono,\n                                    Coaching dan Aspek-asapek Psikologis dalam Coaching,\n                                    ,\n                                    CV. Tambak        Kusuma.Pustaka,\n                                    1988\n                                \n                                    . Snow Harrison,\n                                    The Power of Team Building,,\n                                    ,\n                                    ,\n                                    1992\n                                \n                                    Willmore, H., Jack & Costill, L., David,\n                                    Physiology of Sport and Health Exercise,\n                                    ,\n                                    ,\n                                    1999\n                                	Tidak ada data	Tidak ada data
IF1210	Dasar Pemrograman	Mata kuliah ini\r\nmengenalkan tentang\r\nkonsep  fundamental\r\npemrograman:\r\nabstraksi,\r\ndekomposisi problem,\r\nmodularisasi,\r\nrekurens;\r\nskill/praktek\r\npemrograman skala\r\nkecil (aspek\r\nkoding); dan\r\nmemberikan peta\r\ndunia\r\npemrograman untuk\r\ndapat mempelajari\r\npemrograman secara\r\nlebih mendalam pada\r\ntahap berikutnya.	Konsep fundamental\r\npemrograman:\r\nabstraksi,\r\ndekomposisi problem,\r\nmodularisasi,\r\nberpikir rekursif\r\ndan prosedural \r\nTools dan term\r\npemrograman: bahasa\r\npemrograman, IDE,\r\ncompiler,\r\ninterpreter,\r\nsource code, machine\r\ncode, eksekusi\r\nprogram, algoritma,\r\ndll\r\nPemrograman (aspek\r\nkoding) dengan\r\nparadigma\r\npemrograman\r\nfungsional dan\r\nprosedural, dengan\r\nbeberapa bahasa yang\r\ndipilih untuk skala\r\nkecil dan\r\npersoalan umum.\r\nGambaran besar dunia\r\npemrograman dalam\r\nbidang computing,\r\nyang menantang dan\r\nmenumbuhkan minat\r\n(the beauty and joy\r\nof computer\r\nprogramming)	Mahasiswa mengenal dan memahami semua pemikiran, istilah, tools yang dipakai dalam menyelesaikan persoalan melalui pembuatan progam.Mahasiswa mengenal kelas-kelas persoalan dan kelas-kelas programmer dan kelas-kelas program.Mahasiswa mengenal teknik-teknik yang dibutuhkan dalam memrogram dan mempraktekkan dengan skala kecilMahasiswa mampu mengkode program sederhana berskala kecil dan persoalan umum.Mahasiswa menjadi tertantang dan berminat untuk mempelajari pemrograman secara lebih mendalam pada tahap berikutnya.	Tidak ada data	Tidak ada data	\n                                    Liem, Inggriani,\n                                    Diktat Kuliah IF222 Struktur Data,\n                                    -,\n                                    Departemen Teknik Informatika ITB,\n                                    2003\n                                \n                                    Liem, Inggriani,\n                                    Diktat Kuliah IF223 Algoritma dan Pemrograman,\n                                    -,\n                                    Departemen Teknik Informatika ITB,\n                                    1998\n                                	Tidak ada data	Tidak ada data
II2110	Matematika STI	Memahami Matematika Diskrit, Logika Proporsional, Himpunan, Kalkulus Predikat, Relasi, Struktur Diskrit, Bilangan, Program/Algoritma	Memahami Matematika Diskrit, Logika Proporsional, Himpunan, Kalkulus Predikat, Relasi, Struktur Diskrit, Bilangan, Program/Algoritma	Student understand:1)Understanding of Discrete Mathematic2)Proportional Logic3)Sets4)Predicate Calculus5)Relations6)Discrete Structure7)Numbers8)Program/Algorithm	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II2230	Jaringan Komputer	Kuliah ini bertujuan untuk memberikan pengetahuan tentang jaringan komputer  yang mencakup lapisan fisik, datalink (LAN), network (IP), transport, dan aplikasi. Selain itu mahasiswa juga diberikan pengetahuan tentang QoS, keamanan jaringan dan layanan-layanan multimedia.  Kuliah ini juga memberikan tugas-tugas dan latihan untuk menumbuhkan keterampilan mahasiswa dalam mengelola jaringan komputer dan membuat aplikasi-aplikasi di lingkungan jaringan TCP/IP.	Kuliah ini bertujuan untuk memberikan pengetahuan tentang jaringan komputer  yang mencakup lapisan fisik, datalink (LAN), network (IP), transport, dan aplikasi. Selain itu mahasiswa juga diberikan pengetahuan tentang QoS, keamanan jaringan dan layanan-layanan multimedia.  Kuliah ini juga memberikan tugas-tugas dan latihan untuk menumbuhkan keterampilan mahasiswa dalam mengelola jaringan komputer dan membuat aplikasi-aplikasi di lingkungan jaringan TCP/IP.	Setelah menyelesaikan kuliah ini mahasiswa dapat menjelaskan prinsip-prinsip kerja protokol-protokol, proses-proses dan algoritma layanan-layanan yang ada di dalam setiap layer TCP/IP, mahasiswa dapat menggunakan / mengelola jaringan TCP/IP, mahasiswa dapat membuat aplikasi untuk inspeksi paket, aplikasi manajemen QoS dan aplikasi untuk keamanan jaringan TCP/IP.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II2111	Probabilitas dan Statistik	Konsep probabilitas, variabel random dan distribusinya, elemen geometrik kombinatorial, probabilitas kondisional, teorema Bayes, fungsi distribusi, variabel random bivirate, fungsi variabel random, estimasi, pengujian hipotesis, penerapan probabilitas statistik pada domain computing & electrical engineering	Konsep probabilitas, variabel random dan distribusinya, elemen geometrik kombinatorial, probabilitas kondisional, teorema Bayes, fungsi distribusi, variabel random bivirate, fungsi variabel random, estimasi, pengujian hipotesis, penerapan probabilitas statistik pada domain computing & electrical engineering	1)Understanding of probability and statistic2)Understanding of probability and statistic techniques3)Applying probability and statistic techniques for many cases	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
EL2244	Sistem & Arsitektur Komputer	Kuliah ini membahas tentang sistem computer, mencakup perangkat keras, perangkat lunak, data dan prosedur, komunikasi dan manfaat bagi masyarakat penggunanya, sistem komputer enterprise	Membahas sistem computer digital modern dan timbal balik yang muncul ketika perangkat keras dan perangkat lunak saling berinteraksi. Topik meliputi: abstraksi computer, organisasi perangkat keras, instruction set architecture, representasi dan operasi data, sistem operasi, sistem pengolah (CPU), sistem memori, sistem input-output, analisis kinerja, serta pengantar arsitektur masa depan, sistem komputer enterprise	Tidak ada data	EL2142 Sistem Digital & Mikroprosesor (Prasyarat Sudah Ambil)IF1210 Dasar Pemrograman (Prasyarat Sudah Ambil)	Tidak ada data	\n                                    Randal E. Bryant and David R. Hallaron,\n                                    Computer Systems A Programmer's Perspective,\n                                    2nd,\n                                    Prentice Hall,\n                                    2010\n                                \n                                    John L. Hennessy and David A. patterson,\n                                    Computer Organization and Design: The Software Hardware Interface,\n                                    4th,\n                                    Morgan Kaufmann,\n                                    2009\n                                \n                                    William Stalling,\n                                    Computer Organization and Architecture: Designing for Performance,\n                                    8th,\n                                    Prentice Hall,\n                                    2010\n                                	Tidak ada data	Tidak ada data
EL2142	Sistem Digital & Mikroprosesor	Dasar-dasar logika digital dan sistem mikroprosesor. Meliputi rangkaian logika kombinasional dan sekuensial, perangkat programmable logic. sistem mikroprosesor meliputi hardware, software, perifer, antarmuka dan komunikasi	[Topik-topik berikut akan dibahas:\r\n1. Sistem Digital: komputer digital dan sistem digital; sistem angka biner, oktal dan heksadesimal; “complements”; “signed binary numbers”; kode desimal dan biner ; pengantar logika biner\r\n2. aljabar Boolean: definisi dasar, teori dan sifat aljabar Boolean; fungsi Boolean; bentuk standar fungsi Boolean; operasi logika\r\n3. Penyederhanaan fungsi Boolean: metode peta Karnaugh; kondisi “don’t care”/tidak peduli; NAND dan NOR\r\n\r\n4.rangkaian kombinasional: analisis dan desain prosedur; penambah, subtractors, rangkaian NAND/NOR bertingkat dan kode konversi; switch transistor dan pertimbangan desain praktis\r\n\r\n5. MSI dan PLD perangkat: besaran pembanding, decoder, encoders, multiplexer, “read-only” memory (ROM), “Programmable Logic Array” (PLA), dan “Programmable Array Logic” (PAL)\r\n\r\n6. Analisis rangkaian sekuensial sinkron: flip-flops; analisis rangkaian sekuensial dengan clock; pengurangan dan penentuan fungsi state\r\n7.  Desain rangkaian sekuensial: “flip-flop”,  tabel eksitasi, prosedur desain, desain “counter”\r\n\r\n8. Register, Counter dan perangkat memori: shift register, counter riak, counter sinkron, counter waktu , dan Random Access Memory (RAM)\r\n9. arsitektur Mikroprosesor sistem: von Neumann, dan mesin Harvard.\r\n10. Perangkat keras sistem Mikroprosesor:”bus”, “bus timing”, “stack”, “interrupt”, alamat decoder, perifer dan interfacing, komunikasi serial\r\n11.   Perangkat lunak sistem Microprocessor sistem software: flow chart, “super loop and foreground background architecture”, timer perangkat lunak, ADC, PWM, komunikasi serial, Digital I / O (LED, 7 segmen, matriks LED, switch, keypad), Analog I / O (sensor)\r\n12. Microprocessor dan metodologi desainnya\r\n	1. Be able to represent and manipulate numbers in the binary two 's complement number system, and convert numbers between different positional number systems. Be able to do negation and addition in the two s complement number system, and detect overflow.2. Carry out transformations of Boolean algebra expressions, using the theorems of Boolean algebra and Karnaugh maps. The student can find the minimal sum-of-products (SOP) and product-of-sums (POS) expressions, and create a corresponding circuit from AND, OR, NAND, and NOR gates.3. The student will be able to analyze the functional and electrical behavior of digital CMOS circuits, including noise margins, allowable fan-in/out, and power dissipation. Given an NMOS or CMOS circuit diagram, the student can determine its logic function, using switch models for the transistors. The student can map simple functions onto programmable logic devices manually.4. The student can analyze and design digital systems of moderate complexity using contemporary technology methods, including programmable logic devices and CAD tools. The student can use standard combinational and sequential digital building blocks including adders, multiplexers, decoders, encoders, and registers.5. The student can analyze and design microprocessor system, the student realize how to use stack, interrupt, address decoder, peripheral, interface and serial communication.6. The student can design and implement software for microprocessor, using super loop and foreground and background architecture7. The student will be able to write proper lab reports, communicating their objectives, approach, observations, and conclusions	EL1200 Pengantar Analisis Rangkaian (Prasyarat Sudah Ambil)EL3011 Arsitektur Sistem Komputer (Prasyarat Sudah Ambil)	Tidak ada data	\n                                    S. Brown and Z. Vranesic,\n                                    Fundamentals of Digital Logic and VHDL Design,\n                                    3rd,\n                                    McGraw-Hill,\n                                    2009\n                                \n                                    J.T. Ronald, S.W. Neal, G.L. Moss,\n                                    Digital Systems Principles and Applications,\n                                    10,\n                                    Pearson,\n                                    2007\n                                \n                                    Dhananjay Gadre,\n                                    Programming And Customizing the AVR Microcontroller,\n                                    ,\n                                    McGraw-Hill,\n                                    2001\n                                	Tidak ada data	Tidak ada data
II2220	Manajemen Sumber Daya STI	Tata Kelola organisasi, tata kelola TI dan tata kelola informasi yang dibutuhkan untuk panduan pengelolaan sumberdaya STI di dalam organisasi. Kuliah ini juga memberikan siklus manajemen sumberdaya STI, organisasi pengelola sumberdaya STI, prosedur pengelolaan sumberdaya STI, kebutuhan SDM untuk pengelolaan sumberdaya STI dan ukuran kinerja pengelolaan sumberdaya STI, serta manajemen risiko pengelolaan sumberdaya STI	Tata Kelola organisasi, tata kelola TI dan tata kelola informasi yang dibutuhkan untuk panduan pengelolaan sumberdaya STI di dalam organisasi. Kuliah ini juga memberikan siklus manajemen sumberdaya STI, organisasi pengelola sumberdaya STI, prosedur pengelolaan sumberdaya STI, kebutuhan SDM untuk pengelolaan sumberdaya STI dan ukuran kinerja pengelolaan sumberdaya STI, serta manajemen risiko pengelolaan sumberdaya STI	Mahasiswa dapat menjelaskan secara lisan dan tertulis, dapat mengidentifikasi kecukupan dan dapat memberikan masukan perbaikan terhadap tatakelola organisasi, tatakelola TI, tatakelola informasi, siklus manajemen pengelolaan sumberdaya STI, ukuran kinerja pengelolaan sumberdaya STI dan manajemen risiko sumberdaya STI.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
TI3005	Organisasi & Manajemen Perusahaan Industri	Mata kuliah ini membahas tentang siklus manajemen; manajemen dan organisasi; Modifikasi dan aplikasi dalam perancangan organisasi; komunikasi; dinamika kelompok; kepemimpinan, pengembangan organisasi; struktur dan otoritas formal dalam organisasi; budaya organisasi; budaya organisasi dan manajemen perubahan	Tujuan utama dari mata kuliah ini adalah untuk mengajarkan konsep dan \r\nprinsip manajemen dan organisasi pada perusahaan industri yang meliputi \r\ntahap-tahap proses manajemen, perkembangan konsep organisasi dan \r\npenerapannya sesuai dengan kegiatan tuntutan operasi tertentu, serta \r\nrancangan struktur organisasi yang meliputi: organigram, tugas pokok dan \r\nfungsi kegiatan operasi unit organisasi untuk mencapai tujuan	Mahasiswa agar mampu memahami perkembangan konsep organisasiMahasiswa mampu memahami penerapan konsep organisasi sesuai dengan kegiatan tuntutan operasi tertentuMahasiswa mampu mengidentifikasi dan memahami faktor struktural dan kontekstual organisasiMahasiswa mampu menyusun rancangan struktur organisasi yang meliputi: organigram, tugas pokok dan fungsi kegiatan operasi unit organisasi untuk mencapai tujuan	TI2203 Psikologi Industri (Prasyarat Sudah Ambil)	Tidak ada data	\n                                    Luthans, F\t,\n                                    Organizational Behavior,\n                                    ,\n                                    McGraw-Hill Book Company,\n                                    1992\n                                \n                                    Stephen ROBBINS & Mary Coulter,\n                                    Management,\n                                    8,\n                                    Prentice-Hall International Inc,,\n                                    2005\n                                \n                                    Gordon, J.A,\n                                    Diagnostic Approach to Organizational Behavior,\n                                    ,\n                                    Ally and Bacon,\n                                    1995\n                                	Tidak ada data	Tidak ada data
II2221	Manajemen Proyek STI	Pengantar manajemen proyek, organisasi dan siklus proyek, proses manajemen proyek, manajemen integrasi, manajemen lingkup, manajemen waktu dan biaya, manajemen kualitas, manajemen sumberdaya manusia, manajemen komunikasi resiko dan procurement, kasus studi	Pengantar manajemen proyek, organisasi dan siklus proyek, proses manajemen proyek, manajemen integrasi, manajemen lingkup, manajemen waktu dan biaya, manajemen kualitas, manajemen sumberdaya manusia, manajemen komunikasi resiko dan procurement, kasus studi	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
IF2140	Pemodelan Basis Data	Mata kuliah ini\r\nmemberikan\r\npengetahuan mengenai\r\nsistem basis data\r\nsecara\r\numum, mencakup\r\narsitektur sistem\r\nbasis data,\r\npemodelan data,\r\nperancangan\r\nskema basis data\r\nrelasional,\r\npemanfaatan dan\r\npengelolaan data.	Materi yang\r\ndiberikan pada\r\nkuliah ini mencakup\r\nlatar belakang\r\nkebutuhan\r\nsistem basis data\r\ndalam mendukung\r\nkebutuhan informasi,\r\npengorganisasian\r\ndata di dalam file,\r\nberbagai pendekatan\r\ndalam pemodelan\r\ndata, pemodelan\r\ndata di level\r\nkonseptual dengan\r\nmenggunakan\r\npemodelan\r\nentity-relationship,\r\nmodel data\r\nrelasional dan\r\noperasi relasional\r\ndengan aljabar\r\nrelasional dan\r\nkalkulus relasional,\r\nperancangan skema\r\nbasis data\r\nrelasional dengan\r\nnormalisasi\r\nberdasarkan\r\nfunctional\r\ndependency, bahasa\r\nquery. [peningkatan\r\nperformansi basis\r\ndata, pengelolaan\r\ntransaksi,\r\npengelolaan basis\r\ndata\r\nterdistribusi]	- Mahasiswa memiliki pemahaman mengenai peranan sistem basis data dalam pemenuhan kebutuhan akan informasi- Mahasiswa mampu melakukan pemodelan data skala kecil-menengah dengan menggunakan model entity-relationship- Mahasiswa mampu merancang skema basis data relasional- Mahasiswa mampu mengimplementasikan sebuah basis data menggunakan DBMS Relasional- Mahasiswa mampu menemukan data dan informasi dari basis data serta memanipulasi data di dalam basis data dengan menggunakan SQL	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II2240	Analisis Kebutuhan Sistem	Memberikan pengetahuan dan pemahaman tentang dasar-dasar kebutuhan sistem dan bagaimana melaksanakan analisis terhadap kebutuhan sistem	Pengantar analisis kebutuhan sistem,  stakeholder, proses pengembangan sistem, fondasi kebutuhan, pemodelan proses, model analisis, state transition diagram, Unified Modelling Language, Manajemen Risiko Kebutuhan, dan Manajemen Value Kebutuhan	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
IF2111	Algoritma dan Struktur Data	Mata kuliah ini\r\nmemberikan \r\npemahaman atas\r\nkonsep algoritma dan\r\nstruktur\r\ndata  yang umum\r\ndipakai di bidang\r\ninformatika/ilmu\r\nkomputer, dan (2)\r\nkeahlian dalam\r\nmendesain dan\r\nmenggunakan\r\nalgoritma dan\r\nstruktur data yang\r\nbaik untuk\r\nmemecahkan\r\npersoalan, dan (3)\r\nmembangun program\r\ndengan \r\nparadigma \r\nprosedural skala\r\nmenengah dengan\r\nbahasa pemrograman\r\nC.	Konsep dasar\r\nstruktur data dalam\r\npemrograman\r\nprosedural: Program\r\n=\r\nalgoritma + struktur\r\ndata; modularisasi\r\nprogram; scope dan\r\nlife time variabel; \r\nMesin abstrak, dan\r\ntipe data abstrak :\r\ndasar, tabel,\r\nmatriks, stack,\r\nqueue,\r\nlist, binary tree;\r\nkonsep, primitif,\r\noperasi, dan\r\npemakaian library.\r\nADT\r\ndengan berbagai\r\nvariasi\r\nrepresentasi.\r\nAlgoritma yang\r\nrelevan dengan\r\nmasing-masing\r\nstruktur data:\r\nsequential\r\nprocessing\r\n(traversal,\r\nsearching), sorting,\r\ninsert, update\r\ndelete, reverse  \r\nPemrograman\r\nprosedural dengan\r\nbahasa C & studi\r\nkasus	Mahasiswa mengenali dan memahami konsep dan dasar struktur data Mahasiswa mampu untuk memakai paket struktur data yang tersedia Mahasiswa mambu mendesain dan mengimplementai paket struktur data Mahasiswa mampu melakukan problem solving (dengan pemrograman prosedural, berbahasa C, skala menengah) menggunakan struktur data, baik menggunakan API/library yang tersedia ataupun dengan membangun sendiri library (from the scratch) Mahasiswa siap untuk mempelajari pemrograman berorientasi objek (OOP)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
IF2210	Pemrograman Berorientasi Objek	Mata kuliah PBO\r\nmemberikan pemahaman\r\natas konsep dan\r\nkeahlian dalam\r\nmenmbangun program\r\ndengan  paradigma\r\nberorientasi objek\r\ndengan 3 bahasa\r\npemrograman.	Konsep dan praktek\r\npengembangan program\r\naplikasi dan\r\nkomponen perangkat\r\nlunak dengan OOP\r\nmenggunakan fitur\r\nlanjut OOP, dan\r\npraktek menggunakan\r\ntiga\r\nbahasa pemrograman\r\n(C++, Java, C#)\r\nParadigma\r\nberorientasi objek:\r\nkelas, objek, masa\r\nhidup objek,\r\noperasi-dasar\r\nobjek, generik,\r\ninheritance dan\r\npolymorphism,\r\nexception and\r\nassertion\r\nKonsep dan\r\nimplementasi tipe\r\ndata abstrak(ADT),\r\nmesin abstrak, dan\r\nproses\r\nin OOP (Pemrograman\r\nOO) dengan 3 bahasa\r\nyang dipilih (C++,\r\nJava, C#)	1. Students understand the concepts of OOP paradigm such as class, object, encapulation, message passing, object life time, basic object-operation, inheritace & polymorphism, genericity, and the concepts of abtsract data type , engine and process and tehir implementation in OOP. 2. Students able to develop medium-scale programs using OOP paradigm using three OOP languages , i.e. C , Java, C	IF2110 Algoritma & Struktur Data (Prasyarat Sudah Ambil)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3150	Sistem Multimedia	Membahas cara mengakuisisi,  mengolah, menyimpan, menganalisis, dan menyajikan informasi  multimedia, dan mengevaluasi kualitasnya secara subyektif dan obyektif.	Mata kuliah ini membahas metode akuisisi,konversi, penyimpanan, analisis, dan penyajian informasi multimedia, yakni informasi citra, video, dan audio. Selain itu, mata kuliah ini juga membahas metode distribusi informasi multimedia tersebut melalui jaringan komputer dan internet, dan metode untuk mengevaluasi kualitas pengiriman informasi multimedia tersebut secara subyektif dan obyektif dengan memperkenalkan Quality of Service (QoS) dan Quality of Experience (QoE).	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3260	Platform dan Pengembangan Aplikasi Mobile	1. Sistem web dan teknologi mobile\r\n2. Arsitektur informasi sistem web\r\n3. Sistem wen dan media digital mobile\r\n4. Pengembangan mobile dan sistem web\r\n5. Celah keamanan mobile dan sistem web\r\n6. Perangkat lunak sosial	1. Sistem web dan teknologi mobile\r\n2. Arsitektur informasi sistem web\r\n3. Sistem wen dan media digital mobile\r\n4. Pengembangan mobile dan sistem web\r\n5. Celah keamanan mobile dan sistem web\r\n6. Perangkat lunak sosial	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3130	Sistem Operasi	Konsep system \r\noperasi, manajemen \r\nproses, manajemen \r\nmemory, manajemen \r\nfile \r\ndan device I/O, \r\nproteksi dan \r\nkeamanan system \r\ncomputer, system \r\noperasi untuk \r\nsistem terdistribusi	Kuliah ini adalah kuliah sistem operasi yang ditekankan pada pemrograman. Pengalaman inti dalam kuliah ini adalah menulis bagian dari kernel sistem operasi, dalam bahasa C dan bahasa assembly x86, yang berjalan pada perangkat keras PC. Topik kuliah akan ditekankan pada prinsip perancangan yang mencakup abstraksi, proses, sinkronisasi penjadwalan, alokasi memori, memori maya, sistem file, dan komunikasi antar proses.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3230	Keamanan Informasi	Kuliah ini ditujukan untuk membantu mahasiswa memperoleh pemahaman fundamental dan komprehensif tentang keamanan informasi. Kuliah akan difokuskan pada isu-isu utama keamanan informasi, teknologi dan pendekatan. Mahasiswa yang berhasil menyelesaikan kuliah ini akan memiliki konsep dan pengetahuan mengenai properti keamanan, kepedulian, kebijakan, model, kriptografi, PKI, firewalls, evaluasi keamanan, dan kasus-kasus nyata seputar keamanan. Mahasiswa akan memiliki pengalaman langsung mengenai teknologi keamanan informasi melalui sesi di laboratorium.	Kuliah ini ditujukan untuk membantu mahasiswa memperoleh pemahaman fundamental dan komprehensif tentang keamanan informasi. Kuliah akan difokuskan pada isu-isu utama keamanan informasi, teknologi dan pendekatan. Mahasiswa yang berhasil menyelesaikan kuliah ini akan memiliki konsep dan pengetahuan mengenai properti keamanan, kepedulian, kebijakan, model, kriptografi, PKI, firewalls, evaluasi keamanan, dan kasus-kasus nyata seputar keamanan. Mahasiswa akan memiliki pengalaman langsung mengenai teknologi keamanan informasi melalui sesi di laboratorium.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3160	Pemrograman Integratif	Materi kuliah didasarkan pada perangkat lunak open source seperti PHP dan perangkat lunak closed source seperti ASP. Perangkat lunak yang digunakan bergantung pada dosen yang mengajar. ASP adalah arsitektur scripting di sisi server untuk membangun aplikasi web dinamis dan lingkungan ideal untuk membangun solusi komersial berbasis web. Kuliah akan memandang integrasi sistem dengan berfokus pada mekanisme komunikasi dan standarisasi. Mahasiswa akan belajar bagaimana menyajikan struktur dan bagaimana transportasi data menggunakan XML dan protokol serta teknologi terkait XML. Standarisasi dokumen XML untuk keperluan pertukaran data sangat ditekankan.	Materi kuliah didasarkan pada perangkat lunak open source seperti PHP dan perangkat lunak closed source seperti ASP. Perangkat lunak yang digunakan bergantung pada dosen yang mengajar. ASP adalah arsitektur scripting di sisi server untuk membangun aplikasi web dinamis dan lingkungan ideal untuk membangun solusi komersial berbasis web. Kuliah akan memandang integrasi sistem dengan berfokus pada mekanisme komunikasi dan standarisasi. Mahasiswa akan belajar bagaimana menyajikan struktur dan bagaimana transportasi data menggunakan XML dan protokol serta teknologi terkait XML. Standarisasi dokumen XML untuk keperluan pertukaran data sangat ditekankan.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3231	Interaksi Manusia dg Komputer & Antarmuka	Apa itu Perancangan Interaksi, memahami dan mengkonseptualisasi interaksi, aspek kognitif, interaksi sosial, antarmuka, pengumpulan data, analisis dan interpretasi data, reperesentasi, proses perancangan interaksi, menetapkan kebutuhan, perancangan purwarupa dan konstruksi, kerangka kerja evaluasi, pengenalan pengelolaan ucapan, teknologi penangkapan data, dan teknologi pembangkit informasi	1. Faktor manusia\r\n2. Aspek IMK dari domain aplikasi\r\n3. Evaluasi berpusat manusia\r\n4. Pengembangan antarmuka yang efektif\r\n5. Aksesibilitas\r\n6. Teknologi  baru\r\n7. Komputasi berpusat manusia\r\n8. Tujuan dan asesmen usability\r\n9. Proses perancangan\r\n10. Teori perancangan dan tradeoff\r\n11. Gaya interaksi\r\n12. Perangkat interaksi\r\n13. Pencarian informasi\r\n14. Visualisasi informasi\r\n15. Dokuemetasi pengguna dan bantuan online\r\n16. Pelaporan dan recovery error	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3120	Layanan Sistem dan Teknologi Informasi	Tinjauan dan sejarah pelayanan sistem dan teknologi informasi; Konsep pelayanan; ITIL service lifecycle; Konsep proses dan fungsi; Konsep dan definisi kunci manajemen pelayanan; Prinsip kunci dan model-model; Proses-proses; Roles.	Sejarah pelayanan sistem dan teknologi informasi, IT Infrastructure Library (ITIL); Konsep pelayanan, konsep manajemen pelayanan, konsep pemangku kepentingan; Struktur ITIL service lifecycle, service strategy, service design, service transition, service operation, dan continual service improvement; Definisi proses dan fungsi, model proses-proses, karakteristik proses-proses; Konsep dan definisi kunci manajemen pelayanan; Konsep penciptaan nilai, komponen manajemen pelayanan, aspek utama dalam service design; Pendekatan continual service improvement, konsep aturan di dalam continual service improvement; Proses-proses service strategy, service design, service transition, service operation, continual service improvement; Role dan responsibility dari process owner, process manager, process practitioner, service owner, model RACI (responsible, accountable, consulted, informed).	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
EL4233	Dasar Sistem & Kendali Cerdas	Pengenalan Inteligent Sistem / Mesin dan Cerdas Control, Teori Set Fuzzy, aturan Fuzzy, Penalaran Fuzzy, Fuzzy Inference Systems, Fuzzy Control, Biologi Neural Networks, Neuron Model dan Komputasi, Perceptron, Belajar pengawasan, Adaptive Linear Networks, Multilayer Feedforward Neural Networks, backpropagation, Aplikasi Sistem Cerdas dan Cerdas Control, Implementasi Matlab, Robotika 	Pengenalan Inteligent Sistem dan Cerdas Control, Karakteristik Sistem Cerdas, Fuzzy Set Theory: definisi dasar dan terminologi, fungsi keanggotaan (MF), himpunan fuzzy operasi teoritis, perumusan fungsi keanggotaan, MF dari dua dimensi, Aturan Fuzzy: hubungan fuzzy, relasi kabur komposisi, fuzzy if-then aturan, Fuzzy penalaran: aturan komposisi inferensi, penalaran fuzzy, Fuzzy inference Systems (FIS), Model Mamdani FIS, Fuzzy control: arsitektur kontrol fuzzy dan komponen, fuzzification, defuzzifikasi, aturan fuzzy, mekanisme inferensi fuzzy, struktur kabur kontrol, kabur pengembangan aturan, embedded system fuzzy, aplikasi kontrol fuzzy, Biologi Neural Networks, Neuron Model dan Komputasi, Artificial Neural Networks (ANN) Topologi, Perceptron, Belajar pengawasan, Perceptron Pelatihan, Adaptive Linear Networks, Delta Rule, Multilayer Feedforward Jaringan saraf (MFNN), MFNN maju perhitungan, MFNN perhitungan mundur dan Backpropagation, MFNN belajar mekanisme, aplikasi ANN di pengakuan pola dan kontrol, implementasi Matlab, Pengenalan robot mobile sebagai platform sistem cerdas.	After completing this course students should able to : Compare crisp set and fuzzy set, Formulate fuzzy membership function, Understand continuous and discrete fuzzy set, Perform various fuzzy set operations, Compute fuzzy relation composition, Compute membership function of fuzzy rules, Understand fuzzy reasoning, Compute Fuzzy Inference System, Design fuzzy logic control, Determize fuzzy rules in fuzzy control, Program fuzzy concepts in Matlab, Understand neuron model, Compute neuron output given an input signal, perform perceptron training, understand adaptive networks, understand supervised learning, comprehend MFNN, perform MFNN forward computation, perform Backpropagation to update MFNN weights, comprehend fuzzy embedded control, program fuzzy control on microprocessor/microcontroller, understand the use of ANN in pattern recognition and in control, compare model based design with intelligent systems methods, use Fuzzy Logic Toolbox in Matlab, use Neural Networks Toolbox in Matlab, implement fuzzy inference system in embedded microcontroller/microprocessor, Understand basic pinciples of mobile robots	EL3014 Sistem Mikroprosesor (Prasyarat Sudah Ambil)EL3015 Sistem Kendali (Prasyarat Sudah Ambil)	Tidak ada data	\n                                    J.S.R. Jang, C.T. Sun, E. Mizutani,\n                                    Neurofuzzy and Soft Computing : A computational Approach to Learning and Machine Intelligence,\n                                    ,\n                                    Prentice-Hall,\n                                    1997\n                                \n                                    John H Lilly,\n                                    Fuzzy Control and Identification,\n                                    ,\n                                    Wiley,\n                                    2010\n                                \n                                    S. Haykin,\n                                    Neural Networks: A Comprehensive Foundation,\n                                    ,\n                                    ,\n                                    2002\n                                	Tidak ada data	Tidak ada data
II3121	Analisis Kebutuhan Enterprise	Mata kuliah ini bertujuan untuk memberi pengetahuan tentang identifikasi informasi,  mengumpulkan informasi,  dan melakukan analisis informasi yang diperlukan perusahaan.	Mata kuliah ini bertujuan untuk memberi pengetahuan tentang identifikasi informasi,  mengumpulkan informasi,  dan melakukan analisis informasi yang diperlukan perusahaan.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3220	Arsitektur Enterprise	Memberikan pengetahuan dan pemahaman kepada mahasiswa tentang langkah-langkah pengembangan rencana strategis system informasi berskala enterprise.	Kerangka kerja arsitektur enterprise (EA), komponen asitektur, arsitektur interorganisasi, proses pengembangan EA, manajemen perubahan arsitektur, implementasi WA, EA dan manajemen kendali, strategi sistem informasi, keselarasan strategis (strategic alignment), dampak dari sistem informasi pada proses dan struktur organisasional, perencanaan sistem informasi, peran IT dalam mendefinisikan dan membentuk kompetisi, membiayai dan mengevaluasi kinerja investasi dan operasional IT (BoK ACM)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
IF3152	Rekayasa Perangkat Lunak	Kuliah ini memberikan pengertian tentang rekayasa perangkat lunak dan kemampuan dasar dalam membangun perangkat lunak skala kecil dan sederhana, serta kemampuan mengoperasikan tools terkait pemodelan perangkat lunak.	Tipe Perangkat Lunak (P/L); Pengantar Rekayasa P/L; Siklus Hidup P/L, mencakup pengumpulan kebutuhan, analisis, perancangan, implementasi, pengujian,pengoperasian, dan perawatan; Berbagai Model Proses,mencakup waterfall, prototyping, incremental, agile process model, dll; Metodologi Pembangunan P/L (termasuk metode berorientasi objek): kelebihan, kekurangan dan aspek praktisnya di industri; Standard dan Dokumentasi P/L, mencakup SDD, SRS, SDD, STP;  Alat Bantu Pemodelan P/L, mencakup UML dan DFD; Studi Kasus: Pembangunan P/L Skala Kecil(diberikan spesifikasi kebutuhan, dilakukan  analisis, perancangan, dan perencanaaan pengujian)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II3240	Rekayasa Sistem dan Teknologi Informasi	Prinsip-prinsip, kerangka kerja dan proses rekayasa STI. Kuliah ini juga memberikan kasus-kasus dan tugas-tugas kepada mahasiswa untuk melakukan studi kasus rekayasa STI selama perkuliahan berlangsung, sehingga mahasiswa mempunyai keterampilan melakukan rekayasa STI.	Prinsip-prinsip, kerangka kerja dan proses rekayasa STI. Kuliah ini juga memberikan kasus-kasus dan tugas-tugas kepada mahasiswa untuk melakukan studi kasus rekayasa STI selama perkuliahan berlangsung, sehingga mahasiswa mempunyai keterampilan melakukan rekayasa STI.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II4471	Kapita Selekta STI	Issue-issue terkini di bidang Sistem dan Teknologi Informasi: (1) Cloud Computing; (2) Payment Systems and Banking System; (3) Roadmap of Mobile Systems; (4) Business Continuity Management; (5) Electricity Systems; (6) Battery Technology; (7) Storage System; (8) Big Data Issues; (9) Telecommunication Systems; (10) Future IT Trends; (11)  IT Industries in Many Countries; other flexible topics.	Issue-issue terkini di bidang Sistem dan Teknologi Informasi: (1) Cloud Computing; (2) Payment Systems and Banking System; (3) Roadmap of Mobile Systems; (4) Business Continuity Management; (5) Electricity Systems; (6) Battery Technology; (7) Storage System; (8) Big Data Issues; (9) Telecommunication Systems; (10) Future IT Trends; (11)  IT Industries in Many Countries; other flexible topics.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II4090	Kerja Praktek	Kerja praktek dilakukan oleh mahasiswa di suatu lingkungan kerja, baik industri, lembaga penelitian, instansi pemerintah, dan sebagainya. Tujuan Kerja Praktek untuk memberikan gambaran kepada mahasiswa tentang lingkungan kerja yang akan dihadapinya, disamping juga untuk memberikan pengalaman kerja dan memperluas wawasannya. Mahasiswa yang diperbolehkan mengambil Kerja Praktek adalah mahasiswa yang telah memperoleh kuliah hingga semester 6. Kerja praktek dilaksanakan selama 2 bulan. Mahasiswa melakukan Kerja Praktek dalam libur panjang antara semester 6 dan 7 tanpa mengambil SKS-nya. Pada semester 7, mahasiswa mengambil SKS Kerja Praktek untuk menyusun Laporan Kerja Praktek atas bimbingan dosen pembimbing Kerja Praktek dan menjalani Seminar Kerja Praktek.	Kerja praktek dilakukan oleh mahasiswa di suatu lingkungan kerja, baik industri, lembaga penelitian, instansi pemerintah, dan sebagainya. Tujuan Kerja Praktek untuk memberikan gambaran kepada mahasiswa tentang lingkungan kerja yang akan dihadapinya, disamping juga untuk memberikan pengalaman kerja dan memperluas wawasannya. Mahasiswa yang diperbolehkan mengambil Kerja Praktek adalah mahasiswa yang telah memperoleh kuliah hingga semester 6. Kerja praktek dilaksanakan selama 2 bulan. Mahasiswa melakukan Kerja Praktek dalam libur panjang antara semester 6 dan 7 tanpa mengambil SKS-nya. Pada semester 7, mahasiswa mengambil SKS Kerja Praktek untuk menyusun Laporan Kerja Praktek atas bimbingan dosen pembimbing Kerja Praktek dan menjalani Seminar Kerja Praktek.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU206X	Agama dan Etika	Tidak ada data	Tidak ada data	Tidak ada data	KU2061 Agama dan Etika Islam (Substitusi)KU2062 Agama dan Etika Protestan (Substitusi)KU2063 Agama dan Etika Katolik (Substitusi)KU2064 Agama dan Etika Hindu (Substitusi)KU2065 Agama dan Etika Budha (Substitusi)KU2066 Agama dan Etika Khonghucu (Substitusi)	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II4091	Tugas Akhir 1 & Seminar	Pada kuliah ini, mahasiswa mendefinisikan rumusan masalah untuk tugas akhir yang dikerjakannya, dan mendeskripiskan tujuan dan pengaruh tugas akhirnya terhadap sekitarnya. Mahasiswa juga melakukan studi literatur yang terkait dengan permasalahan. Mahasiwa juga diharuskan untuk menulis laporan tugas akhir I, dan mempresentasikan hasilnya dalam sebuah seminar.	Topik-topik yang dibahas mencakup: informasi umum tugas akhir, studi literatur, rumusan masalah dan tujuan tugas akhir, pendekatan solusi dan solusi umum atas permasalahan, penulisan laporan, dan presentasi lisan.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
KU2071	Pancasila dan Kewarganegaraan	Mahasiswa diberi \r\nmateri  tentang \r\npendidikan  politik,\r\n hukum dan ketata\r\nnegaraan,  penegakan\r\n HAM, tatakelola\r\npemerintahan yang\r\nbaik dan bersih,\r\nhak dan kewajiban\r\nwarga negara, budaya\r\ndemokrasi, serta\r\ngeopolitik dan\r\ngeostrategi.	1). Pancasila\r\nsebagai Falsafah dan\r\ndasar negara \r\n(2).identitas\r\nnasional \r\n(3).Politik dan\r\nstrategi (4).\r\nOtonomi daerah (5).\r\nTatakelola\r\npemerintahan\r\nyang baik dan bersih\r\n (6). Hak dan\r\nkewajiban warga\r\nnegara (7). Budaya\r\nberdemokrasi (8)\r\nMasyarakat  Madani \r\n(9). Supremasi hukum\r\n(10). Penegakan\r\nHAM  (11).\r\nGeopolitik  (12).\r\nGeostrategi	Mahasiswa memiliki cita-cita menjadi warga negara yang baik. Komitted terhadap nilai-nilai Pancasila dalam kehidupan bermasyarakat, berbangsa dan bernegara. Memiliki rasa nasionalisme dan patriotism yang tinggi. Berwawasan Nusantara dalam mewujudkan tatanan sosial budaya dan sosial politik Termotivasi untuk ikut membenahi penegakan hukum dan HAM,. Termotivasi untuk ikut mewujudkan pemerintahan yang baik, besih dan berwibawa. Mendukung terciptanya budaya berdemokrasi. Menjaga kerukunan hidup antar umat beragama dan mampu bekerjasama di tengah-tengah masyarakat Indonesia yang pluralis.	Tidak ada data	Tidak ada data	\n                                    1.\tTim Nasional Dosen Pendidikan Kewarganegaraan,\n                                    Pendidikan Kewarga\r\nnegaraan : \r\nParadigma Terbaru\r\nuntuk Mahasiswa,,\n                                    ,\n                                    Alfabeta, Bandung,\n                                    2010\n                                \n                                    2.\tUbaidillah dan Abdul Razaq,\n                                    Pancasila,\r\nDemokrasi, HAM dan\r\nMasyarakat Madani,\n                                    ,\n                                    Prenada Media Group, Jakarta,,\n                                    2012\n                                \n                                    3.\tAffan Gaffar,\n                                    Politik Indonesia :\r\n Transisi Menuju\r\nDemokrasi,\n                                    ,\n                                    , Pustaka Pelajar Offset, Yogjakarta,\n                                    2000\n                                \n                                    4.\tKaelan,,\n                                    Pendidikan\r\nKewarganegaraan\r\nUntuk Mahasiswa,\n                                    ,\n                                    Pustaka Pelajar,\n                                    2011\n                                \n                                    ,\n                                    ,\n                                    ,\n                                    ,\n                                    \n                                \n                                    ,\n                                    ,\n                                    ,\n                                    ,\n                                    \n                                \n                                    ,\n                                    ,\n                                    ,\n                                    ,\n                                    \n                                	Tidak ada data	Tidak ada data
II4470	Hukum & Etika Teknologi Informasi	Pengantar Cyberethics; Konsep dan teori etika; Keterampilan berpikir kritis dan argumen logika; Etika profesional; Privasi dan Cyberspace; Keamanan di Cyberspace; Cybercrime dan kejahatan yang berhubungan dengan cyber; Intellectual Property; Regulasi perdagangan; Isu-isu sosial; Teknologi konvergensi dan pervasive; Regulasi-regulasi di Indonesia.	Pengantar tentang Cyberethics – konsep, perspektif, dan framework metodologi; Konsep dan teori etika – membangun dan menjustifikasi sistem moral; Keterampilan berpikir kritis dan argumen logika – piranti untuk mengevaluasi isu Cyberethics; Etika profesional dan tanggung jawab moral; Privasi dan Cyberspace; Keamanan di Cyberspace; Cybercrime dan kejahatan yang berhubungan dengan cyber; Perdebatan Intellectual Property di Cyberspace; Regulasi perdagangan di Cyberspace; Isu-isu sosial, digital divide, dan transformasi pekerjaan – dampak dari kelas, ras, dan gender; Komunitas dan identitas di Cyberspace – aspek etika dari teknologi-teknologi Virtual-Reality dan Artificial-Intelligence; Teknologi pervasive computing dan konvergensi – aspek etika dari Ambient Intelligence, Bioinformatics, dan Nanocomputing, Regulasi-regulasi di Indonesia.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
BI2001	Pengetahuan Lingkungan	Ekosistem sebagai modal alam; dampak populasi manusia terhadap modal alam; keberlanjutan sumberdaya (keanekaragaman hayati, lahan, tanah, air, udara, mineral, energi) dan kualitas lingkungan; keberlanjutan masyarakat (ekonomi, politik dan lingkungan); isu-isu lingkungan global dan lokal (Indonesia).	Mata kuliah pengetahuan lingkungan yang menekankan ekosistem sebagai modal alam, dan prinsip keberlanjutan sebagai tema pemersatu. Topik pembahasan mencakup: konsep ekosistem; ekosistem sebagai modal alam (ekosistem terestrial dan akuatik); prinsip-prinsip ekologi dan keberlanjutan; populasi manusia dan dampaknya; jasa ekosistem; keberlanjutan keanekaragaman hayati; keberlanjutan sumberdaya alam (lahan, tanah, air, udara, mineral dan energi); keberlanjutan kualitas lingkungan (isu-isu terkait risiko lingkungan dan kesehatan manusia, pencemaran, perubahan iklim, limbah, lingkungan perkotaan); keberlanjutan masyarakat manusia (ekonomi, politik, etika dan pandangan hidup/worldviews); isu-isu lingkungan global dan lokal (Indonesia), serta contoh-contoh kasus.	Knowledge: \r\n• explain the ecosystem concept and its relevance to environmental issues. \r\n• describe the importance of ecosystems as natural capital and provider of ecosystem services for human welfare. \r\n• express the concept of sustainability and recognize examples of sustainable solutions to environmental problems. \r\n• relate human population growth to resource sustainability and environmental quality.Skill: \r\n• identify and interpret general environmental problems, at the local, regional and global levels. \r\n• collect and present relevant information dealing with environmental issues.Competences: \r\n• point out the complexity of environmental issues as related to economic and sociocultural aspects. \r\n• apply critical thinking in discussing environmental issues.	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
II4472	Komunikasi Interpersonal	Dasar-dasar komunikasi interpersonal; Budaya dan komunikasi interpersonal; Persepsi dan pribadi di dalam komunikasi interpersonal; Mendengarkan di dalam komunikasi interpersonal; Pesan verbal; Pesan nonverbal; Pesan-pesan emosional; Pesan-pesan percakapan; Hubungan interpersonal; Konflik interpersonal dan manajemen konflik; Kekuasaan dan pengaruh interpersonal;	Dasar-dasar komunikasi interpersonal; Budaya dan komunikasi interpersonal; Persepsi dan pribadi di dalam komunikasi interpersonal; Mendengarkan di dalam komunikasi interpersonal – Pentingnya mendengarkan: manfaat profesional dan hubungan, proses mendengarkan, kendala-kendala dalam mendengarkan, budaya, gender, dan mendengarkan, gaya-gaya mendengarkan yang efektif; Pesan verbal – prinsip-prinsip komunikasi verbal, panduan-panduan di dalam komunikasi verbal yang efektif; Pesan nonverbal – kanal-kanal untuk melakukan komunikasi nonverbal, fungsi-fungsi komunikasi nonverbal, komunikasi dan budaya komunikasi nonverbal; Pesan-pesan emosional; Pesan-pesan percakapan; Hubungan interpersonal – tahapan dan teori; Hubungan interpersonal – pengembangan dan kemunduran: Tipe-tipe hubungan interpersonal: persahabatan, cinta, keluarga, dan tempat kerja; Konflik interpersonal dan manajemen konflik; Kekuasaan dan pengaruh interpersonal;	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data	Tidak ada data
\.


--
-- Data for Name: thread; Type: TABLE DATA; Schema: public; Owner: kelompok1
--

COPY public.thread (id_thread, kode_matkul, judul_thread, pertanyaan, isdone, favorit, userid, datecreated) FROM stdin;
1	II3220	Meningkatkan kemampuan belajar	Bagaimana cara meningkatkan kemampuan belajar saya?	f	1	1	2019-04-29 04:44:22.01
2	II3220	Mengerjakan tugas besar	Bagaimana cara mengerjakan tugas besar apabila sekelompok tidak ada ada yang mengerjakan?	f	1	1	2019-04-29 04:45:10.21
3	II3230	Konfirmasi Judul Paper	Selamat siang Pak Budi, kapan judul paper kami dikonfirmasi?	f	0	2	2019-04-29 04:46:55.491
4	II3230	Konfirmasi Tanggal Pengumpulan Paper	Selamat siang Pak Budi, apakah benar pengumpulan papernya tidak jadi tanggal 15 Mei 2019 melainkan 15 Mei 2020?	f	0	3	2019-04-29 04:47:34.757
5	II3240	Kuesioner Perkuliahan	Selamat siang Pak Suhardi, Saya salah mengisi kuesioner perkuliahan, apakah bisa dibenarkan sekaranga?	f	0	4	2019-04-29 04:48:40.596
6	II3240	Tugas Terlalu Susah	Selamat siang Pak Novi, \n Sekarang sudah minggu kelima belas perkuliahan? namun saya belum mulau tugasnya, lalu apakah saya bisa lulus?	f	3	3	2019-04-29 04:52:48.738
\.


--
-- Name: sap_id_sap_seq; Type: SEQUENCE SET; Schema: public; Owner: kelompok1
--

SELECT pg_catalog.setval('public.sap_id_sap_seq', 343, true);


--
-- Name: ekivalensi ekivalensi_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.ekivalensi
    ADD CONSTRAINT ekivalensi_pkey PRIMARY KEY (id_ekivalensi);


--
-- Name: jawaban jawaban_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.jawaban
    ADD CONSTRAINT jawaban_pkey PRIMARY KEY (id_jawaban);


--
-- Name: kurikulum kurikulum_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.kurikulum
    ADD CONSTRAINT kurikulum_pkey PRIMARY KEY (id_kurikulum);


--
-- Name: matkul matkul_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.matkul
    ADD CONSTRAINT matkul_pkey PRIMARY KEY (id_matkul);


--
-- Name: prodi prodi_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.prodi
    ADD CONSTRAINT prodi_pkey PRIMARY KEY (kode_prodi);


--
-- Name: sap sap_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.sap
    ADD CONSTRAINT sap_pkey PRIMARY KEY (id_sap);


--
-- Name: silabus silabus_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.silabus
    ADD CONSTRAINT silabus_pkey PRIMARY KEY (kode_matkul);


--
-- Name: thread thread_pkey; Type: CONSTRAINT; Schema: public; Owner: kelompok1
--

ALTER TABLE ONLY public.thread
    ADD CONSTRAINT thread_pkey PRIMARY KEY (id_thread);


--
-- PostgreSQL database dump complete
--

